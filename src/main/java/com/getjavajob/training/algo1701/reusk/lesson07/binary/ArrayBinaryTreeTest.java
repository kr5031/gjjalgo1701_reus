package com.getjavajob.training.algo1701.reusk.lesson07.binary;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;
import com.getjavajob.training.algo1701.reusk.lesson07.binary.ArrayBinaryTree.NodeImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static com.getjavajob.training.algo1701.reusk.lesson07.binary.ArrayBinaryTree.CAN_NOT_DELETE_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.ArrayBinaryTree.CHILD_NOT_NULL_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.ArrayBinaryTree.NO_PARENT_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.ArrayBinaryTree.NODE_NULL_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.ArrayBinaryTree.ROOT_NOT_NULL_MSG;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        testLeft();
        testRight();
        testAddLeftException();
        testAddLeftNull();
        testAddRightException();
        testAddRightNull();
        testRoot();
        testParent();
        testParentException();
        testAddRoot();
        testAddRootException();
        testAdd();
        testAddException();
        testSet();
        testRemove();
        testRemoveException();
        testSize();
        testIterator();
        testNodes();
    }

    public static void testLeft() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node2 = tree.validate(tree.right(root));
        NodeImpl<Integer> node6 = tree.validate(tree.right(node2));
        NodeImpl<Integer> actual = tree.validate(tree.left(node6));
        NodeImpl<Integer> expected = new NodeImpl<>(13, 13);
        assertEquals("ArrayBinaryTreeTest.testLeft", expected, actual);
    }

    public static void testRight() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> actual = tree.validate(tree.right(root));
        NodeImpl<Integer> expected = new NodeImpl<>(2, 2);
        assertEquals("ArrayBinaryTreeTest.testRight", expected, actual);
    }

    public static void testAddLeftException() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        try {
            tree.addLeft(null, 7);
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testAddLeftException", NODE_NULL_MSG, e.getMessage());
            return;
        }
        fail("ArrayBinaryTreeTest.testAddLeftException");
    }

    public static void testAddRightException() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        try {
            tree.addRight(null, 7);
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testAddRightException", NODE_NULL_MSG, e.getMessage());
            return;
        }
        fail("ArrayBinaryTreeTest.testAddRightException");
    }

    public static void testAddLeftNull() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node2 = tree.validate(tree.right(root));
        NodeImpl<Integer> node6 = tree.validate(tree.right(node2));
        assertEquals("ArrayBinaryTreeTest.testAddLeftNull", null, tree.addLeft(node6, 77));
    }

    public static void testAddRightNull() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node2 = tree.validate(tree.right(root));
        assertEquals("ArrayBinaryTreeTest.testAddLeftNull", null, tree.addRight(root, 77));
    }

    public static void testRoot() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> expected = new NodeImpl<>(0, 0);
        assertEquals("ArrayBinaryTreeTest.testRoot", expected, tree.root());
    }

    public static void testParent() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> node2 = tree.validate(tree.right(tree.root()));
        assertEquals("ArrayBinaryTreeTest.testParent", tree.root(), tree.parent(node2));

    }

    public static void testParentException() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        try {
            tree.parent(tree.root());
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testParentException", NO_PARENT_MSG, e.getMessage());
            return;
        }
        fail("ArrayBinaryTreeTest.testParentException");
    }

    public static void testAddRoot() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        NodeImpl<Integer> expected = new NodeImpl<>(0, 0);
        assertEquals("ArrayBinaryTreeTest.testAddRoot", expected, tree.addRoot(0));
    }

    public static void testAddRootException() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        try {
            tree.addRoot(0);
        } catch (IllegalStateException e) {
            assertEquals("ArrayBinaryTreeTest.testAddRootException", ROOT_NOT_NULL_MSG, e.getMessage());
            return;
        }
        fail("ArrayBinaryTreeTest.testAddRootException");
    }

    public static void testAdd() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(0);
        NodeImpl<Integer> expected = new NodeImpl<>(1, 1);
        assertEquals("ArrayBinaryTreeTest.testAdd", expected, tree.add(tree.root(), 1));
    }

    public static void testAddException() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> node2 = tree.validate(tree.right(tree.root()));
        NodeImpl<Integer> node6 = tree.validate(tree.right(node2));
        try {
            tree.add(node6, 777);
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testAddException", CHILD_NOT_NULL_MSG, e.getMessage());
            return;
        }
        fail("ArrayBinaryTreeTest.testAddException");
    }

    public static void testSet() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        tree.set(tree.root(), 777);
        assertEquals("ArrayBinaryTreeTest.testSet", 777, (int) tree.root().getElement());
    }

    public static void testRemove() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> node2 = tree.validate(tree.right(tree.root()));
        assertEquals("ArrayBinaryTreeTest.testRemove", 2, (int) tree.remove(node2));
    }

    public static void testRemoveException() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        NodeImpl<Integer> node2 = tree.validate(tree.right(tree.root()));
        NodeImpl<Integer> node6 = tree.validate(tree.right(node2));
        try {
            tree.remove(node6);
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testRemoveException", CAN_NOT_DELETE_MSG, e.getMessage());
            return;
        }
        fail("ArrayBinaryTreeTest.testRemoveException");
    }

    public static void testSize() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        assertEquals("ArrayBinaryTreeTest.testSize", 9, tree.size());
    }

    public static void testIterator() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        Iterator<Integer> iterator = tree.iterator();
        int[] actual = new int[tree.size()];
        int counter = 0;
        while (iterator.hasNext()) {
            actual[counter++] = iterator.next();
        }
        int[] expected = {0, 2, 6, 13, 14, 27, 28, 29, 30};
        assertEquals("ArrayBinaryTreeTest.testIterator", expected, actual);
    }

    public static void testNodes() {
        ArrayBinaryTree<Integer> tree = getArrayBinaryTree();
        Collection<Node<Integer>> expected = new ArrayList<>();
        expected.add(new NodeImpl<>(0, 0));
        expected.add(new NodeImpl<>(2, 2));
        expected.add(new NodeImpl<>(6, 6));
        expected.add(new NodeImpl<>(13, 13));
        expected.add(new NodeImpl<>(14, 14));
        expected.add(new NodeImpl<>(27, 27));
        expected.add(new NodeImpl<>(28, 28));
        expected.add(new NodeImpl<>(29, 29));
        expected.add(new NodeImpl<>(30, 30));
        assertEquals("ArrayBinaryTreeTest.testNodes", expected, tree.nodes());
    }

    /*
        ArrayBinaryTree for tests
                                   0
                                     \
                                      2
                                        \
                                         6
                                      /     \
                                     13      14
                                    /  \    /   \
                                   27  28  29    30

      array: 0  _  2  _  _  _  6  _  _  _  _  _  _  13  14  _  _  _  _  _  _  _  _  _  _  _  _  27  28  29  30
             0  1  2  3  4  5  6  7  8  9  10 11 12 13  14  15 16 17 18 19 20 21 22 23 24 25 26 27  28  29  30

     */
    private static ArrayBinaryTree<Integer> getArrayBinaryTree() {
        ArrayBinaryTree<Integer> arrayBinaryTree = new ArrayBinaryTree<>();
        Node root = arrayBinaryTree.addRoot(0);
        Node node2 = arrayBinaryTree.addRight(root, 2);
        Node node6 = arrayBinaryTree.addRight(node2, 6);
        Node node13 = arrayBinaryTree.addLeft(node6, 13);
        Node node27 = arrayBinaryTree.addLeft(node13, 27);
        Node node28 = arrayBinaryTree.addRight(node13, 28);
        Node node14 = arrayBinaryTree.addRight(node6, 14);
        Node node29 = arrayBinaryTree.addLeft(node14, 29);
        Node node30 = arrayBinaryTree.addRight(node14, 30);
        return arrayBinaryTree;
    }

}
