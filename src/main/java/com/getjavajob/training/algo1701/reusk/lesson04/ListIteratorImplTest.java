package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;
import static com.getjavajob.training.algo1701.reusk.lesson04.DynamicArray.CONCURRENT_MODIFY_MSG;

public class ListIteratorImplTest {

    public static void main(String[] args) {
        testHasNextTrue();
        testHasNextFalse();
        testNext();
        testNextConcurrentModify();
        testHasPreviousTrue();
        testHasPreviousFalse();
        testPrevious();
        testPreviousConcurrentModify();
        testNextIndex();
        testPreviousIndex();
        testRemove();
        testRemoveConcurrentModify();
        testSet();
        testSetConcurrentModify();
        testAdd();
        testAddConcurrentModify();
    }

    public static void testHasNextTrue() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        assertEquals("ListIteratorImplTest.testHasNextTrue", true, listIterator.hasNext());
    }

    public static void testHasNextFalse() {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        assertEquals("ListIteratorImplTest.testHasNextFalse", false, listIterator.hasNext());
    }

    public static void testNext() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        Object[] actual = new Object[doublyLinkedList.size()];
        int counter = 0;
        while (listIterator.hasNext()) {
            actual[counter++] = listIterator.next();
        }
        Object[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("ListIteratorImplTest.testNext", expected, actual);
    }

    public static void testNextConcurrentModify() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator1 = doublyLinkedList.listIterator();
        ListIterator<Integer> listIterator2 = doublyLinkedList.listIterator();
        if (listIterator2.hasNext()) {
            listIterator2.next();
            listIterator2.remove();
        }
        try {
            if (listIterator1.hasNext()) {
                listIterator1.next();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorImplTest.testNextConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorImplTest.testNextConcurrentModify");
    }

    public static void testHasPreviousTrue() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator(doublyLinkedList.size());
        assertEquals("ListIteratorImplTest.testHasPreviousTrue", true, listIterator.hasPrevious());
    }

    public static void testHasPreviousFalse() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        assertEquals("ListIteratorImplTest.testHasPreviousFalse", false, listIterator.hasPrevious());
    }

    public static void testPrevious() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator(doublyLinkedList.size());
        Integer[] actual = new Integer[doublyLinkedList.size()];
        int counter = 0;
        while (listIterator.hasPrevious()) {
            actual[counter++] = listIterator.previous();
        }
        Integer[] expected = {9, 8, 7, 6, null, 4, 3, 2, 1, 0};
        assertEquals("ListIteratorImplTest.testPrevious", expected, actual);
    }

    public static void testPreviousConcurrentModify() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator1 = doublyLinkedList.listIterator(doublyLinkedList.size());
        ListIterator<Integer> listIterator2 = doublyLinkedList.listIterator(doublyLinkedList.size());
        if (listIterator2.hasPrevious()) {
            listIterator2.previous();
            listIterator2.remove();
        }
        try {
            if (listIterator1.hasPrevious()) {
                listIterator1.previous();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorImplTest.testPreviousConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorImplTest.testPreviousConcurrentModify");
    }

    public static void testNextIndex() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        int[] actual = new int[doublyLinkedList.size()];
        int counter = 0;
        while (listIterator.hasNext()) {
            actual[counter++] = listIterator.nextIndex();
            listIterator.next();
        }
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertEquals("ListIteratorImplTest.testNextIndex", expected, actual);
    }

    public static void testPreviousIndex() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator(doublyLinkedList.size());
        int[] actual = new int[doublyLinkedList.size()];
        int counter = 0;
        while (listIterator.hasPrevious()) {
            actual[counter++] = listIterator.previousIndex();
            listIterator.previous();
        }
        int[] expected = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        assertEquals("ListIteratorImplTest.testPreviousIndex", expected, actual);
    }

    public static void testRemove() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        if (listIterator.hasNext()) {
            listIterator.next();
            listIterator.remove();
        }
        Object[] actual = doublyLinkedList.toArray();
        Integer[] expected = {1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("ListIteratorImplTest.testRemoveReturn", expected, actual);
    }

    public static void testRemoveConcurrentModify() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator1 = doublyLinkedList.listIterator();
        ListIterator<Integer> listIterator2 = doublyLinkedList.listIterator();
        if (listIterator2.hasNext()) {
            listIterator2.next();
            listIterator2.remove();
        }
        try {
            if (listIterator1.hasNext()) {
                listIterator1.next();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorImplTest.testRemoveConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorImplTest.testRemoveConcurrentModify");
    }

    public static void testSet() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        if (listIterator.hasNext()) {
            listIterator.next();
            listIterator.set(777);
        }
        Object[] expected = {777, 1, 2, 3, 4, null, 6, 7, 8, 9};
        Object[] actual = doublyLinkedList.toArray();
        assertEquals("ListIteratorImplTest.testSet", expected, actual);
    }

    public static void testSetConcurrentModify() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator1 = doublyLinkedList.listIterator();
        ListIterator<Integer> listIterator2 = doublyLinkedList.listIterator();
        if (listIterator2.hasNext()) {
            listIterator2.next();
            listIterator2.set(777);
        }
        try {
            if (listIterator1.hasNext()) {
                listIterator1.next();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorImplTest.testSetConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorImplTest.testSetConcurrentModify");
    }

    public static void testAdd() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator = doublyLinkedList.listIterator();
        listIterator.add(777);
        Object[] expected = {777, 0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("ListIteratorTest.testAdd", expected, doublyLinkedList.toArray());
    }

    public static void testAddConcurrentModify() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoubleLinkedList();
        ListIterator<Integer> listIterator1 = doublyLinkedList.listIterator();
        ListIterator<Integer> listIterator2 = doublyLinkedList.listIterator();
        listIterator2.add(777);
        try {
            listIterator1.add(888);
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorImplTest.testAddConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorImplTest.testAddConcurrentModify");
    }

    private static DoublyLinkedList<Integer> getDoubleLinkedList() {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                doublyLinkedList.add(null);
            } else {
                doublyLinkedList.add(i);
            }
        }
        return doublyLinkedList;
    }

    private static ListIterator getListIteratorImpl(DoublyLinkedList doublyLinkedList, int cursor) {
        return doublyLinkedList.listIterator(cursor);
    }

}
