package com.getjavajob.training.algo1701.reusk.lesson06;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class LinkedHashMapTest {

    public static void main(String[] args) {
        testPutOrder();
    }

    public static void testPutOrder() {
        Map<Integer, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(1, "ONE");
        linkedHashMap.put(2, "TWO");
        linkedHashMap.put(3, "THREE");
        Iterator<Integer> iterator = linkedHashMap.keySet().iterator();
        int[] actual = new int[linkedHashMap.size()];
        int counter = 0;
        while (iterator.hasNext()) {
            actual[counter++] = iterator.next();
        }
        int[] expected = {1, 2, 3};
        assertEquals("LinkedHashMapTest.testPutOrder", expected, actual);
    }
}
