package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class DequeTest {

    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testPollFirst();
        testPollLast();
        testGetFirst();
        testGetLast();
        testPeekFirst();
        testPeekLast();
        testRemoveFirstOccurrence();
        testRemoveLastOccurrence();
        testAdd();
        testOffer();
        testRemove();
        testPoll();
        testElement();
        testPeek();
        testPush();
        testPop();
        testContainsTrue();
        testContainsFalse();
        testSize();
        testIterator();
        testDescendingIterator();
    }

    public static void testAddFirst() {
        Deque<Integer> deque = getArrayDeque();
        deque.addFirst(777);
        assertEquals("DequeTest.testAddFirst", 777, (int) deque.peek());
    }

    public static void testAddLast() {
        Deque<Integer> deque = getArrayDeque();
        deque.addLast(777);
        List<Integer> actual = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            actual.add(deque.remove());
        }
        List<Integer> expected = getList();
        expected.add(777);
        assertEquals("DequeTest.testAddLast", expected, actual);
    }

    public static void testOfferFirst() {
        Deque<Integer> deque = getArrayDeque();
        deque.offerFirst(777);
        assertEquals("DequeTest.testOfferFirst", 777, (int) deque.peek());
    }

    public static void testOfferLast() {
        Deque<Integer> deque = getArrayDeque();
        deque.offerLast(777);
        List<Integer> actual = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            actual.add(deque.remove());
        }
        List<Integer> expected = getList();
        expected.add(777);
        assertEquals("DequeTest.testOfferLast", expected, actual);
    }

    public static void testRemoveFirst() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testRemoveFirst", 0, (int) deque.removeFirst());
    }

    public static void testRemoveLast() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testRemoveLast", 9, (int) deque.removeLast());
    }

    public static void testPollFirst() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testPollFirst", 0, (int) deque.pollFirst());
    }

    public static void testPollLast() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testPollLast", 9, (int) deque.pollLast());
    }

    public static void testGetFirst() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testGetFirst", 0, (int) deque.getFirst());
    }

    public static void testGetLast() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testGetLast", 9, (int) deque.getLast());
    }

    public static void testPeekFirst() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testPeekFirst", 0, (int) deque.peekFirst());
    }

    public static void testPeekLast() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testPeekLast", 9, (int) deque.peekLast());
    }

    public static void testRemoveFirstOccurrence() {
        Deque<Integer> deque = getArrayDeque();
        deque.removeFirstOccurrence(5);
        List<Integer> actual = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            actual.add(deque.remove());
        }
        List<Integer> expected = getList();
        expected.remove(5);
        assertEquals("DequeTest.testRemoveFirstOccurence", expected, actual);
    }

    public static void testRemoveLastOccurrence() {
        Deque<Integer> deque = getArrayDeque();
        deque.removeLastOccurrence(5);
        List<Integer> actual = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            actual.add(deque.remove());
        }
        List<Integer> expected = getList();
        expected.remove(5);
        assertEquals("DequeTest.testRemoveLastOccurence", expected, actual);
    }

    public static void testAdd() {
        Deque<Integer> deque = getArrayDeque();
        deque.add(777);
        List<Integer> actual = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            actual.add(deque.remove());
        }
        List<Integer> expected = getList();
        expected.add(777);
        assertEquals("DequeTest.testAdd", expected, actual);
    }

    public static void testOffer() {
        Deque<Integer> deque = getArrayDeque();
        deque.offer(777);
        List<Integer> actual = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            actual.add(deque.remove());
        }
        List<Integer> expected = getList();
        expected.add(777);
        assertEquals("DequeTest.testOffer", expected, actual);
    }

    public static void testRemove() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testGet", 0, (int) deque.remove());
    }

    public static void testPoll() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testPoll", 0, (int) deque.poll());
    }

    public static void testElement() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testElement", 0, (int) deque.element());
    }

    public static void testPeek() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testPeek", 0, (int) deque.peek());
    }

    public static void testPush() {
        Deque<Integer> deque = getArrayDeque();
        deque.push(777);
        assertEquals("DequeTest.testPush", 777, (int) deque.peek());
    }

    public static void testPop() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testPop", 0, (int) deque.pop());
    }

    public static void testContainsTrue() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testContainsTrue", true, deque.contains(5));
    }

    public static void testContainsFalse() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testContainsFalse", false, deque.contains(777));
    }

    public static void testSize() {
        Deque<Integer> deque = getArrayDeque();
        assertEquals("DequeTest.testSize", 10, deque.size());
    }

    public static void testIterator() {
        Deque<Integer> deque = getArrayDeque();
        Iterator iterator = deque.iterator();
        List<Integer> actual = new ArrayList<>();
        while (iterator.hasNext()) {
            actual.add((int) iterator.next());
        }
        List<Integer> expected = getList();
        assertEquals("DequeTest.testIterator", expected, actual);
    }

    public static void testDescendingIterator() {
        Deque<Integer> deque = getArrayDeque();
        Iterator iterator = deque.descendingIterator();
        List<Integer> actual = new ArrayList<>();
        while (iterator.hasNext()) {
            actual.add((int) iterator.next());
        }
        List<Integer> expected = new ArrayList<>();
        for (int i = 9; i >= 0; i--) {
            expected.add(i);
        }
        assertEquals("DequeTest.testDescendingIterator", expected, actual);
    }

    private static Deque<Integer> getArrayDeque() {
        Deque<Integer> arrayDeque = new ArrayDeque<>();
        for (int i = 0; i < 10; i++) {
            arrayDeque.add(i);
        }
        return arrayDeque;
    }

    private static List<Integer> getList() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        return list;
    }

}
