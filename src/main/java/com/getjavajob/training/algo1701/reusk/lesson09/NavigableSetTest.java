package com.getjavajob.training.algo1701.reusk.lesson09;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class NavigableSetTest {

    public static void main(String[] args) {
        testLower();
        testLowerNullPointerException();
        testFloor();
        testFloorNull();
        testFloorNullPointerException();
        testCeiling();
        testCeilingNull();
        testCeilingNullPointerException();
        testHigher();
        testHigherNull();
        testHigherNullPointerException();
        testPollFirst();
        testPollFirstNull();
        testPollLast();
        testPollLastNull();
        testIterator();
        testDescendingSet();
        testDescendingIterator();
        testSubsetNonInclusive();
        testSubsetFromInclusive();
        testSubsetFromToInclusive();
        testSubsetToInclusive();
        testSubsetNullPointerException();
        testSubsetIllegalArgumentException();
        testHeadSetInclusive();
        testHeadSetNonInclusive();
        testHeadsetNullPointerException();
        testHeadsetIllegalArgumentException();
        testTailSetInclusive();
        testTailSetNonInclusive();
        testTailsetNullPointerException();
        testTailsetIllegalArgumentException();
    }

    public static void testLower() {
        NavigableSet<Integer> set = getNavigableSet();
        assertEquals("NavigableSetTest.testLower", 7, (int) set.lower(8));
    }

    public static void testLowerNullPointerException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.lower(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableSetTest.testLowerNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableSetTest.testLowerNullPointerException");
    }

    public static void testFloor() {
        NavigableSet<Integer> set = getNavigableSet();
        assertEquals("NavigableSetTest.testFloor", 5, (int) set.floor(5));
    }

    public static void testFloorNull() {
        NavigableSet<Integer> set = new TreeSet<>();
        assertEquals("NavigableSetTest.testFloorNull", null, set.floor(5));
    }

    public static void testFloorNullPointerException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.floor(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableSetTest.testFloorNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableSetTest.testFloorNullPointerException");
    }

    public static void testCeiling() {
        NavigableSet<Integer> set = getNavigableSet();
        assertEquals("NavigableSetTest.testCeiling", 5, (int) set.floor(5));
    }

    public static void testCeilingNull() {
        NavigableSet<Integer> set = new TreeSet<>();
        assertEquals("NavigableSetTest.testCeilingNull", null, set.ceiling(5));
    }

    public static void testCeilingNullPointerException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.ceiling(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableSetTest.testCeilingNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableSetTest.testCeilingNullPointerException");
    }

    public static void testHigher() {
        NavigableSet<Integer> set = getNavigableSet();
        assertEquals("NavigableSetTest.testHigher", 5, (int) set.higher(4));
    }

    public static void testHigherNull() {
        NavigableSet<Integer> set = new TreeSet<>();
        assertEquals("NavigableSetTest.testHigherNull", null, set.higher(9));
    }

    public static void testHigherNullPointerException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.higher(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableSetTest.testHigherNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableSetTest.testHigherNullPointerException");
    }

    public static void testPollFirst() {
        NavigableSet<Integer> set = getNavigableSet();
        assertEquals("NavigableSetTest.testPollFirst", 0, (int) set.pollFirst());
    }

    public static void testPollFirstNull() {
        NavigableSet<Integer> set = new TreeSet<>();
        assertEquals("NavigableSetTest.testPollFirstNull", null, set.pollFirst());
    }

    public static void testPollLast() {
        NavigableSet<Integer> set = getNavigableSet();
        assertEquals("NavigableSetTest.testPollLast", 9, (int) set.pollLast());
    }

    public static void testPollLastNull() {
        NavigableSet<Integer> set = new TreeSet<>();
        assertEquals("NavigableSetTest.testPollLastNull", null, set.pollLast());
    }

    public static void testIterator() {
        NavigableSet<Integer> set = getNavigableSet();
        Iterator<Integer> iterator = set.iterator();
        int[] actual = new int[set.size()];
        int counter = 0;
        while (iterator.hasNext()) {
            actual[counter++] = iterator.next();
        }
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertEquals("NavigableSetTest.testIterator", expected, actual);
    }

    public static void testDescendingSet() {
        NavigableSet<Integer> set = getNavigableSet();
        int[] expected = new int[set.size()];
        int counter = 0;
        for (int i = 9; i >= 0; i--) {
            expected[counter++] = i;
        }
        int[] actual = new int[set.size()];
        Iterator<Integer> iterator = set.descendingSet().iterator();
        counter = 0;
        while (iterator.hasNext()) {
            actual[counter++] = iterator.next();
        }
        assertEquals("NavigableSetTest.testDescendingSet", expected, actual);
    }

    public static void testDescendingIterator() {
        NavigableSet<Integer> set = getNavigableSet();
        int[] expected = new int[set.size()];
        int counter = 0;
        for (int i = 9; i >= 0; i--) {
            expected[counter++] = i;
        }
        Iterator<Integer> iterator = set.descendingIterator();
        int[] actual = new int[set.size()];
        counter = 0;
        while (iterator.hasNext()) {
            actual[counter++] = iterator.next();
        }
        assertEquals("NavigableSetTest.testDescendingIterator", expected, actual);
    }

    public static void testSubsetNonInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(1);
        expected.add(2);
        assertEquals("NavigableSetTest.testSubsetNonInclusive", expected, set.subSet(0, false, 3, false));
    }

    public static void testSubsetFromInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(0);
        expected.add(1);
        expected.add(2);
        assertEquals("NavigableSetTest.testSubsetFromInclusive", expected, set.subSet(0, true, 3, false));
    }

    public static void testSubsetFromToInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(0);
        expected.add(1);
        expected.add(2);
        expected.add(3);
        assertEquals("NavigableSetTest.testSubsetFromToInclusive", expected, set.subSet(0, true, 3, true));
    }

    public static void testSubsetToInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        assertEquals("NavigableSetTest.testSubsetToInclusive", expected, set.subSet(0, false, 3, true));
    }

    public static void testSubsetNullPointerException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.subSet(null, true, null, true);
        } catch (NullPointerException e) {
            assertEquals("NavigableSetTest.testSubsetNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableSetTest.testSubsetNullPointerException");
    }

    public static void testSubsetIllegalArgumentException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.subSet(5, true, 0, true);
        } catch (IllegalArgumentException e) {
            assertEquals("NavigableSetTest.testSubsetIllegalArgumentException", "fromKey > toKey", e.getMessage());
            return;
        }
        fail("NavigableSetTest.testSubsetIllegalArgumentException");
    }

    public static void testHeadSetInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(0);
        expected.add(1);
        expected.add(2);
        assertEquals("NavigableSetTest.testHeadSetInclusive", expected, set.headSet(2, true));
    }

    public static void testHeadSetNonInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(0);
        expected.add(1);
        assertEquals("NavigableSetTest.testHeadSetNonInclusive", expected, set.headSet(2, false));
    }

    public static void testHeadsetNullPointerException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.headSet(null, true);
        } catch (NullPointerException e) {
            assertEquals("NavigableSetTest.testHeadsetNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableSetTest.testHeadsetNullPointerException");
    }

    public static void testHeadsetIllegalArgumentException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            SortedSet<Integer> subset = set.subSet(0, 5);
            subset.headSet(100);
        } catch (IllegalArgumentException e) {
            assertEquals("NavigableSetTest.testHeadsetIllegalArgumentException", "toKey out of range", e.getMessage());
            return;
        }
        fail("NavigableSetTest.testHeadsetIllegalArgumentException");
    }

    public static void testTailSetInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(7);
        expected.add(8);
        expected.add(9);
        assertEquals("NavigableSetTest.testTailSetInclusive", expected, set.tailSet(7, true));
    }

    public static void testTailSetNonInclusive() {
        NavigableSet<Integer> set = getNavigableSet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(8);
        expected.add(9);
        assertEquals("NavigableSetTest.testTailSetNonInclusive", expected, set.tailSet(7, false));
    }

    public static void testTailsetNullPointerException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            set.tailSet(null, true);
        } catch (NullPointerException e) {
            assertEquals("NavigableSetTest.testTailsetNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableSetTest.testTailsetNullPointerException");
    }

    public static void testTailsetIllegalArgumentException() {
        NavigableSet<Integer> set = getNavigableSet();
        try {
            SortedSet<Integer> subset = set.subSet(0, 5);
            subset.tailSet(100);
        } catch (IllegalArgumentException e) {
            assertEquals("NavigableSetTest.testTailsetIllegalArgumentException", "fromKey out of range", e.getMessage());
            return;
        }
        fail("NavigableSetTest.testTailsetIllegalArgumentException");
    }

    private static NavigableSet<Integer> getNavigableSet() {
        NavigableSet<Integer> set = new TreeSet<>();
        for (int i = 9; i >= 0; i--) {
            set.add(i);
        }
        return set;
    }
}
