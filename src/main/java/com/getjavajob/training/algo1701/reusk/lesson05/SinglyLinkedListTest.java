package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class SinglyLinkedListTest {

    public static void main(String[] args) {
        testAdd();
        testGetNullValue();
        testGetValue();
    }

    public static void testAdd() {
        SinglyLinkedList singlyLinkedList = getSinglyLinkedList();
        singlyLinkedList.add("added");
        List expected = getList(0, 1, 2, 3, 4, null, 6, 7, 8, 9, "added");
        assertEquals("SinglyLinkedListTest.testAdd", expected, singlyLinkedList.asList());
    }

    public static void testGetNullValue() {
        SinglyLinkedList singlyLinkedList = getSinglyLinkedList();
        assertEquals("SinglyLinkedListTest.testGetNullValue", null, singlyLinkedList.get(5));
    }

    public static void testGetValue() {
        SinglyLinkedList singlyLinkedList = getSinglyLinkedList();
        assertEquals("SinglyLinkedListTest.testGetValue", 3, singlyLinkedList.get(3));
    }

    public static SinglyLinkedList<Integer> getSinglyLinkedList() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                singlyLinkedList.add(null);
            } else {
                singlyLinkedList.add(i);
            }
        }
        return singlyLinkedList;
    }

    private static List getList(Object... objects) {
        List<Object> result = new ArrayList();
        for (Object o : objects) {
            result.add(o);
        }
        return result;
    }

}
