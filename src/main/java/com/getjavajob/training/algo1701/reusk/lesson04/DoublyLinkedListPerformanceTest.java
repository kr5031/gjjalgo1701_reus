package com.getjavajob.training.algo1701.reusk.lesson04;

import org.omg.PortableInterceptor.INACTIVE;

import static com.getjavajob.training.algo1701.reusk.util.StopWatch.start;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.finish;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.printElapsedTime;

import java.util.LinkedList;
import java.util.List;

public class DoublyLinkedListPerformanceTest {
    private static final int NUMBER_OF_ELEMENTS_ADD_BEGINNING = 5_000_000;
    private static final int NUMBER_OF_ELEMENTS_ADD_MIDDLE = 60_000;
    private static final int NUMBER_OF_ELEMENTS_ADD_END = 15_000_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_BEGINNING = 40_000_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_MIDDLE = 90_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_END = 10_000_000;

    private static void testDoublyLinkedListAddToBeginning() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        System.out.println("-------- Addition to the beginning --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_BEGINNING; i++) {
            doublyLinkedList.add(0, i);
        }
        finish();
        printElapsedTime("DoublyLinkedList.add");
    }

    private static void testLinkedListAddToBeginning() {
        List<Integer> linkedList = getLinkedList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_BEGINNING; i++) {
            linkedList.add(0, i);
        }
        finish();
        printElapsedTime("LinkedList.add");
    }

    private static void testDoublyLinkedListAddToMiddle() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        System.out.println("-------- Addition to the middle --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_MIDDLE; i++) {
            doublyLinkedList.add(doublyLinkedList.size() / 2, i);
        }
        finish();
        printElapsedTime("DoublyLinkedList.add");
    }

    private static void testLinkedListAddToMiddle() {
        List<Integer> linkedList = getLinkedList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_MIDDLE; i++) {
            linkedList.add(linkedList.size() / 2, i);
        }
        finish();
        printElapsedTime("LinkedList.add");
    }

    private static void testDoublyLinkedListAddToEnd() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        System.out.println("-------- Addition to the end --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_END; i++) {
            doublyLinkedList.add(i);
        }
        finish();
        printElapsedTime("DoublyLinkedList.add");
    }

    private static void testLinkedListAddToEnd() {
        List<Integer> linkedList = getLinkedList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_END; i++) {
            linkedList.add(i);
        }
        finish();
        printElapsedTime("LinkedList.add");
    }


    private static void testDoublyLinkedListRemoveBeginning() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList(NUMBER_OF_ELEMENTS_REMOVE_BEGINNING);
        System.out.println("-------- Remove from the beginning --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            doublyLinkedList.remove(0);
        }
        finish();
        printElapsedTime("DoublyLinkedList.remove");
    }

    private static void testLinkedListRemoveBeginning() {
        List<Integer> linkedList = getLinkedList(NUMBER_OF_ELEMENTS_REMOVE_BEGINNING);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            linkedList.remove(0);
        }
        finish();
        printElapsedTime("LinkedList.remove");
    }

    private static void testDoublyLinkedListRemoveMiddle() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList(NUMBER_OF_ELEMENTS_REMOVE_MIDDLE);
        System.out.println("-------- Remove from the middle --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            doublyLinkedList.remove(doublyLinkedList.size() / 2);
        }
        finish();
        printElapsedTime("DoublyLinkedList.remove");
    }

    private static void testLinkedListRemoveMiddle() {
        List<Integer> linkedList = getLinkedList(NUMBER_OF_ELEMENTS_REMOVE_MIDDLE);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            linkedList.remove(linkedList.size() / 2);
        }
        finish();
        printElapsedTime("LinkedList.remove");
    }

    private static void testDoublyLinkedListRemoveEnd() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList(NUMBER_OF_ELEMENTS_REMOVE_END);
        System.out.println("-------- Remove from the end --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            doublyLinkedList.remove(doublyLinkedList.size() - 1);
        }
        finish();
        printElapsedTime("DoublyLinkedList.remove");
    }

    private static void testLinkedListRemoveEnd() {
        List<Integer> linkedList = getLinkedList(NUMBER_OF_ELEMENTS_REMOVE_END);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            linkedList.remove(linkedList.size() - 1);
        }
        finish();
        printElapsedTime("LinkedList.remove");
    }

    private static DoublyLinkedList<Integer> getDoublyLinkedList() {  // 0, 1, 2, 3, 4, null, 6, 7, 8, 9
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                doublyLinkedList.add(null);
            } else {
                doublyLinkedList.add(i);
            }
        }
        return doublyLinkedList;
    }

    private static DoublyLinkedList<Integer> getDoublyLinkedList(int number) {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();
        for (int i = 0; i < number; i++) {
            doublyLinkedList.add(i);
        }
        return doublyLinkedList;
    }

    private static List<Integer> getLinkedList() {  // 0, 1, 2, 3, 4, null, 6, 7, 8, 9
        List<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                linkedList.add(null);
            } else {
                linkedList.add(i);
            }
        }
        return linkedList;
    }

    private static List<Integer> getLinkedList(int number) {
        List<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < number; i++) {
            linkedList.add(i);
        }
        return linkedList;
    }

}