package com.getjavajob.training.algo1701.reusk.lesson06;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class LinkedHashSetTest {

    public static void main(String[] args) {
        testAddOrder();
    }

    public static void testAddOrder() {
        Set<Integer> linkedHashSet = new LinkedHashSet();
        for (int i = 0; i < 10; i++) {
            linkedHashSet.add(i);
        }
        int[] actual = new int[linkedHashSet.size()];
        Iterator<Integer> iterator = linkedHashSet.iterator();
        int counter = 0;
        while (iterator.hasNext()) {
            actual[counter++] = iterator.next();
        }
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertEquals("LinkedHashSetTest.testAddOrder", expected, actual);
    }

}