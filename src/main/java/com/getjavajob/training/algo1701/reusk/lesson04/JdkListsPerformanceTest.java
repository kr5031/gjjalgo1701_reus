package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo1701.reusk.util.StopWatch.printElapsedTime;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.start;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.finish;

public class JdkListsPerformanceTest {
    private static final int NUMBER_OF_ELEMENTS_ADD_BEGINNING = 110_000;
    private static final int NUMBER_OF_ELEMENTS_ADD_MIDDLE = 50_000;
    private static final int NUMBER_OF_ELEMENTS_ADD_END = 14_000_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_BEGINNING = 150_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_MIDDLE = 70_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_END = 10_000_000;

    public static void testArrayListAddBegin() {
        List<Integer> arrayList = getArrayList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_BEGINNING; i++) {
            arrayList.add(0, i);
        }
        finish();
        printElapsedTime("ArrayList add to begin");
    }

    public static void testLinkedListAddBegin() {
        List<Integer> linkedList = getLinkedList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_BEGINNING; i++) {
            linkedList.add(0, i);
        }
        finish();
        printElapsedTime("LinkedList add to begin");
    }

    public static void testArrayListAddMiddle() {
        List<Integer> arrayList = getArrayList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_MIDDLE; i++) {
            arrayList.add(arrayList.size() / 2, i);
        }
        finish();
        printElapsedTime("ArrayList add middle");
    }

    public static void testLinkedListAddMiddle() {
        List<Integer> linkedList = getLinkedList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_MIDDLE; i++) {
            linkedList.add(linkedList.size() / 2, i);
        }
        finish();
        printElapsedTime("LinkedList add to middle");
    }

    public static void testArrayListAddEnd() {
        List<Integer> arrayList = getArrayList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_END; i++) {
            arrayList.add(i);
        }
        finish();
        printElapsedTime("ArrayList add to end");
    }

    public static void testLinkedListAddToEnd() {
        List<Integer> linkedList = getLinkedList();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_END; i++) {
            linkedList.add(i);
        }
        finish();
        printElapsedTime("Linked list add to end");
    }

    public static void testArrayListRemoveBegin() {
        List<Integer> arrayList = getArrayList(NUMBER_OF_ELEMENTS_REMOVE_BEGINNING);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            arrayList.remove(0);
        }
        finish();
        printElapsedTime("ArrayList remove from beggining");
    }

    public static void testLinkedListRemoveBegin() {
        List<Integer> linkedList = getLinkedList(NUMBER_OF_ELEMENTS_REMOVE_BEGINNING);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            linkedList.remove(0);
        }
        finish();
        printElapsedTime("Linked list remove from beginning");
    }

    public static void testArrayListRemoveMiddle() {
        List<Integer> arrayList = getArrayList(NUMBER_OF_ELEMENTS_REMOVE_MIDDLE);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            arrayList.remove(arrayList.size() / 2);
        }
        finish();
        printElapsedTime("ArrayList remove middle");
    }

    public static void testLinkedListRemoveMiddle() {
        List<Integer> linkedList = getLinkedList(NUMBER_OF_ELEMENTS_REMOVE_MIDDLE);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            linkedList.remove(linkedList.size() / 2);
        }
        finish();
        printElapsedTime("LinkedList remove middle");
    }

    public static void testArrayListRemoveEnd() {
        List<Integer> arrayList = getArrayList(NUMBER_OF_ELEMENTS_REMOVE_END);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            arrayList.remove(arrayList.size() - 1);
        }
        finish();
        printElapsedTime("ArrayList remove from end");
    }

    public static void testLinkedListRemoveEnd() {
        List<Integer> linkedList = getLinkedList(NUMBER_OF_ELEMENTS_REMOVE_END);
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            linkedList.remove(linkedList.size() - 1);
        }
        finish();
        printElapsedTime("LinkedList remove from end");
    }

    public static List<Integer> getArrayList() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                arrayList.add(null);
            } else {
                arrayList.add(i);
            }
        }
        return arrayList;
    }

    public static List<Integer> getArrayList(int number) {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

    public static List<Integer> getLinkedList() {
        List<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                linkedList.add(null);
            } else {
                linkedList.add(i);
            }
        }
        return linkedList;
    }

    public static List<Integer> getLinkedList(int number) {
        List<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < number; i++) {
            linkedList.add(i);
        }
        return linkedList;
    }

}
