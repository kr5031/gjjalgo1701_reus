package com.getjavajob.training.algo1701.reusk.lesson05;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class LinkedListStackTest {

    public static void main(String[] args) {
        testLinkedListStack();
    }

    public static void testLinkedListStack() {
        Stack<Integer> linkedliststack = new LinkedListStack();
        linkedliststack.push(1);
        linkedliststack.push(2);
        linkedliststack.push(3);
        int[] actual = new int[3];
        for (int i = 0; i < actual.length; i++) {
            actual[i] = linkedliststack.pop();
        }
        int[] expected = {3, 2, 1};
        assertEquals("LinkedListStackTest.testLinkedListStack", expected, actual);
    }

}
