package com.getjavajob.training.algo1701.reusk.lesson08;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;

import java.util.Comparator;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class BinarySearchTreeTest extends BinarySearchTree {

    public static void main(String[] args) {
        testCompareGreater();
        testCompareEqual();
        testCompareLess();
        testTreeSearch();
        testTreeSearchNull();
        testToString();
    }

    public static void testCompareGreater() {
        BinarySearchTree<Integer> tree = getBinarySearchTree();
        assertEquals("BinarySearchTreeTest.testCompareGreater", 1, tree.compare(2, 1));
    }

    public static void testCompareEqual() {
        BinarySearchTree<Integer> tree = getBinarySearchTree();
        assertEquals("BinarySearchTreeTest.testCompareEqual", 0, tree.compare(2, 2));
    }

    public static void testCompareLess() {
        BinarySearchTree<Integer> tree = getBinarySearchTree();
        assertEquals("BinarySearchTreeTest.testCompareLess", -1, tree.compare(1, 2));
    }

    public static void testTreeSearch() {
        BinarySearchTree<Integer> tree = getBinarySearchTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node10 = tree.validate(tree.right(root));
        NodeImpl<Integer> node12 = tree.validate(tree.right(node10));
        assertEquals("BinarySearchTreeTest.testTreeSearch", node12, tree.treeSearch(root, 12));
    }

    public static void testTreeSearchNull() {
        BinarySearchTree<Integer> tree = getBinarySearchTree();
        assertEquals("BinarySearchTreeTest.testTreeSearchNull", null, tree.treeSearch(tree.root(), 13));
    }

    public static void testToString() {
        String expected = "8(6(5,7),10(,12))";
        assertEquals("BinarySearchTreeTest.testToString", expected, getBinarySearchTree().toString());
    }

    /*
                            BST for tests
                               8
                             /   \
                            6     10
                           / \      \
                          5   7      12
     */
    public static BinarySearchTree<Integer> getBinarySearchTree() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        Node<Integer> root = tree.addRoot(8);
        Node<Integer> node6 = tree.addLeft(root, 6);
        Node<Integer> node5 = tree.addLeft(node6, 5);
        Node<Integer> node7 = tree.addRight(node6, 7);
        Node<Integer> node10 = tree.addRight(root, 10);
        Node<Integer> node12 = tree.addRight(node10, 12);
        return tree;
    }

}