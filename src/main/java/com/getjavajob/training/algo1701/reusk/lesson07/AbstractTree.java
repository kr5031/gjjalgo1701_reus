package com.getjavajob.training.algo1701.reusk.lesson07;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.Queue;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return parent(n) == null;
    }

    @Override
    public boolean isEmpty() {
        return root() == null;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Collection<Node<E>> preOrder() {
        Deque<Node<E>> s = new ArrayDeque<>();
        Collection<Node<E>> result = new ArrayList<>();
        Node<E> currentNode = root();
        s.push(currentNode);
        while (!s.isEmpty()) {
            currentNode = s.pop();
            result.add(currentNode);
            Collection<Node<E>> children = children(currentNode);
            for (Node<E> n : children) {
                s.push(n);
            }
        }
        return result;
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Collection<Node<E>> postOrder() {
        Collection<Node<E>> result = new ArrayList<>();
        traversePostOrder(root(), result);
        return result;
    }

    private void traversePostOrder(Node<E> currentNode, Collection<Node<E>> result) {
        if (currentNode == null) {
            return;
        }
        for (Node<E> n : children(currentNode)) {
            traversePostOrder(n, result);
        }
        result.add(currentNode);
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() {
        Queue<Node<E>> q = new ArrayDeque<>();
        Collection<Node<E>> result = new ArrayList<>();
        q.add(root());
        while (!q.isEmpty()) {
            Node<E> currentNode = q.poll();
            result.add(currentNode);
            for (Node<E> n : children(currentNode)) {
                q.add(n);
            }
        }
        return result;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            it.remove();
        }
    }
}
