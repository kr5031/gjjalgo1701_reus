package com.getjavajob.training.algo1701.reusk.lesson07.binary;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;

import java.util.Collection;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    public static final String NODE_NULL_MSG = "Node can not be null";
    public static final String ROOT_NOT_NULL_MSG = "The tree is not empty";
    public static final String WRONG_NODE_TYPE_MSG = "Wrong Node type";
    public static final String CHILDREN_NOT_NULL_MSG = "The node already has a child";
    public static final String CAN_NOT_DELETE_MSG = "The node can not be deleted";
    protected Node<E> root;
    protected int treeSize;

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    public NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        } else if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException(WRONG_NODE_TYPE_MSG);
        }
    }

    // update methods supported by this class

    protected Node<E> createNode(Node<E> parent, E element) {
        return new NodeImpl<>(parent, element);
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root != null) {
            throw new IllegalStateException(ROOT_NOT_NULL_MSG);
        }
        root = createNode(null, e);
        treeSize++;
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        checkNodeForNull(n);
        NodeImpl<E> parent = validate(n);
        if (parent.getLeftChild() == null) {
            return addLeft(n, e);
        } else if (parent.getRightChild() == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException(CHILDREN_NOT_NULL_MSG);
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        checkNodeForNull(n);
        NodeImpl<E> parent = validate(n);
        if (parent.getLeftChild() != null) {
            throw new IllegalArgumentException(CHILDREN_NOT_NULL_MSG);
        } else {
            parent.setLeftChild(createNode(parent, e));
            treeSize++;
            return parent.getLeftChild();
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        checkNodeForNull(n);
        NodeImpl<E> parent = validate(n);
        if (parent.getRightChild() != null) {
            throw new IllegalArgumentException(CHILDREN_NOT_NULL_MSG);
        } else {
            parent.setRightChild(createNode(parent, e));
            treeSize++;
            return parent.getRightChild();
        }
    }

    protected void checkNodeForNull(Node<E> n) {
        if (n == null) {
            throw new IllegalArgumentException(NODE_NULL_MSG);
        }
    }

    protected boolean isLeftChild(NodeImpl<E> parent, NodeImpl<E> child) {
        return parent.getLeftChild() == child;
    }

    protected boolean isRightChild(NodeImpl<E> parent, NodeImpl<E> child) {
        return parent.getRightChild() == child;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        checkNodeForNull(n);
        NodeImpl<E> node = validate(n);
        E toReturn = node.getElement();
        node.setElement(e);
        return toReturn;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        checkNodeForNull(n);
        NodeImpl<E> toRemove = validate(n);
        NodeImpl<E> parentNode = validate(toRemove.getParent());
        NodeImpl<E> childLeft = validate(toRemove.getLeftChild());
        NodeImpl<E> childRight = validate(toRemove.getRightChild());
        if (childrenNumber(toRemove) == 2) {
            throw new IllegalArgumentException(CAN_NOT_DELETE_MSG);
        } else if (isExternal(toRemove)) {
            relinkLeaf(toRemove);
        } else if (childLeft != null) {
            relinkGrandChildren(parentNode, toRemove, validate(childLeft));
        } else if (childRight != null) {
            relinkGrandChildren(parentNode, toRemove, validate(childRight));
        }
        return toRemove.getElement();
    }

    private void relinkLeaf(NodeImpl<E> n) {
        NodeImpl<E> parent = validate(n.getParent());
        if (isLeftChild(parent, n)) {
            parent.setLeftChild(null);
        } else if (isRightChild(parent, n)) {
            parent.setRightChild(null);
        }
        treeSize--;
    }

    private void relinkGrandChildren(NodeImpl<E> grandParent, NodeImpl<E> parent, NodeImpl<E> child) {
        child.setParent(grandParent);
        treeSize--;
        if (parent == root()) {
            root = child;
            return;
        } else if (isLeftChild(grandParent, parent)) {
            grandParent.setLeftChild(child);
        } else if (isRightChild(grandParent, parent)) {
            grandParent.setRightChild(child);
        }
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        checkNodeForNull(p);
        return validate(p).getLeftChild();
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        checkNodeForNull(p);
        return validate(p).getRightChild();
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        Node<E> result = validate(n).getParent();
        return result;
    }

    @Override
    public int size() {
        return treeSize;
    }

    @Override
    public Collection<Node<E>> nodes() {
        return preOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        private Node<E> leftChild;
        private Node<E> rightChild;
        private Node<E> parent;
        private E element;

        public NodeImpl(Node<E> parent, E element) {
            this.parent = parent;
            this.element = element;
        }

        public NodeImpl(E element) {
            this(null, element);
        }

        @Override
        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            NodeImpl<?> node = (NodeImpl<?>) o;
            return element != null ? element.equals(node.element) : node.element == null;
        }

        @Override
        public int hashCode() {
            return element != null ? element.hashCode() : 0;
        }

        public Node<E> getLeftChild() {
            return leftChild;
        }

        public void setLeftChild(Node<E> leftChild) {
            this.leftChild = leftChild;
        }

        public Node<E> getRightChild() {
            return rightChild;
        }

        public void setRightChild(Node<E> rightChild) {
            this.rightChild = rightChild;
        }

        public Node<E> getParent() {
            return parent;
        }

        public void setParent(Node<E> parent) {
            this.parent = parent;
        }
    }

}