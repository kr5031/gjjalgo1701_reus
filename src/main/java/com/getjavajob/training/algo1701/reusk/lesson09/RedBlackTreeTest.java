package com.getjavajob.training.algo1701.reusk.lesson09;

import static com.getjavajob.training.algo1701.reusk.lesson09.RedBlackTree.Color.BLACK;
import static com.getjavajob.training.algo1701.reusk.lesson09.RedBlackTree.Color.RED;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class RedBlackTreeTest extends RedBlackTree {

    public static void main(String[] args) {
        testAfterElementAddedRoot();
        testAfterElementAddedToLeftUncleRed();
        testAfterElementAddedUncleBlackLineLeft();
        testAfterElementAddedUncleBlackLineRight();
        testAfterElementAddedUncleBlackTriangleLeft();
        testAfterElementAddedUncleBlackTriangleRight();
        testAfterElementAddedComplexTree();
        testRemoveLeaf();
        testRemoveBlackWithTwoChildren();
        testRemoveBlackWithOneChild();
        testRemoveRedComplex();
        testRemoveBlackLeaf();
    }

    public static void testAfterElementAddedRoot() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addRoot(100);
        assertEquals("RedBlackTreeTest.testAfterElementAddedRoot", "100b", tree.toString());
    }

    public static void testAfterElementAddedToLeftUncleRed() {
        RedBlackTree<Integer> tree = getRedBlackTreeWithUncleRed();
        NodeImplRB<Integer> node50 = tree.validate(tree.validate(tree.root()).getLeftChild());
        tree.afterElementAdded(tree.add(node50, 60));
        String expected = "100b(50b(,60r),200b)";
        assertEquals("RedBlackTreeTest.testAfterElementAddedToLeftUncleRed", expected, tree.toString());
    }

    public static void testAfterElementAddedUncleBlackLineLeft() {
        RedBlackTree<Integer> tree = getRedBlackTreeWithUncleBlackLeft();
        NodeImplRB<Integer> node5 = tree.validate(tree.validate(tree.root()).getLeftChild());
        tree.afterElementAdded(tree.add(node5, 1));
        String expected = "5b(1r,15r)";
        assertEquals("RedBlackTreeTest.testAfterElementAddedUncleBlackLineLeft", expected, tree.toString());
    }

    public static void testAfterElementAddedUncleBlackLineRight() {
        RedBlackTree<Integer> tree = getRedBlackTreeWithUncleBlackRight();
        NodeImplRB<Integer> node20 = tree.validate(tree.validate(tree.root()).getRightChild());
        tree.afterElementAdded(tree.add(node20, 25));
        String expected = "20b(15r,25r)";
        assertEquals("RedBlackTreeTest.testAfterElementAddedUncleBlackLineRight", expected, tree.toString());
    }

    public static void testAfterElementAddedUncleBlackTriangleLeft() {
        RedBlackTree<Integer> tree = getRedBlackTreeWithUncleBlackLeft();
        NodeImplRB<Integer> node5 = tree.validate(tree.validate(tree.root()).getLeftChild());
        tree.afterElementAdded(tree.add(node5, 6));
        String expected = "6b(5r,15r)";
        assertEquals("RedBlackTreeTest.testAfterElementAddedUncleBlackTriangleLeft", expected, tree.toString());
    }

    public static void testAfterElementAddedUncleBlackTriangleRight() {
        RedBlackTree<Integer> tree = getRedBlackTreeWithUncleBlackRight();
        NodeImplRB<Integer> node20 = tree.validate(tree.validate(tree.root()).getRightChild());
        tree.afterElementAdded(tree.add(node20, 19));
        String expected = "19b(15r,20r)";
        assertEquals("RedBlackTreeTest.testAfterElementAddedUncleBlackTriangleRight", expected, tree.toString());
    }

    public static void testAfterElementAddedComplexTree() {
        RedBlackTree<Integer> tree = getComplexRedBlackTree();
        NodeImplRB<Integer> root = tree.validate(tree.root());
        NodeImplRB<Integer> node5 = tree.validate(root.getLeftChild());
        NodeImplRB<Integer> node15 = tree.validate(root.getRightChild());
        NodeImplRB<Integer> node12 = tree.validate(node15.getLeftChild());
        NodeImplRB<Integer> node19 = tree.validate(node15.getRightChild());
        NodeImplRB<Integer> node23 = tree.validate(node19.getRightChild());
        NodeImplRB<Integer> node9 = tree.validate(node12.getLeftChild());
        NodeImplRB<Integer> node13 = tree.validate(node12.getRightChild());
        tree.afterElementAdded(tree.add(node9, 10));
        String expected = "12b(8r(5b,9b(,10r)),15r(13b,19b(,23r)))";
        assertEquals("RedBlackTreeTest.testAfterElementAddedComplexTree", expected, tree.toString());
    }

    public static void testRemoveLeaf() {
        RedBlackTree<Integer> tree = getComplexRedBlackTree();
        tree.remove(tree.treeSearch(tree.root(), 23));
        String expected = "8b(5b,15r(12b(9r,13r),19b))";
        assertEquals("RedBlackTreeTest.testRemoveLeaf", expected, tree.toString());
    }

    public static void testRemoveBlackWithTwoChildren() {
        RedBlackTree<Integer> tree = getComplexRedBlackTree();
        NodeImplRB<Integer> node12 = tree.validate(tree.treeSearch(tree.root(), 12));
        tree.remove(node12);
        String expected = "8b(5b,15r(9b(,13r),19b(,23r)))";
        assertEquals("RedBlackTreeTest.testRemoveBlackWithTwoChildren", expected, tree.toString());
    }

    public static void testRemoveBlackWithOneChild() {
        RedBlackTree<Integer> tree = getComplexRedBlackTree();
        NodeImplRB<Integer> node19 = tree.validate(tree.treeSearch(tree.root(), 19));
        tree.remove(node19);
        String expected = "8b(5b,15r(12b(9r,13r),23b))";
        assertEquals("RedBlackTreeTest.testRemoveBlackWithOneChild", expected, tree.toString());
    }

    public static void testRemoveRedComplex() {
        RedBlackTree<Integer> tree = getComplexRedBlackTree();
        NodeImplRB<Integer> node15 = tree.validate(tree.treeSearch(tree.root(), 15));
        tree.remove(node15);
        String expected = "8b(5b,13r(12b(9r,),19b(,23r)))";
        assertEquals("RedBlackTreeTest.testRemoveRedComplex", expected, tree.toString());
    }

    public static void testRemoveBlackLeaf() {
        RedBlackTree<Integer> tree = getComplexRedBlackTree();
        NodeImplRB<Integer> node5 = tree.validate(tree.treeSearch(tree.root(), 5));
        tree.remove(node5);
        String expected = "12b(9r,15b(13b,19b(,23r)))";
        assertEquals("RedBlackTreeTest.testRemoveBlackLeaf", expected, tree.toString());
    }

    /*
                                   8 b
                                 /     \
                                5 b     15 r
                                       /   \
                                    12 b   19 b
                                   /    \     \
                                  9 r    13 r  23 r
     */
    private static RedBlackTree<Integer> getComplexRedBlackTree() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        NodeImplRB<Integer> root = rbt.validate(rbt.addRoot(8));
        root.setColor(BLACK);
        NodeImplRB<Integer> node5 = rbt.validate(rbt.addLeft(root, 5));
        node5.setColor(BLACK);
        NodeImplRB<Integer> node15 = rbt.validate(rbt.addRight(root, 15));
        node15.setColor(RED);
        NodeImplRB<Integer> node12 = rbt.validate(rbt.addLeft(node15, 12));
        node12.setColor(BLACK);
        NodeImplRB<Integer> node9 = rbt.validate(rbt.addLeft(node12, 9));
        node9.setColor(RED);
        NodeImplRB<Integer> node13 = rbt.validate(rbt.addRight(node12, 13));
        node13.setColor(RED);
        NodeImplRB<Integer> node19 = rbt.validate(rbt.addRight(node15, 19));
        node19.setColor(BLACK);
        NodeImplRB<Integer> node23 = rbt.validate(rbt.addRight(node19, 23));
        node23.setColor(RED);
        return rbt;
    }

    /*
                        100 b
                      /      \
                    50r       200 r
     */
    private static RedBlackTree<Integer> getRedBlackTreeWithUncleRed() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        NodeImplRB<Integer> root = rbt.validate(rbt.addRoot(100));
        root.setColor(BLACK);
        NodeImplRB<Integer> node50 = rbt.validate(rbt.addLeft(root, 50));
        node50.setColor(RED);
        NodeImplRB<Integer> node200 = rbt.validate(rbt.addRight(root, 200));
        node200.setColor(RED);
        return rbt;
    }

    /*
            15 b
           /
          5 r
     */
    private static RedBlackTree<Integer> getRedBlackTreeWithUncleBlackLeft() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        NodeImplRB<Integer> root = rbt.validate(rbt.addRoot(15));
        root.setColor(BLACK);
        NodeImplRB<Integer> node5 = rbt.validate(rbt.addLeft(root, 5));
        node5.setColor(RED);
        return rbt;
    }

    /*
            15 b
                \
                 20 r

     */
    private static RedBlackTree<Integer> getRedBlackTreeWithUncleBlackRight() {
        RedBlackTree<Integer> rbt = new RedBlackTree<>();
        NodeImplRB<Integer> root = rbt.validate(rbt.addRoot(15));
        root.setColor(BLACK);
        NodeImplRB<Integer> node20 = rbt.validate(rbt.addRight(root, 20));
        node20.setColor(RED);
        return rbt;
    }
}