package com.getjavajob.training.algo1701.reusk.lesson07.binary;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import static java.lang.Math.round;
import static java.lang.Math.log;
import static java.util.Arrays.copyOf;
import static java.util.Map.Entry;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {
    public static final String WRONG_NODE_TYPE_MSG = "Wrong Node type";
    public static final String NODE_NULL_MSG = "Node can not be null";
    public static final String NO_PARENT_MSG = "The node does not have a parent";
    public static final String ROOT_NOT_NULL_MSG = "The tree is not empty";
    public static final String CHILD_NOT_NULL_MSG = "The node already has a child";
    public static final String CAN_NOT_DELETE_MSG = "The node can not be deleted";
    private Node<E>[] elements = new Node[1];
    private int treeSize;
    private int maxElementIndex;

    private void ensureCapacity(int desiredIndex) {
        int depthOfDesiredIndex = (int) round(log(desiredIndex) / log(2));
        int currentHeight = currentHeight();
        if (depthOfDesiredIndex > currentHeight) {
            int newSize = elements.length * 2;
            if (currentHeight == 0) {
                newSize += 2;
            }
            elements = copyOf(elements, newSize);
        }
    }

    private int currentHeight() {
        return (int) round(log(maxElementIndex) / log(2));
    }

    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        } else if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException(WRONG_NODE_TYPE_MSG);
        }
    }

    private int indexOfLeftChild(int i) {
        return 2 * i + 1;
    }

    private int indexOfRightChild(int i) {
        return 2 * i + 2;
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException(NODE_NULL_MSG);
        } else {
            NodeImpl<E> node = validate(p);
            int nodeIndex = node.getIndex();
            return elements[indexOfLeftChild(nodeIndex)];
        }
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException(NODE_NULL_MSG);
        } else {
            NodeImpl<E> node = validate(p);
            int nodeIndex = node.getIndex();
            return elements[indexOfRightChild(nodeIndex)];
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        checkNodeForNull(n);
        NodeImpl<E> parentNode = validate(n);
        int newNodeIndex = indexOfLeftChild(parentNode.getIndex());
        if (newNodeIndex <= elements.length && elements[newNodeIndex] != null) {
            return null;
        }
        ensureCapacity(indexOfLeftChild(parentNode.getIndex()));
        NodeImpl<E> newNode = new NodeImpl<>(e, newNodeIndex);
        elements[newNodeIndex] = newNode;
        if (newNodeIndex > maxElementIndex) {
            maxElementIndex = newNodeIndex;
        }
        treeSize++;
        return newNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        checkNodeForNull(n);
        NodeImpl<E> parentNode = validate(n);
        int newNodeIndex = indexOfRightChild(parentNode.getIndex());
        if (newNodeIndex <= elements.length && elements[newNodeIndex] != null) {
            return null;
        }
        ensureCapacity(indexOfRightChild(parentNode.getIndex()));
        NodeImpl<E> newNode = new NodeImpl<>(e, newNodeIndex);
        elements[newNodeIndex] = newNode;
        if (newNodeIndex > maxElementIndex) {
            maxElementIndex = newNodeIndex;
        }
        treeSize++;
        return newNode;
    }

    private void checkNodeForNull(Node<E> n) {
        if (n == null) {
            throw new IllegalArgumentException(NODE_NULL_MSG);
        }
    }

    private boolean isLeftChild(Node<E> n) {
        NodeImpl<E> node = validate(n);
        int nodeIndex = node.getIndex();
        return (nodeIndex - 1) % 2 == 0;
    }

    private boolean isRightChild(Node<E> n) {
        NodeImpl<E> node = validate(n);
        int nodeIndex = node.getIndex();
        return (nodeIndex - 2) % 2 == 0;
    }

    private int parentIndexForLeftChild(int i) {
        return (i - 1) / 2;
    }

    private int parentIndexForRightChild(int i) {
        return (i - 2) / 2;
    }

    @Override
    public Node<E> root() {
        return elements[0];
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        if (n == root()) {
            throw new IllegalArgumentException(NO_PARENT_MSG);
        }
        NodeImpl<E> node = validate(n);
        if (isLeftChild(node)) {
            return elements[parentIndexForLeftChild(node.getIndex())];
        } else {
            return elements[parentIndexForRightChild(node.getIndex())];
        }
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (elements[0] != null) {
            throw new IllegalStateException(ROOT_NOT_NULL_MSG);
        } else {
            Node<E> toAdd = new NodeImpl<>(e, 0);
            elements[0] = toAdd;
            treeSize++;
            maxElementIndex = 0;
            return toAdd;
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> parentNode = validate(n);
        int rightChildIndex = indexOfRightChild(parentNode.getIndex());
        int leftChildIndex = indexOfLeftChild(parentNode.getIndex());
        if (rightChildIndex >= elements.length) {
            ensureCapacity(rightChildIndex);
        }
        Node<E> rightChildNode = elements[rightChildIndex];
        Node<E> leftChildNode = elements[leftChildIndex];
        if (leftChildNode != null && rightChildNode != null) {
            throw new IllegalArgumentException(CHILD_NOT_NULL_MSG);
        } else if (leftChildNode == null) {
            return addLeft(n, e);
        } else {
            return addRight(n, e);
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        int index = node.getIndex();
        E toReturn = node.getElement();
        node.setElement(e);
        elements[index] = node;
        return toReturn;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        int result = 0;
        if (elements[indexOfLeftChild(node.getIndex())] != null) {
            result++;
        }
        if (elements[indexOfRightChild(node.getIndex())] != null) {
            result++;
        }
        return result;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        if (childrenNumber(n) == 2) {
            throw new IllegalArgumentException(CAN_NOT_DELETE_MSG);
        } else {
            E toReturn = n.getElement();
            NodeImpl<E> node = validate(n);
            if (isExternal(node)) {
                elements[node.getIndex()] = null;
                treeSize--;
                return toReturn;
            } else {
                List<Pair> indices = getOldNewIndicesMap(node);
                for (Pair p : indices) {
                    NodeImpl<E> nodeToMove = validate(elements[p.getOldIndex()]);
                    nodeToMove.setIndex(p.getNewIndex());
                    elements[p.getNewIndex()] = nodeToMove;
                    elements[p.getOldIndex()] = null;
                }
                treeSize--;
            }
            return toReturn;
        }
    }

    private List<Pair> getOldNewIndicesMap(NodeImpl<E> node) {
        int removedElementIndex = node.getIndex();
        int rightChildIndex = indexOfRightChild(removedElementIndex);
        int leftChildIndex = indexOfLeftChild(removedElementIndex);
        List<Pair> indices = new ArrayList<>();
        if (elements[rightChildIndex] != null) {
            indices.add(new Pair(rightChildIndex, removedElementIndex));
        } else if (elements[leftChildIndex] != null) {
            indices.add(new Pair(leftChildIndex, removedElementIndex));
        }
        for (int i = 0; i < indices.size(); i++) {
            Pair entry = indices.get(i);
            int oldIndex = entry.getOldIndex();
            int newIndex = entry.getNewIndex();
            int oldIndexLeft = oldIndex * 2 + 1;
            int oldIndexRight = oldIndex * 2 + 2;
            int newIndexLeft = newIndex * 2 + 1;
            int newIndexRight = newIndex * 2 + 2;
            if (oldIndexLeft < elements.length && elements[oldIndexLeft] != null) {
                indices.add(new Pair(oldIndexLeft, newIndexLeft));
            }
            if (oldIndexRight < elements.length && elements[oldIndexRight] != null) {
                indices.add(new Pair(oldIndexRight, newIndexRight));
            }
        }
        return indices;
    }

    @Override
    public int size() {
        return treeSize;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayBinaryTreeIterator();
    }

    @Override
    public Collection<Node<E>> nodes() {
        Collection<Node<E>> collection = new ArrayList<>();
        for (Node<E> n : elements) {
            if (n != null) {
                collection.add(n);
            }
        }
        return collection;
    }

    protected static class NodeImpl<E> implements Node<E> {
        private E element;
        private int index;

        public NodeImpl(E element, int index) {
            this.element = element;
            this.index = index;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        @Override
        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            NodeImpl<?> node = (NodeImpl<?>) o;

            return element != null ? element.equals(node.element) : node.element == null;
        }

        @Override
        public int hashCode() {
            return element != null ? element.hashCode() : 0;
        }
    }

    private static class Pair {
        private int oldIndex;
        private int newIndex;

        public Pair(int oldIndex, int newIndex) {
            this.oldIndex = oldIndex;
            this.newIndex = newIndex;
        }

        public int getOldIndex() {
            return oldIndex;
        }

        public int getNewIndex() {
            return newIndex;
        }
    }

    private class ArrayBinaryTreeIterator implements Iterator {
        private Queue<E> elementsQueue = new ArrayDeque<>(elements.length);

        public ArrayBinaryTreeIterator() {
            visitTreeBreadthFirst();
        }

        private void visitTreeBreadthFirst() {
            for (int i = 0; i < elements.length; i++) {
                if (elements[i] != null) {
                    elementsQueue.add(validate(elements[i]).getElement());
                }
            }
        }

        @Override
        public boolean hasNext() {
            return !elementsQueue.isEmpty();
        }

        @Override
        public E next() {
            return elementsQueue.poll();
        }
    }

}
