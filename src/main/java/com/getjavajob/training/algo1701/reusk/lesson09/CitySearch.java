package com.getjavajob.training.algo1701.reusk.lesson09;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import static java.lang.Character.MAX_VALUE;
import static java.lang.Character.MIN_VALUE;

import static java.lang.String.CASE_INSENSITIVE_ORDER;
import static java.util.Arrays.asList;

public class CitySearch {
    private static final SortedSet<String> cities = new TreeSet<>(CASE_INSENSITIVE_ORDER);

    public static SortedSet<String> findCities(String input) {
        cities.addAll(asList("Moscow", "Mogilev", "ab" + MAX_VALUE + "cd", "Saint-Petersburg", "Ashkhabad",
                "Murmansk", "Abakan", "monte-Karlo", "Uryupinsk" + MAX_VALUE, "Zaporojye"));
        String query = input.toUpperCase();
        char lastQueryChar = query.charAt(query.length() - 1);
        StringBuilder upperBound = new StringBuilder(query);
        upperBound.deleteCharAt(upperBound.length() - 1);
        if (lastQueryChar == MAX_VALUE) {
            upperBound.append(MIN_VALUE);
        } else {
            upperBound.append((char) ((int) lastQueryChar + 1));
        }
        SortedSet<String> subset = cities.subSet(query, upperBound.toString());
        return subset;
    }
}