package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class SinglyLinkedListTest {

    public static void main(String[] args) {
        testAdd();
        testGetNullValue();
        testGetValue();
        testReverse();
        testAsList();
    }

    public static void testAdd() {
        SinglyLinkedList<Integer> singlyLinkedList = getSinglyLinkedList();
        singlyLinkedList.add(777);
        List<Integer> expected = getList(0, 1, 2, 3, 4, null, 6, 7, 8, 9, 777);
        assertEquals("SinglyLinkedListTest.testAdd", expected, singlyLinkedList.asList());
    }

    public static void testGetNullValue() {
        SinglyLinkedList<Integer> singlyLinkedList = getSinglyLinkedList();
        assertEquals("SinglyLinkedListTest.testGetNullValue", null, singlyLinkedList.get(5));
    }

    public static void testGetValue() {
        SinglyLinkedList<Integer> singlyLinkedList = getSinglyLinkedList();
        assertEquals("SinglyLinkedListTest.testGetValue", 3, (int) singlyLinkedList.get(3));
    }

    public static void testReverse() {
        SinglyLinkedList<Integer> singlyLinkedList = getSinglyLinkedList();
        singlyLinkedList.reverse();
        List<Integer> expected = getReversedList();
        assertEquals("SinglyLinkedListTest.testReverse", expected, singlyLinkedList.asList());
    }

    public static void testAsList() {
        SinglyLinkedList<Integer> singlyLinkedList = getSinglyLinkedList();
        List<Integer> expected = getArrayList();
        assertEquals("SinglyLinkedListTest.testAsList", expected, singlyLinkedList.asList());
    }

    public static SinglyLinkedList<Integer> getSinglyLinkedList() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                singlyLinkedList.add(null);
            } else {
                singlyLinkedList.add(i);
            }
        }
        return singlyLinkedList;
    }

    public static List<Integer> getArrayList() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                arrayList.add(null);
            } else {
                arrayList.add(i);
            }
        }
        return arrayList;
    }

    public static List<Integer> getReversedList() {
        List<Integer> list = new ArrayList<>();
        for (int i = 9; i >= 0; i--) {
            if (i == 5) {
                list.add(null);
            } else {
                list.add(i);
            }
        }
        return list;
    }

    private static List getList(Object... objects) {
        List result = new ArrayList();
        for (Object o : objects) {
            result.add(o);
        }
        return result;
    }

}
