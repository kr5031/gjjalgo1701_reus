package com.getjavajob.training.algo1701.reusk.lesson08;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;
import com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

//extend the tested class to get access to the protected NodeImpl class
public class BalanceableTreeTest<E extends Comparable> extends BalanceableTree<E> {
    public static void main(String[] args) {
        testLeftRelink();
        testRightRelinkWithRoot();
        testRightRelink();
        testLeftRelinkWithRoot();
        testReduceSubtreeHeightLeftSubtreeCoincide();
        testReduceSubtreeHeightLeftSubtreeCoincideReturn();
        testReduceSubtreeHeightLeftSubtreeNotCoincide();
        testReduceSubtreeHeightLeftSubtreeNotCoincideReturn();
        testReduceSubtreeHeightRightSubtreeCoincide();
        testReduceSubtreeHeightRightSubtreeCoincideReturn();
        testReduceSubtreeHeightRightSubtreeNotCoincide();
        testReduceSubtreeHeightRightSubtreeNotCoincideReturn();
    }

    public static void testLeftRelink() {
        BalanceableTree<Integer> tree = getBalanceableTree();
        String expected = "8(7(6(5,),),10(,12))";
        NodeImpl<Integer> newChild = tree.validate(tree.validate(tree.root()).getLeftChild());
        NodeImpl<Integer> newParent = tree.validate(newChild.getRightChild());
        tree.relink(newParent, newChild, true);
        assertEquals("BalanceableTreeTest.testLeftRelink", expected, tree.toString());
    }

    public static void testRightRelink() {
        BalanceableTree<Integer> tree = getBalanceableTree();
        String expected = "8(5(,6(,7)),10(,12))";
        NodeImpl<Integer> newChild = tree.validate(tree.validate(tree.root()).getLeftChild());
        NodeImpl<Integer> newParent = tree.validate(newChild.getLeftChild());
        tree.relink(newParent, newChild, false);
        assertEquals("BalanceableTreeTest.testRightRelink", expected, tree.toString());
    }

    public static void testRightRelinkWithRoot() {
        BalanceableTree<Integer> tree = getBalanceableTree();
        String expected = "6(5,8(7,10(,12)))";
        NodeImpl<Integer> newChild = tree.validate(tree.root());
        NodeImpl<Integer> newParent = tree.validate(newChild.getLeftChild());
        tree.relink(newParent, newChild, false);
        assertEquals("BalanceableTreeTest.testRightRelinkWithRoot", expected, tree.toString());
    }

    public static void testLeftRelinkWithRoot() {
        BalanceableTree<Integer> tree = getBalanceableTree();
        String expected = "10(8(6(5,7),),12)";
        NodeImpl<Integer> newChild = tree.validate(tree.root());
        NodeImpl<Integer> newParent = tree.validate(newChild.getRightChild());
        tree.relink(newParent, newChild, true);
        assertEquals("BalanceableTreeTest.testLeftRelinkWithRoot", expected, tree.toString());
    }

    public static void testReduceSubtreeHeightLeftSubtreeCoincide() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node8 = tree.validate(tree.addLeft(root, 8));
        NodeImpl<Integer> node7 = tree.validate(tree.addLeft(node8, 7));
        String expected = "8(7,10)";
        tree.reduceSubtreeHeight(node7);
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftSubtreeCoincide", expected, tree.toString());
    }

    public static void testReduceSubtreeHeightLeftSubtreeCoincideReturn() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node8 = tree.validate(tree.addLeft(root, 8));
        NodeImpl<Integer> node7 = tree.validate(tree.addLeft(node8, 7));
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftSubtreeCoincideReturn", 8,
                (int) tree.reduceSubtreeHeight(node7).getElement());
    }

    public static void testReduceSubtreeHeightLeftSubtreeNotCoincide() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node8 = tree.validate(tree.addLeft(root, 8));
        NodeImpl<Integer> node9 = tree.validate(tree.addRight(node8, 9));
        tree.reduceSubtreeHeight(node9);
        String expected = "9(8,10)";
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftSubtreeNotCoincide", expected, tree.toString());
    }

    public static void testReduceSubtreeHeightLeftSubtreeNotCoincideReturn() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node8 = tree.validate(tree.addLeft(root, 8));
        NodeImpl<Integer> node9 = tree.validate(tree.addRight(node8, 9));
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftSubtreeNotCoincideReturn", 9,
                (int) tree.reduceSubtreeHeight(node9).getElement());
    }

    public static void testReduceSubtreeHeightRightSubtreeCoincide() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node20 = tree.validate(tree.addRight(root, 20));
        NodeImpl<Integer> node30 = tree.validate(tree.addRight(node20, 30));
        tree.reduceSubtreeHeight(node30);
        String expected = "20(10,30)";
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightSubtreeCoincide", expected, tree.toString());
    }

    public static void testReduceSubtreeHeightRightSubtreeCoincideReturn() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node20 = tree.validate(tree.addRight(root, 20));
        NodeImpl<Integer> node30 = tree.validate(tree.addRight(node20, 30));
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightSubtreeCoincideReturn",
                20, (int) tree.reduceSubtreeHeight(node30).getElement());
    }

    public static void testReduceSubtreeHeightRightSubtreeNotCoincide() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node20 = tree.validate(tree.addRight(root, 20));
        NodeImpl<Integer> node15 = tree.validate(tree.addLeft(node20, 15));
        tree.reduceSubtreeHeight(node15);
        String expected = "15(10,20)";
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightSubtreeNotCoincide", expected, tree.toString());
    }

    public static void testReduceSubtreeHeightRightSubtreeNotCoincideReturn() {
        BalanceableTree<Integer> tree = new BalanceableTreeInstance<>();
        NodeImpl<Integer> root = tree.validate(tree.addRoot(10));
        NodeImpl<Integer> node20 = tree.validate(tree.addRight(root, 20));
        NodeImpl<Integer> node15 = tree.validate(tree.addLeft(node20, 15));
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightSubtreeNotCoincideReturn", 15,
                (int) tree.reduceSubtreeHeight(node15).getElement());
    }

    /*
                                BST for tests
                                   8
                                 /   \
                                6     10
                               / \      \
                              5   7      12
         */
    public static BalanceableTree<Integer> getBalanceableTree() {
        BalanceableTreeInstance<Integer> tree = new BalanceableTreeInstance<>();
        Node<Integer> root = tree.addRoot(8);
        Node<Integer> node6 = tree.addLeft(root, 6);
        Node<Integer> node5 = tree.addLeft(node6, 5);
        Node<Integer> node7 = tree.addRight(node6, 7);
        Node<Integer> node10 = tree.addRight(root, 10);
        Node<Integer> node12 = tree.addRight(node10, 12);
        return tree;
    }

    private static class BalanceableTreeInstance<E extends Comparable> extends BalanceableTree<E> {
    }
}