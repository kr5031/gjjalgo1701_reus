package com.getjavajob.training.algo1701.reusk.lesson01;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task07.swapBitwiseXOR;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task07.swapBitwiseOR;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task07.swapArithmeticAdd;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task07.swapArithmeticMultiply;

/**
 * Created by KReus on 01.02.2017.
 */
public class Task07Test {

    public static void main(String[] args) {
        testSwapBitwiseXOR();
        testSwapBitwiseOR();
        testSwapArithmeticAdd();
        testSwapArithmeticMultiply();
    }

    public static void testSwapBitwiseXOR() {
        String expected = "Swapped a = 22, b = 77";
        assertEquals("Task07Test.testSwapBitwiseXOR", expected, swapBitwiseXOR(77, 22));
    }

    public static void testSwapBitwiseOR() {
        String expected = "Swapped a = 22, b = 77";
        assertEquals("Task07Test.testSwapBitwiseOR", expected, swapBitwiseOR(77, 22));
    }

    public static void testSwapArithmeticAdd() {
        String expected = "Swapped a = 22, b = 77";
        assertEquals("Task07Test.testSwapArithmeticAdd", expected, swapArithmeticAdd(77, 22));
    }

    public static void testSwapArithmeticMultiply() {
        String expected = "Swapped a = 22, b = 77";
        assertEquals("Task07Test.testSwapArithmeticMultiply", expected, swapArithmeticMultiply(77, 22));
    }

}