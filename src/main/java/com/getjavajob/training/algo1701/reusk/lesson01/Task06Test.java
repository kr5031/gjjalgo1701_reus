package com.getjavajob.training.algo1701.reusk.lesson01;

import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateA;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateB;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateC;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateD;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateE;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateF;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateG;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateH;
import static com.getjavajob.training.algo1701.reusk.lesson01.Task06.calculateI;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class Task06Test {

    public static void main(String[] args) {
        testA();
        testB();
        testC();
        testD();
        testE();
        testF();
        testG();
        testH();
        testI();
    }

    public static void testA() {
        assertEquals("Task06Test.testA", 0b1000, calculateA(3));
    }

    public static void testB() {
        assertEquals("Task06Test.testB", 0b11000, calculateB(3, 4));
    }

    public static void testC() {
        assertEquals("Task06Test.testC", 0b1111000, calculateC(0b1111111, 3));
    }

    public static void testD() {
        assertEquals("Task06Test.testD", 0b10100, calculateD(0b10000, 3));
    }

    public static void testE() {
        assertEquals("Task06Test.testE", 0b10100, calculateE((byte) 0b10000, (byte) 3));
    }

    public static void testF() {
        assertEquals("Task06Test.testF", 0b11111011, calculateF(0b11111111, 3));
    }

    public static void testG() {
        assertEquals("Task06Test.testG", 0b1010, calculateG(0b10101010, 4));
    }

    public static void testH() {
        assertEquals("Task06Test.testH", 0b1, calculateH(0b10101010, 4));
    }

    public static void testI() {
        assertEquals("Task06Test.testI", "00000000000000000000000011111111", calculateI(255));
    }

}