package com.getjavajob.training.algo1701.reusk.lesson05;

public interface Stack<E> {

    void push(E e);

    E pop();

}