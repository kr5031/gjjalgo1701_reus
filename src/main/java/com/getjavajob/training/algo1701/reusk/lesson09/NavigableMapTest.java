package com.getjavajob.training.algo1701.reusk.lesson09;

import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.TreeSet;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class NavigableMapTest {
    public static void main(String[] args) {
        testLowerEntry();
        testLowerEntryNull();
        testLowerEntryNullPointerException();
        testLowerKey();
        testLowerKeyNull();
        testLowerKeyNullPointerException();
        testFloorEntry();
        testFloorEntryNull();
        testFloorEntryNullPointerException();
        testFloorKey();
        testFloorKeyNull();
        testFloorKeyNullPointerException();
        testCeilingEntry();
        testCeilingEntryNull();
        testCeilingEntryNullPointerException();
        testCeilingKey();
        testCeilingKeyNull();
        testCeilingKeyNullPointerException();
        testHigherEntry();
        testHigherEntryNull();
        testHigherEntryNullPointerException();
        testHigherKey();
        testHigherKeyNull();
        testHigherKeyNullPointerException();
        testFirstEntry();
        testFirstEntryNull();
        testLastEntry();
        testLastEntryNull();
        testPollFirstEntry();
        testPollFirstEntryNull();
        testPollLastEntry();
        testPollLastEntryNull();
        testNavigableKeyset();
        testDescendingKeyset();
        testSubmap();
        testSubmapEmpty();
        testSubmapIllegalArgumentException();
        testSubmapNullPointerException();
        testHeadmap();
        testHeadmapIllegalArgumentException();
        testHeadmapNullPointerException();
        testTailmap();
        testTailmapIllegalArgumentException();
        testTailmapNullPointerException();
        testSubmapNonInclusive();
        testSubmapFromInclusive();
        testSubmapToInclusive();
        testSubmapFromToInclusive();
        testHeadmapNonInclusive();
        testHeadmapInclusive();
        testTailmapNonInclusive();
        testTailmapInclusive();
    }

    public static void testLowerEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(1, "one");
        assertEquals("NavigableMapTest.testLowerEntry", expected, map.lowerEntry(3));
    }

    public static void testLowerEntryNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testLowerEntryNull", null, map.lowerEntry(1));
    }

    public static void testLowerEntryNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.lowerEntry(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testLowerEntryNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testLowerEntryNullPointerException");
    }

    public static void testLowerKey() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testLowerKey", 1, (int) map.lowerKey(2));
    }

    public static void testLowerKeyNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testLowerKeyNull", null, map.lowerKey(1));
    }

    public static void testLowerKeyNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.lowerKey(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testLowerKeyNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testLowerKeyNullPointerException");
    }

    public static void testFloorEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(1, "one");
        assertEquals("NavigableMapTest.testFloorEntry", expected, map.floorEntry(2));
    }

    public static void testFloorEntryNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testFloorEntryNull", null, map.floorEntry(0));
    }

    public static void testFloorEntryNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.floorEntry(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testFloorEntryNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testFloorEntryNullPointerException");
    }

    public static void testFloorKey() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testFloorKey", 3, (int) map.floorKey(3));
    }

    public static void testFloorKeyNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testFloorKeyNull", null, map.floorKey(0));
    }

    public static void testFloorKeyNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.floorKey(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testFloorKeyNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testFloorKeyNullPointerException");
    }

    public static void testCeilingEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(3, "three");
        assertEquals("NavigableMapTest.testCeilingEntry", expected, map.ceilingEntry(2));
    }

    public static void testCeilingEntryNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testCeilingEntryNull", null, map.ceilingEntry(8));
    }

    public static void testCeilingEntryNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.ceilingEntry(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testCeilingEntryNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testCeilingEntryNullPointerException");
    }

    public static void testCeilingKey() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testCeilingKey", 3, (int) map.ceilingKey(3));
    }

    public static void testCeilingKeyNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testCeilingKeyNull", null, map.ceilingKey(8));
    }

    public static void testCeilingKeyNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.ceilingKey(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testCeilingKeyNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testCeilingKeyNullPointerException");
    }

    public static void testHigherEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(3, "three");
        assertEquals("NavigableMapTest.testHigherEntry", expected, map.higherEntry(2));
    }

    public static void testHigherEntryNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testHigherEntryNull", null, map.higherEntry(7));
    }

    public static void testHigherEntryNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.higherEntry(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testHigherEntryNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testHigherEntryNullPointerException");
    }

    public static void testHigherKey() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testHigherKey", 3, (int) map.higherKey(2));
    }

    public static void testHigherKeyNull() {
        NavigableMap<Integer, String> map = getNavigableMap();
        assertEquals("NavigableMapTest.testHigherKeyNull", null, map.higherKey(7));
    }

    public static void testHigherKeyNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.higherKey(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testHigherKeyNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testHigherKeyNullPointerException");
    }

    public static void testFirstEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(1, "one");
        assertEquals("NavigableMapTest.testFirstEntry", expected, map.firstEntry());
    }

    public static void testFirstEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        assertEquals("NavigableMapTest.testFirstEntryNull", null, map.firstEntry());
    }

    public static void testLastEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(7, "seven");
        assertEquals("NavigableMapTest.testLastEntry", expected, map.lastEntry());
    }

    public static void testLastEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        assertEquals("NavigableMapTest.testFirstEntryNull", null, map.lastEntry());
    }

    public static void testPollFirstEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(1, "one");
        assertEquals("NavigableMapTest.testPollFirstEntry", expected, map.pollFirstEntry());
    }

    public static void testPollFirstEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        assertEquals("NavigableMapTest.testPollFirstEntryNull", null, map.pollFirstEntry());
    }

    public static void testPollLastEntry() {
        NavigableMap<Integer, String> map = getNavigableMap();
        Map.Entry<Integer, String> expected = new SimpleEntry<>(7, "seven");
        assertEquals("NavigableMapTest.testPollLastEntry", expected, map.pollLastEntry());
    }

    public static void testPollLastEntryNull() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        assertEquals("NavigableMapTest.testPollLastEntryNull", null, map.pollLastEntry());
    }

    public static void testNavigableKeyset() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableSet<Integer> actual = map.navigableKeySet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(1);
        expected.add(3);
        expected.add(5);
        expected.add(7);
        assertEquals("NavigableMapTest.testNavigableKeyset", expected, actual);
    }

    public static void testDescendingKeyset() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableSet<Integer> actual = map.descendingKeySet();
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(7);
        expected.add(5);
        expected.add(3);
        expected.add(1);
        assertEquals("NavigableMapTest.testDescendingKeyset", expected, actual);
    }

    public static void testSubmap() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        assertEquals("NavigableMapTest.testSubmap", expected, map.subMap(0, 2));
    }

    public static void testSubmapEmpty() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        assertEquals("NavigableMapTest.testSubmapEmpty", expected, map.subMap(0, 0));
    }

    public static void testSubmapNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.subMap(null, null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testSubmapNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testSubmapNullPointerException");
    }

    public static void testSubmapIllegalArgumentException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.subMap(2, 0);
        } catch (IllegalArgumentException e) {
            assertEquals("NavigableMapTest.testSubmapIllegalArgumentException", "fromKey > toKey", e.getMessage());
            return;
        }
        fail("NavigableMapTest.testSubmapIllegalArgumentException");
    }

    public static void testHeadmap() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        assertEquals("NavigableMapTest.testHeadmap", expected, map.headMap(2));
    }

    public static void testHeadmapNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.headMap(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testHeadmapNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testHeadmapNullPointerException");
    }

    public static void testHeadmapIllegalArgumentException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        SortedMap<Integer, String> submap = map.subMap(0, 1);
        try {
            submap.headMap(9);
        } catch (IllegalArgumentException e) {
            assertEquals("NavigableMapTest.testHeadmapIllegalArgumentException", "toKey out of range", e.getMessage());
            return;
        }
        fail("NavigableMapTest.testHeadmapIllegalArgumentException");
    }

    public static void testTailmap() {
        NavigableMap<Integer, String> map = getNavigableMap();
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(5, "five");
        expected.put(7, "seven");
        assertEquals("NavigableMapTest.testTailmap", expected, map.tailMap(5));
    }

    public static void testTailmapNullPointerException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        try {
            map.tailMap(null);
        } catch (NullPointerException e) {
            assertEquals("NavigableMapTest.testTailmapNullPointerException", null, e.getMessage());
            return;
        }
        fail("NavigableMapTest.testTailmapNullPointerException");
    }

    public static void testTailmapIllegalArgumentException() {
        NavigableMap<Integer, String> map = getNavigableMap();
        SortedMap<Integer, String> subMap = map.subMap(0, 2);
        try {
            subMap.tailMap(9);
        } catch (IllegalArgumentException e) {
            assertEquals("NavigableMapTest.testTailmapIllegalArgumentException", "fromKey out of range", e.getMessage());
            return;
        }
        fail("NavigableMapTest.testTailmapIllegalArgumentException");
    }

    public static void testSubmapNonInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(3, "three");
        assertEquals("NavigableMapTest.testSubmapNonInclusive", expected, map.subMap(0, false, 5, false));

    }

    public static void testSubmapFromInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(3, "three");
        assertEquals("NavigableMapTest.testSubmapFromInclusive", expected, map.subMap(1, true, 5, false));
    }

    public static void testSubmapToInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(3, "three");
        expected.put(5, "five");
        assertEquals("NavigableMapTest.testSubmapToInclusive", expected, map.subMap(0, false, 5, true));
    }

    public static void testSubmapFromToInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(3, "three");
        expected.put(5, "five");
        assertEquals("NavigableMapTest.testSubmapFromToInclusive", expected, map.subMap(0, true, 5, true));
    }

    public static void testHeadmapNonInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(3, "three");
        assertEquals("NavigableMapTest.testHeadmapNonInclusive", expected, map.headMap(5, false));
    }

    public static void testHeadmapInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(3, "three");
        expected.put(5, "five");
        assertEquals("NavigableMapTest.testHeadmapInclusive", expected, map.headMap(5, true));
    }

    public static void testTailmapNonInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(7, "seven");
        assertEquals("NavigableMapTest.testTailmapNonInclusive", expected, map.tailMap(5, false));
    }

    public static void testTailmapInclusive() {
        NavigableMap<Integer, String> map = getNavigableMap();
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(5, "five");
        expected.put(7, "seven");
        assertEquals("NavigableMapTest.testTailmapInclusive", expected, map.tailMap(5, true));
    }

    public static NavigableMap<Integer, String> getNavigableMap() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(3, "three");
        map.put(5, "five");
        map.put(7, "seven");
        return map;
    }
}
