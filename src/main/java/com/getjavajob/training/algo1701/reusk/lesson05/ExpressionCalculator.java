package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

import static java.lang.Character.isDigit;
import static java.lang.Character.isAlphabetic;
import static java.lang.Integer.parseInt;

public class ExpressionCalculator {
    private static final char[] OPERATORS = {'+', '-', '*', '/'};

    public static void main(String[] args) {
        System.out.println(convertToPostfix("2+3*((16/4)-2)"));
        System.out.println(calculatePostfix("2 3 16 4/2-*+"));
    }

    public static String convertToPostfix(String input) {
        Deque<Character> stack = new ArrayDeque<>();
        StringBuilder postfix = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (isDigit(c) || isAlphabetic(c)) {
                postfix.append(c);
                if (i < input.length() - 1 && !isDigit(input.charAt(i + 1))) {
                    postfix.append(" ");
                }
            } else if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                if (postfix.lastIndexOf(" ") == postfix.length() - 1) {
                    postfix.deleteCharAt(postfix.lastIndexOf(" "));
                }
                boolean isFinished = stack.isEmpty();
                while (!isFinished) {
                    char popped = stack.pop();
                    if (popped != '(') {
                        postfix.append(popped);
                    } else {
                        isFinished = true;
                    }
                }
            } else if (isOperator(c)) {
                if (stack.isEmpty()) {
                    stack.push(c);
                } else {
                    char topStack = stack.peek();
                    if (topStack == '(' || firstOperandHasHigherPriority(c, topStack)) {
                        stack.push(c);
                    } else {
                        postfix.deleteCharAt(postfix.lastIndexOf(" "));
                        boolean isFinished = false;
                        while (!isFinished) {
                            postfix.append(stack.pop());
                            isFinished = stack.isEmpty();
                        }
                        stack.push(c);
                    }
                }
            }
        }
        int indexOfLastOperand = findIndexOfLastOperand(postfix.toString()); //delete space after the last operand
        if (indexOfLastOperand < postfix.length() - 1 && postfix.charAt(indexOfLastOperand + 1) == ' ') {
            postfix.deleteCharAt(indexOfLastOperand + 1);
        }
        while (!stack.isEmpty()) {
            postfix.append(stack.pop());
        }
        return postfix.toString();
    }

    public static int calculatePostfix(String input) {
        Deque<Integer> stack = new ArrayDeque<>();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == ' ') {
                continue;
            }
            if (isDigit(c)) {
                StringBuilder sb = new StringBuilder();
                sb.append(c);
                while (isDigit(input.charAt(i + 1))) {
                    sb.append(input.charAt(++i));
                }
                stack.push(parseInt(sb.toString()));
            } else if (isOperator(c)) {
                try {
                    int operand1 = stack.pop();
                    int operand2 = stack.pop();
                    switch (c) {
                        case '+': {
                            stack.push(operand1 + operand2);
                            break;
                        }
                        case '-': {
                            stack.push(operand2 - operand1);
                            break;
                        }
                        case '*': {
                            stack.push(operand1 * operand2);
                            break;
                        }
                        case '/': {
                            stack.push(operand2 / operand1);
                            break;
                        }
                    }
                } catch (ClassCastException e) {
                    System.out.println("Wrong elements in the stack!");
                }
            }
        }
        return stack.pop();
    }

    private static boolean isOperator(char c) {
        for (int i = 0; i < OPERATORS.length; i++) {
            if (OPERATORS[i] == c) {
                return true;
            }
        }
        return false;
    }

    private static int findIndexOfLastOperand(String input) {
        int result = -1;
        for (int i = 0; i < input.length(); i++) {
            if (isAlphabetic(input.charAt(i)) || isDigit(input.charAt(i))) {
                result = i;
            }
        }
        return result;
    }

    private static boolean firstOperandHasHigherPriority(char operand, char topStack) {
        return (operand == OPERATORS[2] || operand == OPERATORS[3]) &&
                (topStack == OPERATORS[0] || topStack == OPERATORS[1]);
    }

}