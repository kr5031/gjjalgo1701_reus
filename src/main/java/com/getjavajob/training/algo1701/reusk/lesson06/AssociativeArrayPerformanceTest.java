package com.getjavajob.training.algo1701.reusk.lesson06;

import java.util.HashMap;

import static com.getjavajob.training.algo1701.reusk.util.StopWatch.start;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.finish;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.printElapsedTime;

public class AssociativeArrayPerformanceTest {
    public static final int NUMBER_ADD = 15_000_000;
    public static final int NUMBER_GET = 30_000_000;
    public static final int NUMBER_REMOVE = 30_000_000;

    public static void testAddHashMap() {
        start();
        HashMap<Integer, Object> hashMap = getHashMap(NUMBER_ADD);
        finish();
        printElapsedTime("testAddHashMap");
    }

    public static void testAddAA() {
        start();
        AssociativeArray<Integer, Object> aa = getAssociativeArray(NUMBER_ADD);
        finish();
        printElapsedTime("testAddAssociativeArray");
    }

    public static void testGetAA() {
        AssociativeArray<Integer, Object> aa = getAssociativeArray(NUMBER_GET);
        start();
        for (int i = 0; i < NUMBER_GET; i++) {
            aa.get(i);
        }
        finish();
        printElapsedTime("testGetAssociativeArray");
    }

    public static void testGetHashMap() {
        HashMap<Integer, Object> hashMap = getHashMap(NUMBER_GET);
        start();
        for (int i = 0; i < NUMBER_GET; i++) {
            hashMap.get(i);
        }
        finish();
        printElapsedTime("testGetHashMap");
    }

    public static void testRemoveAA() {
        AssociativeArray<Integer, Object> aa = getAssociativeArray(NUMBER_REMOVE);
        start();
        for (int i = 0; i < NUMBER_REMOVE; i++) {
            aa.remove(i);
        }
        finish();
        printElapsedTime("testRemoveAssociativeArray");
    }

    public static void testRemoveHashMap() {
        HashMap<Integer, Object> hashMap = getHashMap(NUMBER_REMOVE);
        start();
        for (int i = 0; i < NUMBER_REMOVE; i++) {
            hashMap.remove(i);
        }
        finish();
        printElapsedTime("testRemoveHashMap");
    }

    private static AssociativeArray<Integer, Object> getAssociativeArray(int n) {
        AssociativeArray<Integer, Object> aa = new AssociativeArray<>();
        for (int i = 0; i < n; i++) {
            aa.add(i, new Object());
        }
        return aa;
    }

    private static HashMap<Integer, Object> getHashMap(int n) {
        HashMap<Integer, Object> hashMap = new HashMap<>();
        for (int i = 0; i < n; i++) {
            hashMap.put(i, new Object());
        }
        return hashMap;
    }

}
