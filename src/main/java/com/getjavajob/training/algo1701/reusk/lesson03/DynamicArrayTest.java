package com.getjavajob.training.algo1701.reusk.lesson03;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class DynamicArrayTest {
    private static final String WRONG_INDEX_MSG = "Wrong index #";

    public static void main(String[] args) {
        testBooleanAdd();
        testVoidAddBeggining();
        testVoidAddMiddle();
        testVoidAddEnd();
        testCheckIndex();
        testGet();
        testRemoveBeginning();
        testRemoveMiddle();
        testRemoveEnd();
        testSize();
        testIndexOfExist();
        testIndexOfDontExist();
        testContainsTrue();
        testContainsFalse();
        testToArray();
    }

    public static void testBooleanAdd() {
        DynamicArray dynamicArray = new DynamicArray();
        assertEquals("DynamicArrayTest.testBooleanAdd", true, dynamicArray.add(new Object()));
    }

    public static void testVoidAddBeggining() {
        DynamicArray actual = getDynamicArray();
        actual.add(0, 10);
        Object[] expected = {10, 0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("DynamicArrayTest.testVoidAddBeggining", expected, actual.toArray());
    }

    public static void testVoidAddMiddle() {
        DynamicArray actual = getDynamicArray();
        actual.add(5, 10);
        Object[] expected = {0, 1, 2, 3, 4, 10, null, 6, 7, 8, 9};
        assertEquals("DynamicArrayTest.testVoidAddMiddle", expected, actual.toArray());
    }

    public static void testVoidAddEnd() {
        DynamicArray actual = getDynamicArray();
        actual.add(10, 10);
        Object[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9, 10};
        assertEquals("DynamicArrayTest.testVoidAddEnd", expected, actual.toArray());
    }

    public static void testCheckIndex() {
        DynamicArray dynamicArray = getDynamicArray();
        try {
            dynamicArray.add(100, "added string");
        } catch (ArrayIndexOutOfBoundsException exception) {
            assertEquals("DynamicTestArray.testCheckIndex", WRONG_INDEX_MSG + "100", exception.getMessage());
        }
    }

    public static void testGet() {
        DynamicArray actual = getDynamicArray();
        assertEquals("DynamicTestArray.testGet", null, actual.get(5));
    }

    public static void testRemoveBeginning() {
        DynamicArray actual = getDynamicArray();
        actual.remove(0);
        Object[] expected = {1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("DynamicTestArray.testRemoveBeginning", expected, actual.toArray());
    }

    public static void testRemoveMiddle() {
        DynamicArray actual = getDynamicArray();
        actual.remove(5);
        Object[] expected = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        assertEquals("DynamicTestArray.testRemoveMiddle", expected, actual.toArray());
    }

    public static void testRemoveEnd() {
        DynamicArray actual = getDynamicArray();
        actual.remove(9);
        Object[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8};
        assertEquals("DynamicTestArray.testRemoveEnd", expected, actual.toArray());
    }

    public static void testSize() {
        DynamicArray actual = getDynamicArray();
        assertEquals("DynamicTestArray.testSize", 10, actual.size());
    }

    public static void testIndexOfExist() {
        DynamicArray actual = getDynamicArray();
        assertEquals("DynamicTestArray.testIndexOfExist", 5, actual.indexOf(null));
    }

    public static void testIndexOfDontExist() {
        DynamicArray actual = getDynamicArray();
        assertEquals("DynamicTestArray.testIndexOfDontExist", -1, actual.indexOf("abc"));
    }

    public static void testContainsTrue() {
        DynamicArray actual = getDynamicArray();
        assertEquals("DynamicTestArray.testContainsTrue", true, actual.contains(null));
    }

    public static void testContainsFalse() {
        DynamicArray actual = getDynamicArray();
        assertEquals("DynamicTestArray.testContainsFalse", false, actual.contains("abc"));
    }

    public static void testToArray() {
        Object[] actual = getDynamicArray().toArray();
        Object[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("DynamicTestArray.testToArray", expected, actual);
    }

    private static DynamicArray getDynamicArray() { // {0, 1, 2, 3, 4, null, 6, 7, 8, 9}
        DynamicArray result = new DynamicArray();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                result.add(null);
                continue;
            }
            result.add(i);
        }
        return result;
    }

}