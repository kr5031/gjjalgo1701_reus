package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.valueOf;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class CollectionTest {

    public static void main(String[] args) {
        testSize();
        testIsEmptyTrue();
        testIsEmptyFalse();
        testContainsTrue();
        testContainsFalse();
        testAddTrue();
        testRemoveIndex();
        testRemoveTrue();
        testRemoveFalse();
        testContainsAllTrue();
        testContainsAllFalse();
        testAddAllTrue();
        testAddAllFalse();
        testRemoveAllTrue();
        testRemoveAllFalse();
        testRetainAllTrue();
        testClear();
        testEqualsTrue();
        testEqualsFalse();
        testHashCodeTrue();
        testHashCodeFalse();
    }

    public static void testSize() {
        List<Integer> arrayList = getArrayList();
        assertEquals("CollectionTest.size", 10, arrayList.size());
    }

    public static void testIsEmptyTrue() {
        List<Integer> arrayList = new ArrayList<>();
        assertEquals("CollectionTest.testIsEmptyTrue", true, arrayList.isEmpty());
    }

    public static void testIsEmptyFalse() {
        List<Integer> arrayList = getArrayList();
        assertEquals("CollectionTest.testIsEmptyFalse", false, arrayList.isEmpty());
    }

    public static void testContainsTrue() {
        List<Integer> arrayList = getArrayList();
        assertEquals("CollectionTest.testContainsTrue", true, arrayList.contains(5));
    }

    public static void testContainsFalse() {
        List<Integer> arrayList = getArrayList();
        assertEquals("CollectionTest.testContainsFalse", false, arrayList.contains(777));
    }

    public static void testAddTrue() {
        List<Integer> arrayList = getArrayList();
        assertEquals("CollectionTest.testAddTrue", true, arrayList.add(777));
    }

    public static void testRemoveIndex() {
        List arrayList = getArrayList();
        assertEquals("CollectionTest.testRemoveIndex", 5, (int) arrayList.remove(5));
    }

    public static void testRemoveTrue() {
        List<Integer> arrayList = getArrayList();
        assertEquals("CollectionTest.testRemoveTrue", true, arrayList.remove(valueOf(5)));
    }

    public static void testRemoveFalse() {
        List<Integer> arrayList = getArrayList();
        assertEquals("CollectionTest.testRemoveFalse", false, arrayList.remove(new Object()));
    }

    public static void testContainsAllTrue() {
        List<Integer> arrayList = getArrayList();
        List<Integer> numbers = getNumbers();
        assertEquals("CollectionTest.testContainsAllTrue", true, arrayList.containsAll(numbers));
    }

    public static void testContainsAllFalse() {
        List<Integer> arrayList = getArrayList();
        List<Integer> numbers = getNumbersOutOfRange();
        assertEquals("CollectionTest.testContainsAllFalse", false, arrayList.containsAll(numbers));
    }

    public static void testAddAllTrue() {
        List<Integer> arrayList = getArrayList();
        List<Integer> numbers = getNumbers();
        assertEquals("CollectionTest.testAddAllTrue", true, arrayList.addAll(numbers));
    }

    public static void testAddAllFalse() {
        List<Integer> arrayList1 = getArrayList();
        List<Integer> arrayList2 = getArrayList();
        arrayList2.add(888);
        assertEquals("CollectionTest.testAddAllTrue", true, arrayList1.addAll(arrayList2));
    }

    public static void testRemoveAllTrue() {
        List<Integer> arrayList = getArrayList();
        List<Integer> numbers = getNumbersInRange();
        assertEquals("CollectionTest.testRemoveAllTrue", true, arrayList.removeAll(numbers));
    }

    public static void testRemoveAllFalse() {
        List<Integer> arrayList = getArrayList();
        List<Integer> numbers = getNumbersOutOfRange();
        assertEquals("CollectionTest.testRemoveAllFalse", false, arrayList.removeAll(numbers));
    }

    public static void testRetainAllTrue() {
        List<Integer> arraylist = getArrayList();
        List<Integer> numbers = getNumbersInRange();
        assertEquals("CollectionTest.testRetainAllTrue", true, arraylist.retainAll(numbers));
    }

    public static void testClear() {
        List<Integer> arrayList = getArrayList();
        arrayList.clear();
        assertEquals("CollectionTest.testClear", true, arrayList.isEmpty());
    }

    public static void testEqualsTrue() {
        List<Integer> arrayList1 = getArrayList();
        List<Integer> arrayList2 = getArrayList();
        assertEquals("CollectionTest.testEqualsTrue", true, arrayList1.equals(arrayList2));
    }

    public static void testEqualsFalse() {
        List<Integer> arrayList1 = getArrayList();
        List<Integer> arrayList2 = getNumbersOutOfRange();
        assertEquals("CollectionTest.testEqualsFalse", false, arrayList1.equals(arrayList2));
    }

    public static void testHashCodeTrue() {
        List<Integer> arrayList1 = getArrayList();
        List<Integer> arrayList2 = getArrayList();
        assertEquals("CollectionTest.testHashCodeTrue", true, arrayList1.hashCode() == arrayList2.hashCode());
    }

    public static void testHashCodeFalse() {
        List<Integer> arrayList1 = getArrayList();
        List<Integer> arrayList2 = getNumbersOutOfRange();
        assertEquals("CollectionTest.testHashCodeFalse", false, arrayList1.hashCode() == arrayList2.hashCode());
    }

    private static List<Integer> getArrayList() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

    private static List<Integer> getNumbers() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

    private static List<Integer> getNumbersOutOfRange() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 100; i < 110; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

    private static List<Integer> getNumbersInRange() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

}
