package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class QueueTest {

    public static void main(String[] args) {
        testAdd();
        testOffer();
        testRemoveReturn();
        testRemoveException();
        testPoll();
        testElement();
        testElementException();
        testPeek();
    }

    public static void testAdd() {
        Queue<Integer> queue = getArrayDeque();
        queue.add(777);
        List<Integer> actual = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            actual.add(queue.remove());
        }
        List<Integer> expected = getList();
        expected.add(777);
        assertEquals("QueueTest.testAdd", expected, actual);
    }

    public static void testOffer() {
        Queue<Integer> queue = getArrayDeque();
        assertEquals("QueueTest.testOffer", true, queue.offer(777));
    }

    public static void testRemoveReturn() {
        Queue<Integer> queue = getArrayDeque();
        assertEquals("QueueTest.testRemoveReturn", 0, (int) queue.remove());
    }

    public static void testRemoveException() {
        Queue<Integer> queue = new ArrayDeque<>();
        try {
            queue.remove();
        } catch (NoSuchElementException e) {
            System.out.println("QueueTest.testRemoveException passed");
            return;
        }
        System.out.println("QueueTest.testRemoveException failed, no exception had been thrown");
    }

    public static void testPoll() {
        Queue<Integer> queue = getArrayDeque();
        assertEquals("QueueTest.testPoll", 0, (int) queue.poll());
    }

    public static void testElement() {
        Queue<Integer> queue = getArrayDeque();
        assertEquals("QueueTest.testElement", 0, (int) queue.element());
    }

    public static void testElementException() {
        Queue<Integer> queue = new ArrayDeque<>();
        try {
            queue.element();
        } catch (NoSuchElementException e) {
            System.out.println("QueueTest.testElementException passed");
            return;
        }
        System.out.println("QueueTest.testElementException failed, no exception had been thrown");
    }

    public static void testPeek() {
        Queue<Integer> queue = getArrayDeque();
        assertEquals("QueueTest.testPeek", 0, (int) queue.peek());
    }

    private static Queue<Integer> getArrayDeque() {
        Queue<Integer> arrayDeque = new ArrayDeque<>();
        for (int i = 0; i < 10; i++) {
            arrayDeque.add(i);
        }
        return arrayDeque;
    }

    private static List<Integer> getList() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        return list;
    }
}