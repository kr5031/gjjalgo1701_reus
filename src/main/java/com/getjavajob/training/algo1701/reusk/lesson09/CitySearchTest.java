package com.getjavajob.training.algo1701.reusk.lesson09;

import java.util.SortedSet;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static com.getjavajob.training.algo1701.reusk.lesson09.CitySearch.findCities;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

import static java.lang.Character.MAX_VALUE;

public class CitySearchTest {
    public static void main(String[] args) {
        testFindCitiesAB();
        testFindCitiesAbLastCharIsMax();
        testFindCitiesMO();
        testFindCitiesNull();
    }

    public static void testFindCitiesAB() {
        SortedSet<String> expected = new TreeSet<>(asList("ab" + Character.MAX_VALUE + "cd", "Abakan"));
        assertEquals("CitySearchTest.testFindCitiesAB", expected, findCities("ab"));
    }

    public static void testFindCitiesAbLastCharIsMax() {
        SortedSet<String> expected = new TreeSet<>(asList("Uryupinsk" + MAX_VALUE));
        assertEquals("CitySearchTest.testFindCitiesAbLastCharIsMax", expected, findCities("Ur"));
    }

    public static void testFindCitiesMO() {
        SortedSet<String> expected = new TreeSet<>(asList("Moscow", "Mogilev", "monte-Karlo"));
        assertEquals("CitySearchTest.testFindCitiesMO", expected, findCities("mo"));
    }

    public static void testFindCitiesNull() {
        SortedSet<String> expected = new TreeSet<>();
        assertEquals("CitySearchTest.testFindCitiesNull", expected, findCities("zzz"));
    }

}