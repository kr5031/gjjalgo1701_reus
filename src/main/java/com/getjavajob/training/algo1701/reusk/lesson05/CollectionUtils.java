package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;

interface ActionClosure<T> {

    void action(T o);

}

interface Transformer<I, O> {

    O transform(I o);

}

interface Predicate<T> {

    boolean test(T o);

}

public class CollectionUtils {
    public static final String UNMODIFIABLE_MESSAGE = "The collection is unmodifiable!";

    public static <I> boolean filter(Collection<I> input, Predicate<I> predicate) {
        boolean result = false;
        Iterator<I> iterator = input.iterator();
        while (iterator.hasNext()) {
            if (predicate.test(iterator.next())) {
                continue;
            } else {
                iterator.remove();
                result = true;
            }
        }
        return result;
    }

    public static <I, O> Collection<O> transformToNewCollection(Collection<I> input, Transformer<I, O> transformer) {
        Collection<O> result = new ArrayList(input.size());
        Iterator<I> iterator = input.iterator();
        while (iterator.hasNext()) {
            result.add(transformer.transform(iterator.next()));
        }
        return result;
    }

    public static List transformCollection(List input, Transformer transformer) {
        ListIterator iterator = input.listIterator();
        while (iterator.hasNext()) {
            Object element = iterator.next();
            iterator.remove();
            iterator.add(transformer.transform(element));
        }
        return input;
    }

    public static <I> void forAllDo(Collection<I> input, ActionClosure closure) {
        for (I element : input) {
            closure.action(element);
        }
    }

    public static <I> Collection<I> unmodifiableCollection(Collection<I> input) {
        return new AbstractCollection<I>() {

            @Override
            public boolean add(I e) {
                throw new UnsupportedOperationException(UNMODIFIABLE_MESSAGE);
            }

            @Override
            public boolean remove(Object o) {
                throw new UnsupportedOperationException(UNMODIFIABLE_MESSAGE);
            }

            @Override
            public void forEach(Consumer action) {
                throw new UnsupportedOperationException(UNMODIFIABLE_MESSAGE);
            }

            @Override
            public Iterator<I> iterator() {
                return input.iterator();
            }

            @Override
            public int size() {
                return input.size();
            }

            @Override
            public boolean removeIf(java.util.function.Predicate filter) {
                throw new UnsupportedOperationException(UNMODIFIABLE_MESSAGE);
            }

            @Override
            public Spliterator spliterator() {
                return input.spliterator();
            }

            @Override
            public Stream stream() {
                return input.stream();
            }

            @Override
            public Stream parallelStream() {
                return input.parallelStream();
            }
        };
    }
}
