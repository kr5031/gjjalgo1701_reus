package com.getjavajob.training.algo1701.reusk.lesson10;

public class Sortings<T extends Comparable> {

    public T[] bubbleSort(T[] input) {
        int lastSwapIndex = input.length - 1;
        for (int i = 1; i < input.length; i++) {
            boolean isSorted = true;
            int currentSwap = -1;
            for (int j = 0; j < lastSwapIndex; j++) {
                if (input[j].compareTo(input[j + 1]) > 0) {
                    T tmp = input[j];
                    input[j] = input[j + 1];
                    input[j + 1] = tmp;
                    isSorted = false;
                    currentSwap = j;
                }
            }
            if (isSorted) {
                break;
            }
            lastSwapIndex = currentSwap;
        }
        return input;
    }

    public T[] insertSort(T[] input) {
        for (int i = 1; i < input.length; i++) {
            T x = input[i];
            int j = i - 1;
            while (j >= 0 && input[j].compareTo(x) > 0) {
                input[j + 1] = input[j];
                j--;
            }
            input[j + 1] = x;
        }
        return input;
    }

    public T[] quickSort(T[] input) {
        partitionForQuickSort(input, 0, input.length - 1);
        return input;
    }

    private void partitionForQuickSort(T[] input, int low, int high) {
        if (input == null || input.length == 0 || low >= high) {
            return;
        }
        int middle = low + (high - low) / 2;
        T pivot = input[middle];
        int i = low;
        int j = high;
        while (i <= j) {
            while (input[i].compareTo(pivot) < 0) {
                i++;
            }
            while (input[j].compareTo(pivot) > 0) {
                j--;
            }
            if (i <= j) {
                T tmp = input[i];
                input[i++] = input[j];
                input[j--] = tmp;
            }
        }
        if (low < j) {
            partitionForQuickSort(input, low, j);
        }
        if (high > i) {
            partitionForQuickSort(input, i, high);
        }
    }

    public T[] mergeSort(T[] input) {
        Object[] helper = new Object[input.length];
        mergeSort(input, helper, 0, input.length - 1);
        return input;
    }

    private void mergeSort(T[] input, Object[] helper, int left, int right) {
        if (left < right) {
            int middle = left + (right - left) / 2;
            mergeSort(input, helper, left, middle);
            mergeSort(input, helper, middle + 1, right);
            merge(input, helper, left, middle + 1, right);
        }
    }

    private void merge(T[] input, Object[] helper, int left, int right, int rightEnd) {
        int leftEnd = right - 1;
        int k = left;
        int num = rightEnd - left + 1;
        while (left <= leftEnd && right <= rightEnd) {
            if (input[left].compareTo(input[right]) <= 0) {
                helper[k++] = input[left++];
            } else {
                helper[k++] = input[right++];
            }
        }
        while (left <= leftEnd) {
            helper[k++] = input[left++];
        }
        while (right <= rightEnd) {
            helper[k++] = input[right++];
        }
        for (int i = 0; i < num; i++, rightEnd--) {
            input[rightEnd] = (T) helper[rightEnd];
        }
    }
}