package com.getjavajob.training.algo1701.reusk.lesson01;

import java.util.Scanner;

/**
 * swap 2 vars' values without intermediate var in 4 ways: 2 bitwise and 2 arithmetic solutions
 */
public class Task07 {

    public static String swapBitwiseXOR(int a, int b) {
        a ^= b;
        b ^= a;
        a ^= b;
        return "Swapped a = " + a + ", b = " + b;
    }

    public static String swapBitwiseOR(int a, int b) {
        a = ~a & b | ~b & a;
        b = ~a & b | ~b & a;
        a = ~a & b | ~b & a;
        return "Swapped a = " + a + ", b = " + b;
    }

    public static String swapArithmeticAdd(int a, int b) {
        a += b;
        b = a - b;
        a = a - b;
        return "Swapped a = " + a + ", b = " + b;
    }

    public static String swapArithmeticMultiply(int a, int b) {
        a *= b;
        b = a / b;
        a = a / b;
        return "Swapped a = " + a + ", b = " + b;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        int a = scanner.nextInt();
        System.out.print("Enter b: ");
        int b = scanner.nextInt();
        System.out.println(swapBitwiseXOR(a, b));
        System.out.println(swapBitwiseOR(a, b));
        System.out.println(swapArithmeticAdd(a, b));
        System.out.println(swapArithmeticMultiply(a, b));
    }

}