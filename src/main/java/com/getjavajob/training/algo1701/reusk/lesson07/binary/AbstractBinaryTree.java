package com.getjavajob.training.algo1701.reusk.lesson07.binary;

import com.getjavajob.training.algo1701.reusk.lesson07.AbstractTree;
import com.getjavajob.training.algo1701.reusk.lesson07.Node;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> parent = parent(n);
        if (left(parent) == n) {
            return right(parent);
        } else {
            return left(parent);
        }
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Collection<Node<E>> result = new ArrayList<>();
        Node<E> leftChild = left(n);
        if (leftChild != null) {
            result.add(leftChild);
        }
        Node<E> rightChild = right(n);
        if (rightChild != null) {
            result.add(rightChild);
        }
        return result;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        return children(n).size();
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        Deque<Node<E>> s = new ArrayDeque<>();
        Collection<Node<E>> result = new ArrayList<>();
        Node<E> currentNode = root();
        while (!s.isEmpty() || currentNode != null) {
            if (currentNode != null) {
                s.push(currentNode);
                currentNode = left(currentNode);
            } else {
                currentNode = s.pop();
                result.add(currentNode);
                currentNode = right(currentNode);
            }
        }
        return result;
    }

}
