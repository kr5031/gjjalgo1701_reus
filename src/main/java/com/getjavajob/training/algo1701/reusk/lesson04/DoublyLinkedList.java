package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.AbstractList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<V> extends AbstractList<V> implements List<V> {
    private static final String WRONG_INDEX = "Wrong index #";
    private static final String WRONG_ARGUMENT = "Wrong type of argument ";
    private int size;
    private Element<V> first;
    private Element<V> last;
    private int modVersion;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean add(V val) {
        final Element<V> l = last;
        final Element<V> toAdd = new Element<>(l, null, val);
        last = toAdd;
        if (l == null) {
            first = toAdd;
        } else {
            l.next = toAdd;
        }
        size++;
        modCount++;
        return true;
    }

    @Override
    public void add(int index, V val) {
        checkIndexAdd(index);
        Element<V> currentElementN = getElementN(index);
        if (index == 0) {
            Element<V> toInsert = new Element<>(null, currentElementN, val);
            currentElementN.prev = toInsert;
            first = toInsert;
            size++;
            modVersion++;
        } else if (index == size()) { //if we want to add to the end
            add(val);
        } else { //currentElementN evidently has previous and next Elements
            Element<V> toInsert = new Element<>(currentElementN.prev, currentElementN, val);
            currentElementN.prev.next = toInsert;
            currentElementN.prev = toInsert;
            size++;
            modVersion++;
        }
    }

    @Override
    public boolean remove(Object o) {
        try {
            Element<V> toRemove = findElementByValue(first, (V) o);
            return removeElement(toRemove);
        } catch (ClassCastException exception) {
            System.out.println(WRONG_ARGUMENT + getClassName(o));
            return false;
        }
    }

    @Override
    public V remove(int index) {
        checkIndexGet(index);
        Element<V> toRemove = getElementN(index);
        removeElement(toRemove);
        return toRemove.val;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator iterator() {
        return new ListIteratorImpl();
    }

    @Override
    public V[] toArray() {
        V[] result = (V[]) new Object[size()];
        int counter = 0;
        for (Element<V> element = first; element != null; element = element.next) {
            result[counter++] = element.val;
        }
        return result;
    }

    @Override
    public V get(int index) {
        checkIndexGet(index);
        return getElementN(index).val;
    }

    @Override
    public ListIterator<V> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<V> listIterator(int index) {
        return new ListIteratorImpl(index);
    }

    private boolean removeElement(Element<V> toRemove) {
        if (toRemove == null) {
            return false;
        }
        Element<V> beforeRemoved = toRemove.prev;
        Element<V> afterRemoved = toRemove.next;
        if (beforeRemoved != null && afterRemoved != null) {
            beforeRemoved.next = afterRemoved;
            afterRemoved.prev = beforeRemoved;
            toRemove = null;
            size--;
            modVersion++;
            return true;
        } else if (beforeRemoved == null && afterRemoved != null) {
            first = afterRemoved;
            first.prev = null;
            toRemove = null;
            size--;
            modVersion++;
            return true;
        } else if (beforeRemoved != null && afterRemoved == null) {
            last = beforeRemoved;
            last.next = null;
            toRemove = null;
            size--;
            modVersion++;
            return true;
        }
        return false;
    }

    private Element<V> findElementByValue(Element<V> current, V toFind) {
        if (toFind == null && current.val == null) {
            return current;
        } else if (current.val == null ^ toFind == null) {
            return findElementByValue(current.next, toFind);
        } else if (toFind.equals(current.val)) {
            return current;
        } else {
            return findElementByValue(current.next, toFind);
        }
    }

    private void checkIndexAdd(int index) {
        if (index < 0 || index > size()) {
            throw new IllegalArgumentException(WRONG_INDEX + index);
        }
    }

    private void checkIndexGet(int index) {
        if (index < 0 || index > size() - 1) {
            throw new IllegalArgumentException(WRONG_INDEX + index);
        }
    }

    private Element<V> getElementN(int n) {
        checkIndexGet(n);
        Element<V> element;
        if (n < size() / 2) {
            element = first;
            for (int i = 0; i < n; i++) {
                element = element.next;
            }
            return element;
        } else {
            element = last;
            for (int i = size() - 1; i > n; i--) {
                element = element.prev;
            }
            return element;
        }
    }

    private String getClassName(Object o) {
        return o.getClass().getName();
    }

    private static class Element<V> {
        private Element<V> prev;
        private Element<V> next;
        private V val;

        public Element(Element<V> prev, Element<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }
    }

    private class ListIteratorImpl implements ListIterator<V> {
        private static final String CONCURRENT_MODIFY = "External modification detected";
        private static final String NO_MORE_ELEMENTS = "No more elements";
        private Element<V> lastReturned;
        private Element<V> next;
        private int nextIndexToReturn;
        private int expectedModVersion = modVersion;

        public ListIteratorImpl(int cursor) {
            checkIndexAdd(cursor);
            next = cursor == size() ? null : getElementN(cursor);
            nextIndexToReturn = cursor;
        }

        public ListIteratorImpl() {
            this(0);
        }

        @Override
        public boolean hasNext() {
            return nextIndexToReturn < size();
        }

        @Override
        public V next() {
            checkModification();
            if (hasNext()) {
                lastReturned = next;
                next = next.next;
                nextIndexToReturn++;
                return lastReturned.val;
            } else {
                throw new NoSuchElementException(NO_MORE_ELEMENTS);
            }
        }

        @Override
        public boolean hasPrevious() {
            return nextIndexToReturn > 0;
        }

        @Override
        public V previous() {
            checkModification();
            if (hasPrevious()) {
                lastReturned = next = (next == null) ? last : next.prev;
                nextIndexToReturn--;
            } else {
                throw new NoSuchElementException(NO_MORE_ELEMENTS);
            }
            return lastReturned.val;
        }

        @Override
        public int nextIndex() {
            return nextIndexToReturn;
        }

        @Override
        public int previousIndex() {
            return nextIndexToReturn - 1;
        }

        @Override
        public void remove() {
            checkModification();
            if (lastReturned == null) {
                throw new IllegalArgumentException(WRONG_INDEX + lastReturned);
            }
            Element<V> afterRemoved = lastReturned.next;
            removeElement(lastReturned);
            next = afterRemoved;
            lastReturned = null;
            expectedModVersion++;
        }

        @Override
        public void set(V v) {
            checkModification();
            if (lastReturned == null) {
                throw new IllegalArgumentException(WRONG_INDEX);
            }
            lastReturned.val = v;
            modVersion++;
            expectedModVersion++;
        }

        @Override
        public void add(V v) {
            checkModification();
            DoublyLinkedList.this.add(nextIndex(), v);
            expectedModVersion++;
        }

        private void checkModification() {
            if (expectedModVersion != modVersion) {
                throw new ConcurrentModificationException(CONCURRENT_MODIFY);
            }
        }
    }

}