package com.getjavajob.training.algo1701.reusk.lesson06;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class MatrixClassTest {

    public static void main(String[] args) {
        testGet();
        testSet();
    }

    public static void testGet() {
        MatrixClass<Integer> matrix = new MatrixClass<>();
        matrix.set(1_000_000, 1_000_000, 777);
        assertEquals("MatrixClassTest.testGet", 777, (int) matrix.get(1_000_000, 1_000_000));
    }

    public static void testSet() {
        MatrixClass<Integer> matrix = new MatrixClass<>();
        matrix.set(1_000_000, 1_000_000, 777);
        assertEquals("MatrixClassTest.testSet", 777, (int) matrix.get(1_000_000, 1_000_000));
    }

}
