package com.getjavajob.training.algo1701.reusk.lesson06;

import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class SetTest {

    public static void main(String[] args) {
        testSize();
        testIsEmptyTrue();
        testIsEmptyFalse();
        testContainsTrue();
        testContainsFalse();
        testAddTrue();
        testRemoveTrue();
        testRemoveFalse();
        testContainsAllTrue();
        testContainsAllFalse();
        testAddAllFalse();
        testAddAllTrue();
        testRemoveAllTrue();
        testRemoveAllFalse();
        testRetainAllTrue();
        testClear();
        testEqualsTrue();
        testEqualsFalse();
        testHashCodeTrue();
        testHashCodeFalse();
    }

    public static void testSize() {
        Set<Integer> set = getSet();
        assertEquals("SetTest.testSize", 6, set.size());
    }

    public static void testIsEmptyTrue() {
        Set<Integer> set = new HashSet<>();
        assertEquals("SetTest.testIsEmptyTrue", true, set.isEmpty());
    }

    public static void testIsEmptyFalse() {
        Set<Integer> set = getSet();
        assertEquals("SetTest.testIsEmptyFalse", false, set.isEmpty());
    }

    public static void testContainsTrue() {
        Set<Integer> set = getSet();
        assertEquals("SetTest.testContainsTrue", true, set.contains(null));
    }

    public static void testContainsFalse() {
        Set<Integer> set = getSet();
        assertEquals("SetTest.testContainsFalse", false, set.contains(777));
    }

    public static void testAddTrue() {
        Set<Integer> set = getSet();
        assertEquals("SetTest.testAddTrue", true, set.add(777));
    }

    public static void testRemoveTrue() {
        Set<Integer> set = getSet();
        assertEquals("SetTest.testRemoveTrue", true, set.remove(5));
    }

    public static void testRemoveFalse() {
        Set<Integer> set = getSet();
        assertEquals("SetTest.testRemoveFalse", false, set.remove(777));
    }

    public static void testContainsAllTrue() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = getSet();
        assertEquals("SetTest.testContainsAllTrue", true, set1.containsAll(set2));
    }

    public static void testContainsAllFalse() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = getSet();
        set2.add(777);
        assertEquals("SetTest.testContainsAllFalse", false, set1.containsAll(set2));
    }

    public static void testAddAllFalse() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = getSet();
        assertEquals("SetTest.testAddAllFalse", false, set1.addAll(set2));
    }

    public static void testAddAllTrue() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = getSet();
        set2.add(777);
        assertEquals("SetTest.testAddAllTrue", true, set1.addAll(set2));
    }

    public static void testRemoveAllTrue() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = getSet();
        assertEquals("SetTest.testRemoveAllTrue", true, set1.removeAll(set2));
    }

    public static void testRemoveAllFalse() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = new HashSet<>();
        assertEquals("SetTest.testRemoveAllFalse", false, set1.removeAll(set2));
    }

    public static void testRetainAllTrue() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = new HashSet<>();
        set2.add(1);
        assertEquals("SetTest.testRetainAllTrue", true, set1.retainAll(set2));
    }

    public static void testClear() {
        Set<Integer> set = getSet();
        set.clear();
        assertEquals("SetTest.testClear", true, set.isEmpty());
    }

    public static void testEqualsTrue() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = getSet();
        assertEquals("SetTest.testEqualsTrue", true, set1.equals(set2));
    }

    public static void testEqualsFalse() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = new HashSet<>();
        assertEquals("SetTest.testEqualsTrue", false, set1.equals(set2));
    }

    public static void testHashCodeTrue() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = getSet();
        assertEquals("SetTest.testHashCodeTrue", true, set1.hashCode() == set2.hashCode());
    }

    public static void testHashCodeFalse() {
        Set<Integer> set1 = getSet();
        Set<Integer> set2 = new HashSet<>();
        assertEquals("SetTest.testHashCodeFalse", false, set1.hashCode() == set2.hashCode());
    }

    private static Set<Integer> getSet() {
        Set<Integer> set = new HashSet<>();
        set.add(null);
        for (int i = 1; i < 6; i++) {
            set.add(i);
        }
        return set;
    }

}
