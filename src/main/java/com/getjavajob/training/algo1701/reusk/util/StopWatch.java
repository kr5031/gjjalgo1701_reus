package com.getjavajob.training.algo1701.reusk.util;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.nanoTime;

public class StopWatch {

    private static long startTime;
    private static long endTime;

    public static void start() {
        startTime = currentTimeMillis();
    }

    public static void finish() {
        endTime = currentTimeMillis();
    }

    public static double getElapsedTimeMs() {
        return endTime - startTime;
    }

    public static void printElapsedTime(String testName) {
        System.out.printf(testName + ": %.0f ms\n", getElapsedTimeMs());
    }

}
