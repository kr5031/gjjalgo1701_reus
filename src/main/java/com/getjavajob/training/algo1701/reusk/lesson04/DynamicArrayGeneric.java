package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.AbstractList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ListIterator;

import static java.lang.System.arraycopy;
import static java.util.Arrays.copyOf;

public class DynamicArrayGeneric<T> extends AbstractList<T> implements List<T> {
    public static final String CONCURRENT_MODIFY_MSG = "External modification detected";
    public static final String WRONG_INDEX_MSG = "Wrong index #";
    public static final String WRONG_ARGUMENT = "Wrong type of argument ";
    private T[] elements;
    private int nextIndexToWrite;
    private int modVersion;

    public DynamicArrayGeneric(int size) {
        this.elements = (T[]) new Object[size];
    }

    public DynamicArrayGeneric() {
        this(10);
    }

    public boolean add(T e) {
        checkArraySize();
        modVersion++;
        elements[nextIndexToWrite++] = e;
        return true;
    }

    public void add(int i, T e) {
        checkIndex(i);
        checkArraySize();
        modVersion++;
        arraycopy(elements, i, elements, i + 1, nextIndexToWrite - i);
        elements[i] = e;
        nextIndexToWrite++;
    }

    public T set(int i, T e) {
        checkIndex(i);
        T toReturn = elements[i];
        modVersion++;
        elements[i] = e;
        return toReturn;
    }

    public T get(int i) {
        checkIndex(i);
        return elements[i];
    }

    public T remove(int i) {
        checkIndex(i);
        T toReturn = elements[i];
        int numToMove = size() - i - 1;
        if (numToMove > 0) {
            arraycopy(elements, i + 1, elements, i, nextIndexToWrite - i - 1);
        }
        modVersion++;
        elements[--nextIndexToWrite] = null;
        return toReturn;
    }

    public boolean remove(Object e) {
        try {
            int indexToRemove = findIndexOfObject((T) e);
            if (indexToRemove != -1) {
                modVersion++;
                remove(indexToRemove);
                return true;
            }
            return false;
        } catch (ClassCastException exception) {
            System.out.println(WRONG_ARGUMENT + e.getClass().getName());
            return false;
        }
    }

    public int size() {
        return nextIndexToWrite;
    }

    public int indexOf(Object e) {
        try {
            return findIndexOfObject((T) e);
        } catch (ClassCastException exception) {
            System.out.println(WRONG_ARGUMENT + getClassName(e));
            return -1;
        }
    }

    public boolean contains(Object e) {
        try {
            return findIndexOfObject((T) e) > -1;
        } catch (ClassCastException exception) {
            System.out.println(WRONG_ARGUMENT + getClassName(e));
            return false;
        }
    }

    public T[] toArray() {
        return copyOf(elements, size());
    }

    public ListIteratorImpl listIterator(int initialCursorPosition) {
        return new ListIteratorImpl(initialCursorPosition);
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    private int findIndexOfObject(T e) {
        for (int i = 0; i < elements.length; i++) {
            if (e == null) {
                if (get(i) == null) {
                    return i;
                } else {
                    continue;
                }
            }
            if (e.equals(get(i))) {
                return i;
            }
        }
        return -1;
    }

    private void checkIndex(int index) {
        if (index < 0 || index > nextIndexToWrite) {
            throw new ArrayIndexOutOfBoundsException(WRONG_INDEX_MSG + index);
        }
    }

    private void checkArraySize() {
        if (nextIndexToWrite == elements.length) {
            growArray();
        }
    }

    private void growArray() {
        int newSize = (int) (elements.length * 1.5);
        elements = copyOf(elements, newSize);
    }

    private String getClassName(Object object) {
        return object.getClass().getName();
    }

    private class ListIteratorImpl implements ListIterator {
        private int cursor;
        private int lastReturnedIndex = -1;
        private int expectedModVersion;

        public ListIteratorImpl(int cursor) {
            expectedModVersion = modVersion;
            this.cursor = cursor;
        }

        public ListIteratorImpl() {
            this(0);
        }

        private void checkModification() {
            if (expectedModVersion != modVersion) {
                throw new ConcurrentModificationException(CONCURRENT_MODIFY_MSG);
            }
        }

        public boolean hasNext() {
            return cursor < size();
        }

        public T next() {
            checkModification();
            if (hasNext()) {
                lastReturnedIndex = cursor;
                return get(cursor++);
            } else {
                throw new NoSuchElementException(WRONG_INDEX_MSG + cursor);
            }
        }

        public boolean hasPrevious() {
            return cursor > 0;
        }

        public T previous() {
            checkModification();
            if (hasPrevious()) {
                lastReturnedIndex = --cursor;
                return get(cursor);
            } else {
                throw new NoSuchElementException(WRONG_INDEX_MSG + cursor);
            }
        }

        //Returns the index of the element that would be returned by a subsequent call to next().
        // (Returns list size if the list iterator is at the end of the list.)
        public int nextIndex() {
            return cursor;
        }

        //Returns the index of the element that would be returned by a subsequent call to previous().
        // (Returns -1 if the list iterator is at the beginning of the list.)
        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            checkModification();
            DynamicArrayGeneric.this.remove(lastReturnedIndex);
            expectedModVersion++;
            cursor = lastReturnedIndex;
            lastReturnedIndex = -1;
        }

        public void set(Object e) {
            checkModification();
            try {
                DynamicArrayGeneric.this.set(lastReturnedIndex, (T) e);
                expectedModVersion++;
            } catch (ClassCastException exception) {
                System.out.println(WRONG_ARGUMENT + getClassName(e));
            }
        }

        public void add(Object e) {
            checkModification();
            try {
                DynamicArrayGeneric.this.add(cursor, (T) e);
                expectedModVersion++;
            } catch (ClassCastException exception) {
                System.out.println(WRONG_ARGUMENT + getClassName(e));
            }
        }
    }
}
