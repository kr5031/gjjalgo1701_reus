package com.getjavajob.training.algo1701.reusk.lesson06;

import java.util.HashMap;
import java.util.Map;

interface Matrix<V> {

    V get(int i, int j);

    void set(int i, int j, V val);

}

public class MatrixClass<V> implements Matrix<V> {
    private Map<PairingClass, V> elements = new HashMap<>();

    @Override
    public V get(int i, int j) {
        PairingClass coordinates = new PairingClass(i, j);
        return elements.get(coordinates);
    }

    @Override
    public void set(int i, int j, V val) {
        PairingClass coordinates = new PairingClass(i, j);
        elements.put(coordinates, val);
    }

    private static class PairingClass {
        private int i;
        private int j;

        public PairingClass(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PairingClass that = (PairingClass) o;

            if (i != that.i) return false;
            return j == that.j;
        }

        @Override
        public int hashCode() {
            int result = i;
            result = 31 * result + j;
            return result;
        }
    }

}