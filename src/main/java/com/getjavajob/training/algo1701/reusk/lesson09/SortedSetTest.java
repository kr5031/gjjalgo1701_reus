package com.getjavajob.training.algo1701.reusk.lesson09;

import java.util.NoSuchElementException;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class SortedSetTest {

    public static void main(String[] args) {
        testSubSet();
        testSubSetEmpty();
        testSubSetIllegalArgumentException();
        testHeadset();
        testHeadsetNullPointerException();
        testHeadsetIllegalArgumentException();
        testTailset();
        testTailsetNullPointerException();
        testTailsetIllegalArgumentException();
        testFirst();
        testFirstNoSuchElementException();
        testLast();
        testLastNoSuchElementException();
    }

    public static void testSubSet() {
        SortedSet<Integer> set = getSortedSet();
        SortedSet<Integer> expected = new TreeSet<>();
        expected.add(0);
        expected.add(1);
        expected.add(2);
        assertEquals("SortedSetTest.testSubSet", expected, set.subSet(0, 3));
    }

    public static void testSubSetEmpty() {
        SortedSet<Integer> set = getSortedSet();
        SortedSet<Integer> expected = new TreeSet<>();
        assertEquals("SortedSetTest.testSubSetEmpty", expected, set.subSet(0, 0));
    }

    public static void testSubSetIllegalArgumentException() {
        SortedSet<Integer> set = getSortedSet();
        SortedSet<Integer> subset = set.subSet(0, 5);
        try {
            subset.add(7);
        } catch (IllegalArgumentException e) {
            assertEquals("SortedSetTest.testSubSetIllegalArgumentException", "key out of range", e.getMessage());
            return;
        }
        fail("SortedSetTest.testSubSetIllegalArgumentException");
    }

    public static void testHeadset() {
        SortedSet<Integer> set = getSortedSet();
        SortedSet<Integer> expected = new TreeSet<>();
        expected.add(0);
        expected.add(1);
        expected.add(2);
        assertEquals("SortedSetTest.testHeadset", expected, set.headSet(3));
    }

    public static void testHeadsetNullPointerException() {
        SortedSet<Integer> set = getSortedSet();
        try {
            set.headSet(null);
        } catch (NullPointerException e) {
            assertEquals("SortedSetTest.testHeadsetNullPointerException", null, e.getMessage());
            return;
        }
        fail("SortedSetTest.testHeadsetNullPointerException");
    }

    public static void testHeadsetIllegalArgumentException() {
        SortedSet<Integer> set = getSortedSet();
        SortedSet<Integer> subset = set.subSet(0, 5);
        try {
            subset.headSet(9);
        } catch (IllegalArgumentException e) {
            assertEquals("SortedSetTest.testHeadsetIllegalArgumentException", "toKey out of range", e.getMessage());
            return;
        }
        fail("SortedSetTest.testHeadsetIllegalArgumentException");
    }

    public static void testTailset() {
        SortedSet<Integer> set = getSortedSet();
        SortedSet<Integer> expected = new TreeSet<>();
        expected.add(7);
        expected.add(8);
        expected.add(9);
        assertEquals("SortedSetTest.testTailset", expected, set.tailSet(7));
    }

    public static void testTailsetNullPointerException() {
        SortedSet<Integer> set = getSortedSet();
        try {
            set.tailSet(null);
        } catch (NullPointerException e) {
            assertEquals("SortedSetTest.testTailsetNullPointerException", null, e.getMessage());
            return;
        }
        fail("SortedSetTest.testTailsetNullPointerException");
    }

    public static void testTailsetIllegalArgumentException() {
        SortedSet<Integer> set = getSortedSet();
        SortedSet<Integer> subset = set.subSet(0, 5);
        try {
            subset.tailSet(6);
        } catch (IllegalArgumentException e) {
            assertEquals("SortedSetTest.testTailsetIllegalArgumentException", "fromKey out of range", e.getMessage());
            return;
        }
        fail("SortedSetTest.testTailsetIllegalArgumentException");
    }

    public static void testFirst() {
        SortedSet<Integer> set = getSortedSet();
        assertEquals("SortedSetTest.testFirst", 0, (int) set.first());
    }

    public static void testFirstNoSuchElementException() {
        SortedSet<Integer> set = new TreeSet<>();
        try {
            set.first();
        } catch (NoSuchElementException e) {
            assertEquals("SortedSetTest.testFirstNoSuchElementException", null, e.getMessage());
            return;
        }
        fail("SortedSetTest.testFirstNoSuchElementException");
    }

    public static void testLast() {
        SortedSet<Integer> set = getSortedSet();
        assertEquals("SortedSetTest.testLast", 9, (int) set.last());
    }

    public static void testLastNoSuchElementException() {
        SortedSet<Integer> set = new TreeSet<>();
        try {
            set.last();
        } catch (NoSuchElementException e) {
            assertEquals("SortedSetTest.testLastNoSuchElementException", null, e.getMessage());
            return;
        }
        fail("SortedSetTest.testLastNoSuchElementException");
    }

    private static SortedSet<Integer> getSortedSet() {
        SortedSet<Integer> set = new TreeSet<>();
        for (int i = 9; i >= 0; i--) {
            set.add(i);
        }
        return set;
    }

}