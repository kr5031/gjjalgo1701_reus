package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.getjavajob.training.algo1701.reusk.lesson05.CollectionUtils.filter;
import static com.getjavajob.training.algo1701.reusk.lesson05.CollectionUtils.forAllDo;
import static com.getjavajob.training.algo1701.reusk.lesson05.CollectionUtils.transformToNewCollection;
import static com.getjavajob.training.algo1701.reusk.lesson05.CollectionUtils.transformCollection;
import static com.getjavajob.training.algo1701.reusk.lesson05.CollectionUtils.unmodifiableCollection;
import static com.getjavajob.training.algo1701.reusk.lesson05.CollectionUtils.UNMODIFIABLE_MESSAGE;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;
import static java.lang.Integer.parseInt;

public class CollectionUtilsTest {

    public static void main(String[] args) {
        testFilterTrue();
        testFilterFalse();
        testTransformToNewCollection();
        testTransform();
        testForAllDo();
        testUnmodifiableCollectionSize();
        testUnmodifiableCollectionRemove();
        testUnmodifiableCollectionAdd();
    }

    public static void testFilterTrue() {
        assertEquals("CollectionUtilsTest.testFilterTrue", true, filter(getCollectionStrings(),
                new Predicate<String>() {
                    @Override
                    public boolean test(String o) {
                        return o.contains("Ivan");
                    }
                }));
    }

    public static void testFilterFalse() {
        assertEquals("CollectionUtilsTest.testFilterFalse", false, filter(getCollectionIvanov(),
                new Predicate<String>() {
                    @Override
                    public boolean test(String o) {
                        return o.contains("Ivan");
                    }
                }));
    }

    public static void testTransform() {
        List<String> actual = new ArrayList(getCollectionStringsNumber());
        actual = transformCollection(actual, new Transformer() {
            @Override
            public Object transform(Object o) {
                return Integer.valueOf(o.toString());
            }
        });
        Collection<Integer> expected = getCollectionIntegers();
        assertEquals("CollectionUtilsTest.testTransform", expected, actual);
    }

    public static void testTransformToNewCollection() {
        Collection<String> actual = getCollectionStringsNumber();
        Collection<Integer> expected = getCollectionIntegers();
        assertEquals("CollectionUtilsTest.testTransformToNewCollection", expected,
                transformToNewCollection(actual, new Transformer<String, Integer>() {
                    @Override
                    public Integer transform(String o) {
                        return parseInt(o.toString());
                    }
                }));
    }

    public static void testForAllDo() {
        Collection<String> actual = getCollectionStrings();
        forAllDo(actual, new ActionClosure() {
            @Override
            public void action(Object o) {
                String s = (String) o;
                s += "test";
            }
        });
        Collection<String> expected = getCollectionStrings();
        for (String s : expected) {
            s += "test";
        }
        assertEquals("CollectionUtilsTest.testForAllDo", expected, actual);
    }

    public static void testUnmodifiableCollectionSize() {
        Collection<Integer> collection = getCollectionIntegers();
        assertEquals("CollectionUtilsTest.testUnmodifiableCollectionSize", 10,
                unmodifiableCollection(collection).size());
    }

    public static void testUnmodifiableCollectionRemove() {
        Collection<Integer> collection = getCollectionIntegers();
        try {
            unmodifiableCollection(collection).remove(1);
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnmodifiableCollectionRemove", UNMODIFIABLE_MESSAGE, e.getMessage());
            return;
        }
        fail("CollectionUtilsTest.testUnmodifiableCollectionRemove");
    }

    public static void testUnmodifiableCollectionAdd() {
        Collection<Integer> collection = getCollectionIntegers();
        try {
            unmodifiableCollection(collection).add(1);
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnmodifiableCollectionAdd", UNMODIFIABLE_MESSAGE, e.getMessage());
            return;
        }
        fail("CollectionUtilsTest.testUnmodifiableCollectionAdd");
    }

    private static Collection<String> getCollectionStrings() {
        Collection<String> result = new ArrayList<>();
        result.add("Ivanov");
        result.add("Petrov");
        result.add("Smirnov");
        result.add("R2D2");
        return result;
    }

    private static Collection<String> getCollectionIvanov() {
        Collection<String> result = new ArrayList<>();
        result.add("Ivanov");
        result.add("Ivanov");
        result.add("Ivanyan");
        result.add("Ivanopulo");
        return result;
    }

    private static Collection<String> getCollectionStringsNumber() {
        Collection<String> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            result.add(String.valueOf(i));
        }
        return result;
    }

    private static Collection<Integer> getCollectionIntegers() {
        Collection<Integer> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            result.add(i);
        }
        return result;
    }

}
