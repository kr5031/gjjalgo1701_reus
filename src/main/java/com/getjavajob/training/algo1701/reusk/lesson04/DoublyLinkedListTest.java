package com.getjavajob.training.algo1701.reusk.lesson04;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class DoublyLinkedListTest {

    public static void main(String[] args) {
        testAdd();
        testAddToIndex();
        testRemoveByValue();
        testRemoveByIndex();
        testIsEmptyTrue();
        testIsEmptyFalse();
        testToArray();
        testGetNullValue();
        testGetValue();
    }

    public static void testAdd() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        doublyLinkedList.add(777);
        Object[] actual = doublyLinkedList.toArray();
        Object[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9, 777};
        assertEquals("DoublyLinkedListTest.testAdd", expected, actual);
    }

    public static void testAddToIndex() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        doublyLinkedList.add(5, 777);
        Object[] actual = doublyLinkedList.toArray();
        Object[] expected = {0, 1, 2, 3, 4, 777, null, 6, 7, 8, 9};
        assertEquals("DoublyLinkedListTest.testAddToIndex", expected, actual);
    }

    public static void testRemoveByValue() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        doublyLinkedList.remove(null);
        Object[] actual = doublyLinkedList.toArray();
        Object[] expected = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        assertEquals("DoublyLinkedListTest.testRemoveByValue", expected, actual);
    }

    public static void testRemoveByIndex() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        doublyLinkedList.remove(5);
        Object[] actual = doublyLinkedList.toArray();
        Object[] expected = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        assertEquals("DoublyLinkedListTest.testRemoveByIndex", expected, actual);
    }

    public static void testIsEmptyTrue() {
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList();
        assertEquals("DoublyLinkedListTest.testIsEmptyTrue", true, doublyLinkedList.isEmpty());
    }

    public static void testIsEmptyFalse() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        assertEquals("DoublyLinkedListTest.testIsEmptyFalse", false, doublyLinkedList.isEmpty());
    }

    public static void testToArray() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        Object[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("DoublyLinkedListTest.testToArray", expected, doublyLinkedList.toArray());
    }

    public static void testGetNullValue() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        assertEquals("DoublyLinkedListTest.testGetNullValue", null, doublyLinkedList.get(5));
    }

    public static void testGetValue() {
        DoublyLinkedList<Integer> doublyLinkedList = getDoublyLinkedList();
        assertEquals("DoublyLinkedListTest.testGetValue", 4, (int) doublyLinkedList.get(4));
    }

    private static DoublyLinkedList<Integer> getDoublyLinkedList() {  // 0, 1, 2, 3, 4, null, 6, 7, 8, 9
        DoublyLinkedList<Integer> doublyLinkedList = new DoublyLinkedList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                doublyLinkedList.add(null);
            } else {
                doublyLinkedList.add(i);
            }
        }
        return doublyLinkedList;
    }

}
