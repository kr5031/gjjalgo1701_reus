package com.getjavajob.training.algo1701.reusk.lesson10;

import java.util.Arrays;
import java.util.function.BinaryOperator;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;
import static java.util.Arrays.sort;
import static java.util.Arrays.parallelSort;
import static java.util.Arrays.parallelPrefix;
import static java.util.Arrays.binarySearch;
import static java.util.Arrays.fill;
import static java.util.Arrays.copyOf;
import static java.util.Arrays.copyOfRange;
import static java.util.Arrays.deepEquals;
import static java.util.Arrays.deepToString;

public class ArraysTest {

    public static void main(String[] args) {
        testSort();
        testParallelSort();
        testParallelPrefix();
        testParallelPrefixFromTo();
        testParallelPrefixNullPointerException();
        testBinarySearch();
        testBinarySearchFromTo();
        testBinarySearchFromToIllegalArgumentException();
        testBinarySearchFromToArrayIndexOutOfBoundsException();
        testBinarySearchInsertionPoint();
        testEqualsTrue();
        testEqualsFalse();
        testFill();
        testFillFromTo();
        testFillIllegalArgumentException();
        testFillArrayIndexOutOfBoundsException();
        testCopyOf();
        testCopyOfNegativeArraySizeException();
        testCopyOfNullPointerException();
        testCopyOfRange();
        testCopyOfRangeIllegalArgumentException();
        testCopyOfRangeNullPointerException();
        testDeepEqualsTrue();
        testDeepEqualsFalse();
        testDeepToString();
    }

    public static void testSort() {
        int[] actual = getUnsortedArray();
        sort(actual);
        assertEquals("ArraysTest.testSort", getSortedArray(), actual);
    }

    public static void testParallelSort() {
        int[] actual = getUnsortedArray();
        parallelSort(actual);
        assertEquals("ArraysTest.testParallelSort", getSortedArray(), actual);
    }

    public static void testParallelPrefix() {
        Integer[] actual = {0, 1, 2, 3};
        BinaryOperator<Integer> binaryOperator = (i1, i2) -> i1 + i2;
        parallelPrefix(actual, binaryOperator);
        Integer[] expected = {0, 1, 3, 6};
        assertEquals("ArraysTest.testParallelPrefix", expected, actual);
    }

    public static void testParallelPrefixFromTo() {
        Integer[] actual = {0, 1, 2, 3};
        BinaryOperator<Integer> binaryOperator = (i1, i2) -> i1 + i2;
        parallelPrefix(actual, 1, 3, binaryOperator);
        Integer[] expected = {0, 1, 3, 3};
        assertEquals("ArraysTest.testParallelPrefixFromTo", expected, actual);
    }

    public static void testParallelPrefixNullPointerException() {
        Integer[] actual = {0, 1, 2, 3};
        try {
            parallelPrefix(actual, null);
        } catch (NullPointerException e) {
            assertEquals("ArraysTest.testParallelPrefixNullPointerException", null, e.getMessage());
            return;
        }
        fail("ArraysTest.testParallelPrefixNullPointerException");
    }

    public static void testBinarySearch() {
        int[] actual = getSortedArray();
        assertEquals("ArraysTest.testBinarySearch", 7, binarySearch(actual, 7));
    }

    public static void testBinarySearchInsertionPoint() {
        double[] actual = getSortedArrayDouble();
        assertEquals("ArraysTest.testBinarySearchInsertionPoint", -8, binarySearch(actual, 6.5));
    }

    public static void testBinarySearchFromTo() {
        int[] actual = getSortedArray();
        assertEquals("ArraysTest.testBinarySearchFromTo", 5, binarySearch(actual, 1, 6, 5));
    }

    public static void testBinarySearchFromToIllegalArgumentException() {
        int[] actual = getSortedArray();
        try {
            binarySearch(actual, 6, 1, 5);
        } catch (IllegalArgumentException e) {
            assertEquals("ArraysTest.testBinarySearchFromToIllegalArgumentException", "fromIndex(6) > toIndex(1)",
                    e.getMessage());
            return;
        }
        fail("ArraysTest.testBinarySearchFromToIllegalArgumentException");
    }

    public static void testBinarySearchFromToArrayIndexOutOfBoundsException() {
        int[] actual = getSortedArray();
        try {
            binarySearch(actual, 0, 20, 5);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals("ArraysTest.testBinarySearchFromToArrayIndexOutOfBoundsException",
                    "Array index out of range: 20", e.getMessage());
            return;
        }
        fail("ArraysTest.testBinarySearchFromToArrayIndexOutOfBoundsException");
    }

    public static void testEqualsTrue() {
        int[] array1 = getSortedArray();
        int[] array2 = getSortedArray();
        assertEquals("ArraysTest.testEqualsTrue", true, Arrays.equals(array1, array2));
    }

    public static void testEqualsFalse() {
        int[] array1 = getSortedArray();
        int[] array2 = new int[1];
        assertEquals("ArraysTest.testEqualsFalse", false, Arrays.equals(array1, array2));
    }

    public static void testFill() {
        int[] actual = getSortedArray();
        fill(actual, 7);
        int[] expected = {7, 7, 7, 7, 7, 7, 7, 7, 7, 7};
        assertEquals("ArraysTest.testFill", expected, actual);
    }

    public static void testFillFromTo() {
        int[] actual = getSortedArray();
        fill(actual, 3, 6, 7);
        int[] expected = {0, 1, 2, 7, 7, 7, 6, 7, 8, 9};
        assertEquals("ArraysTest.testFillFromTo", expected, actual);
    }

    public static void testFillIllegalArgumentException() {
        int[] actual = getSortedArray();
        try {
            fill(actual, 5, 0, 777);
        } catch (IllegalArgumentException e) {
            assertEquals("ArraysTest.testFillIllegalArgumentException", "fromIndex(5) > toIndex(0)", e.getMessage());
            return;
        }
        fail("ArraysTest.testFillIllegalArgumentException");
    }

    public static void testFillArrayIndexOutOfBoundsException() {
        int[] actual = getSortedArray();
        try {
            fill(actual, 0, 100, 777);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertEquals("ArraysTest.testFillArrayIndexOutOfBoundsException", "Array index out of range: 100",
                    e.getMessage());
            return;
        }
        fail("ArraysTest.testFillArrayIndexOutOfBoundsException");
    }

    public static void testCopyOf() {
        int[] array1 = getSortedArray();
        int[] copy = copyOf(array1, array1.length);
        assertEquals("ArraysTest.testCopyOf", true, Arrays.equals(array1, copy));
    }

    public static void testCopyOfNegativeArraySizeException() {
        int[] array1 = getSortedArray();
        try {
            copyOf(array1, -1);
        } catch (NegativeArraySizeException e) {
            assertEquals("ArraysTest.testCopyOfNegativeArraySizeException", null, e.getMessage());
            return;
        }
        fail("ArraysTest.testCopyOfNegativeArraySizeException");
    }

    public static void testCopyOfNullPointerException() {
        int[] array1 = null;
        try {
            copyOf(array1, array1.length);
        } catch (NullPointerException e) {
            assertEquals("ArraysTest.testCopyOfNullPointerException", null, e.getMessage());
            return;
        }
        fail("ArraysTest.testCopyOfNullPointerException");
    }

    public static void testCopyOfRange() {
        int[] array1 = getSortedArray();
        int[] actual = copyOfRange(array1, 2, 5);
        int[] expected = {2, 3, 4};
        assertEquals("ArraysTest.testCopyOfRange", expected, actual);
    }

    public static void testCopyOfRangeIllegalArgumentException() {
        int[] array1 = getSortedArray();
        try {
            int[] array2 = copyOfRange(array1, 5, 2);
        } catch (IllegalArgumentException e) {
            assertEquals("ArraysTest.testCopyOfRangeIllegalArgumentException", "5 > 2", e.getMessage());
            return;
        }
        fail("ArraysTest.testCopyOfRangeIllegalArgumentException");
    }

    public static void testCopyOfRangeNullPointerException() {
        int[] array1 = null;
        try {
            int[] array2 = copyOfRange(array1, 2, 5);
        } catch (NullPointerException e) {
            assertEquals("ArraysTest.testCopyOfRangeNullPointerException", null, e.getMessage());
            return;
        }
        fail("ArraysTest.testCopyOfRangeNullPointerException");
    }

    public static void testDeepEqualsTrue() {
        int[][] array1 = getDoubleDimensionalArray();
        int[][] array2 = getDoubleDimensionalArray();
        assertEquals("ArraysTest.testDeepEqualsTrue", true, deepEquals(array1, array2));
    }

    public static void testDeepEqualsFalse() {
        int[][] array1 = getDoubleDimensionalArray();
        int[][] array2 = new int[1][2];
        assertEquals("ArraysTest.testDeepEqualsFalse", false, deepEquals(array1, array2));
    }

    public static void testDeepToString() {
        int[][] array = new int[][]{
                {1, 2},
                {3, 4}
        };
        assertEquals("ArraysTest.testDeepToString", "[[1, 2], [3, 4]]", deepToString(array));
    }

    public static int[] getUnsortedArray() {
        return new int[]{1, 7, 0, 6, 2, 4, 5, 3, 9, 8};
    }

    public static int[] getSortedArray() {
        return new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    }

    public static double[] getSortedArrayDouble() {
        return new double[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    }

    public static int[][] getDoubleDimensionalArray() {
        int[][] result = new int[8][10];
        int counter = 0;
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = counter++;
            }
        }
        return result;
    }

}