package com.getjavajob.training.algo1701.reusk.lesson08;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;
import com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
        this((o1, o2) -> ((Comparable) o1).compareTo(o2));
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        return comparator.compare(val1, val2);
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        if (n == null) {
            return null;
        }
        int compareResult = compare(n.getElement(), val);
        NodeImpl<E> node = validate(n);
        if (compareResult == 0) {
            return n;
        } else if (compareResult > 0) {
            return treeSearch(left(node), val);
        } else if (compareResult < 0) {
            return treeSearch(right(node), val);
        }
        return null;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (compare(n.getElement(), e) < 0) {
            return addRight(n, e);
        } else {
            return addLeft(n, e);
        }
    }

    protected void afterElementRemoved(Node<E> n) {
    }

    protected void afterElementAdded(Node<E> n) {
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        subString(sb, validate(root()), false);
        return sb.toString();
    }

    protected void subString(StringBuilder sb, NodeImpl<E> node, boolean isRight) {
        if (node == null) {
            return;
        }
        sb.append(node.getElement());
        if (isInternal(node)) {
            sb.append("(");
            subString(sb, validate(left(node)), false);
            sb.append(",");
            subString(sb, validate((right(node))), true);
            sb.append(")");
        }
    }

}
