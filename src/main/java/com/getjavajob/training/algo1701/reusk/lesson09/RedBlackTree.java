package com.getjavajob.training.algo1701.reusk.lesson09;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;
import com.getjavajob.training.algo1701.reusk.lesson08.BalanceableTree;

import static com.getjavajob.training.algo1701.reusk.lesson09.RedBlackTree.Color.BLACK;
import static com.getjavajob.training.algo1701.reusk.lesson09.RedBlackTree.Color.RED;

/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E extends Comparable> extends BalanceableTree<E> {

    public NodeImplRB<E> validate(Node<E> n) {
        if (n == null) {
            return null;
        } else if (n instanceof NodeImplRB) {
            return (NodeImplRB<E>) n;
        } else {
            throw new IllegalArgumentException(WRONG_NODE_TYPE_MSG);
        }
    }

    @Override
    protected Node<E> createNode(Node<E> parent, E element) {
        return new NodeImplRB<>(parent, element);
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        super.addRoot(e);
        makeBlack(root());
        return root;
    }

    @Override
    protected void subString(StringBuilder sb, NodeImpl<E> node, boolean isRight) {
        if (node == null) {
            return;
        }
        NodeImplRB<E> n = validate(node);
        sb.append(n.getElement());
        if (isBlack(n)) {
            sb.append("b");
        } else if (isRed(n)) {
            sb.append("r");
        }
        if (isInternal(node)) {
            sb.append("(");
            subString(sb, validate(left(node)), false);
            sb.append(",");
            subString(sb, validate((right(node))), true);
            sb.append(")");
        }
    }

    private boolean isBlack(Node<E> n) {
        return BLACK.equals(colorOf(n));
    }

    private boolean isRed(Node<E> n) {
        return RED.equals(colorOf(n));
    }

    private void makeBlack(Node<E> n) {
        if (n != null) {
            NodeImplRB<E> node = validate(n);
            node.setColor(BLACK);
        }
    }

    private void makeRed(Node<E> n) {
        if (n != null) {
            NodeImplRB<E> node = validate(n);
            node.setColor(RED);
        }
    }

    private Color colorOf(Node<E> node) {
        NodeImplRB<E> n = validate(node);
        return n == null ? BLACK : n.getColor();
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        NodeImplRB<E> child = validate(n);
        NodeImplRB<E> parent = validate(child.getParent());
        NodeImplRB<E> grandParent = validate(parent.getParent());
        NodeImplRB<E> uncle = validate(sibling(parent));
        while (isRed(parent)) {
            if (isRed(uncle)) {
                makeBlack(parent);
                makeBlack(uncle);
                makeRed(grandParent);
                child = grandParent;
                parent = validate(child.getParent());
                if (parent != null) {
                    grandParent = validate(parent.getParent());
                }
            } else {
                boolean isLine = isLeftChild(grandParent, parent) && isLeftChild(parent, child) ||
                        isRightChild(grandParent, parent) && isRightChild(parent, child);
                if (!isLine) {
                    rotate(child);
                    NodeImplRB<E> tmp = child;
                    child = parent;
                    parent = tmp;
                }
                makeBlack(parent);
                makeRed(grandParent);
                rotate(parent);
            }
        }
        makeBlack(root());
    }

    private Node<E> successor(Node<E> node) {
        NodeImplRB<E> n = validate(node);
        if (n == null) {
            return null;
        } else if (left(n) != null) {
            NodeImplRB<E> p = validate(left(n));
            while (right(p) != null) {
                p = validate(right(p));
            }
            return p;
        } else {
            NodeImplRB<E> p = validate(n.getParent());
            NodeImplRB<E> ch = n;
            while (p != null && isLeftChild(p, ch)) {
                ch = p;
                p = validate(p.getParent());
            }
            return p;
        }
    }

    @Override
    public E remove(Node<E> node) throws IllegalArgumentException {
        E toReturn = node.getElement();
        NodeImplRB<E> p = validate(node);
        if (left(p) != null && right(p) != null) {
            NodeImplRB<E> s = validate(successor(p));
            p.setElement(s.getElement());
            p = s;
        }
        NodeImplRB<E> replacement = left(p) != null ? validate(left(p)) : validate(right(p));
        NodeImplRB<E> parent = validate(p.getParent());
        if (replacement != null) {
            replacement.setParent(parent);
            if (parent == null) {
                root = replacement;
            } else if (isLeftChild(parent, p)) {
                parent.setLeftChild(replacement);
            } else {
                parent.setRightChild(replacement);
            }
            p.setParent(null);
            p.setLeftChild(null);
            p.setRightChild(null);
            if (isBlack(p)) {
                afterElementRemoved(replacement);
            }
        } else if (parent == null) {
            root = null;
        } else {
            if (isBlack(p)) {
                afterElementRemoved(p);
            }
            if (parent != null) {
                if (isLeftChild(parent, p)) {
                    parent.setLeftChild(null);
                } else if (isRightChild(parent, p)) {
                    parent.setRightChild(null);
                }
                p.setParent(null);
            }
        }
        treeSize--;
        return toReturn;
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        NodeImplRB<E> x = validate(n);
        NodeImplRB<E> parent = validate(x.getParent());
        while (x != root() && isBlack(x)) {
            if (isLeftChild(parent, x)) {
                NodeImplRB<E> sib = validate(right(parent));
                if (isRed(sib)) {
                    makeBlack(sib);
                    makeRed(parent);
                    rotate(sib);
                    sib = validate(right(parent));
                }
                if (isBlack(left(sib)) && isBlack(right(sib))) {
                    makeRed(sib);
                    x = parent;
                    parent = validate(x.getParent());
                } else {
                    if (isBlack(right(sib))) {
                        makeBlack(left(sib));
                        makeRed(sib);
                        rotate(left(sib));
                        sib = validate(right(parent));
                    }
                    sib.setColor(colorOf(parent));
                    makeBlack(parent);
                    makeBlack(right(sib));
                    rotate(sib);
                    x = validate(root());
                }
            } else {
                NodeImplRB<E> sib = validate(left(parent));
                if (isRed(sib)) {
                    makeBlack(sib);
                    makeRed(parent);
                    rotate(sib);
                    sib = validate(left(parent));
                }
                if (isBlack(right(sib)) && isBlack(left(sib))) {
                    makeRed(sib);
                    x = parent;
                    parent = validate(x.getParent());
                } else {
                    if (isBlack(left(sib))) {
                        makeBlack(right(sib));
                        makeRed(sib);
                        rotate(right(sib));
                        sib = validate(left(parent));
                    }
                    sib.setColor(colorOf(parent));
                    makeBlack(parent);
                    makeBlack(left(sib));
                    rotate(sib);
                    x = validate(root());
                }
            }
        }
        makeBlack(x);
    }

    public enum Color {
        BLACK, RED
    }

    protected static class NodeImplRB<E> extends NodeImpl<E> {
        private Color color;

        public NodeImplRB(Node<E> parent, E element) {
            super(parent, element);
            setColor(RED);
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }
    }
}
