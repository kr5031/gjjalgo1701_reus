package com.getjavajob.training.algo1701.reusk.util;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.util.Arrays.deepEquals;
import static java.util.Arrays.deepToString;

public class Assert {
    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected == null && actual == null || expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + Arrays.toString(expected) + ", Actual: "
                    + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + deepToString(expected) + ", Actual: "
                    + deepToString(actual));
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(String testName, Object expected, Object actual) {
        if (expected == null && actual == null) {
            System.out.println(testName + " passed");
            return;
        }
        if (expected == null && actual != null || actual == null && expected != null) {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
            return;
        }
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static <E> void assertEquals(String testName, Node<E> expected, Node<E> actual) {
        if (expected == null && actual == null || expected.getElement().equals(actual.getElement())) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected.getElement() + ", Actual: " +
                    actual.getElement());
        }
    }

    public static <E> void assertEquals(String testName, Collection<E> expected, Collection<E> actual) {
        boolean equals = true;
        if (expected.size() != actual.size()) {
            equals = false;
        } else {
            for (E element : expected) {
                if (!actual.contains(element)) {
                    equals = false;
                    break;
                }
            }
        }
        if (equals) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void assertEquals(String testName, Object[] expected, Object[] actual) {
        if (deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed. Expected: " + expected + ", Actual: " + actual);
        }
    }

    public static void fail(String testName) {
        System.out.println(testName + " failed. No exception had been thrown");
    }

}