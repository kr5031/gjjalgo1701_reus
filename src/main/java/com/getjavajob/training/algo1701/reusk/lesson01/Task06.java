package com.getjavajob.training.algo1701.reusk.lesson01;

import java.util.Scanner;

/**
 * don't use arithmetic + * - / % operators in tasks. use only &, |, ~, ^, <<, >>,>>>.
 * a) given n<31, calc 2^n
 * b) given n,m<31, calc 2^n+2^m
 * <p>
 * given int a, n>0:
 * c) reset n lower bits (create mask with n lower bits reset)
 * d) set a's n-th bit with 1
 * e) invert n-th bit (use 2 bit ops)
 * f) set a's n-th bit with 0
 * g) return n lower bits
 * h) return n-th bit
 * <p>
 * i) given byte a. output bin representation using bit ops (don't use jdk api).
 */
public class Task06 {

    public static int calculateA(int n) {
        if (n >= 31) {
            return -1;
        }
        return 2 << n - 1;
    }

    public static int calculateB(int n, int m) {
        if (n >= 31 || m >= 31) {
            return -1;
        }
        return 2 << n - 1 | 2 << m - 1;
    }

    public static int calculateC(int a, int n) {
        if (a <= 0 || n <= 0) {
            return -1;
        }
        return a & (~1 << n - 1);
    }

    public static int calculateD(int a, int n) {
        if (a <= 0 || n <= 0) {
            return -1;
        }
        return a | 1 << n - 1;
    }

    public static int calculateE(short a, short n) {
        if (a <= 0 || n <= 0) {
            return -1;
        }
        return a ^ 1 << n - 1;
    }

    public static int calculateF(int a, int n) {
        if (a <= 0 || n <= 0) {
            return -1;
        }
        return a & ~(1 << n - 1);
    }

    public static int calculateG(int a, int n) {
        if (a <= 0 || n <= 0) {
            return -1;
        }
        return a & ((1 << n) - 1);
    }

    public static int calculateH(int a, int n) {
        if (a <= 0 || n <= 0) {
            return -1;
        }
        return (a >> n - 1) & 1;
    }

    public static String calculateI(int a) {
        if (a <= 0) {
            return "Wrong input";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 32; i++) { //32 for number of bits in int
            sb.append(a & 1);
            a >>>= 1;
        }
        return sb.reverse().toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter n for A: ");
        int n = scanner.nextInt();
        System.out.println("2^" + n + " = " + calculateA(n));

        System.out.println("Enter n for B: ");
        n = scanner.nextInt();
        System.out.println("Enter m for B: ");
        int m = scanner.nextInt();
        System.out.println("2^" + n + " + 2^" + m + " = " + calculateB(n, m));

        System.out.println("Enter a for C: ");
        int a = scanner.nextInt();
        System.out.println("Enter n for C: ");
        n = scanner.nextInt();
        System.out.println(Integer.toBinaryString(a) + " with resetted " + n + " lower bits = " +
                Integer.toBinaryString(calculateC(a, n)));

        System.out.println("Enter a for D: ");
        a = scanner.nextInt();
        System.out.println("Enter n for D: ");
        n = scanner.nextInt();
        System.out.println(Integer.toBinaryString(a) + " with set " + n + " bit = " +
                Integer.toBinaryString(calculateD(a, n)));

        System.out.println("Enter a for E: ");
        a = scanner.nextInt();
        System.out.println("Enter n for E: ");
        n = scanner.nextInt();
        System.out.println(Integer.toBinaryString(a) + " with inverted " + n + " bit = " +
                Integer.toBinaryString(calculateE((short) a, (short) n)));

        System.out.println("Enter a for F: ");
        a = scanner.nextInt();
        System.out.println("Enter n for F: ");
        n = scanner.nextInt();
        System.out.println(Integer.toBinaryString(a) + " with resetted " + n + " bit = " +
                Integer.toBinaryString(calculateF(a, n)));

        System.out.println("Enter a for G: ");
        a = scanner.nextInt();
        System.out.println("Enter n for G: ");
        n = scanner.nextInt();
        System.out.println(Integer.toBinaryString(a) + " lower " + n + " bits = " +
                Integer.toBinaryString(calculateG(a, n)));

        System.out.println("Enter a for H: ");
        a = scanner.nextInt();
        System.out.println("Enter n for H: ");
        n = scanner.nextInt();
        System.out.println(Integer.toBinaryString(a) + n + "th bit = " +
                Integer.toBinaryString(calculateH(a, n)));

        System.out.println("Enter a for I: ");
        a = scanner.nextInt();
        System.out.println(a + " in binary (4 byte) = " + calculateI(a));
    }

}