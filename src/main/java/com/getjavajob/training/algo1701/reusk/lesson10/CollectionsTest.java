package com.getjavajob.training.algo1701.reusk.lesson10;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import static java.util.Collections.sort;
import static java.util.Arrays.asList;
import static java.util.Collections.binarySearch;
import static java.util.Collections.reverse;
import static java.util.Collections.shuffle;
import static java.util.Collections.swap;
import static java.util.Collections.fill;
import static java.util.Collections.copy;
import static java.util.Collections.min;
import static java.util.Collections.max;
import static java.util.Collections.rotate;
import static java.util.Collections.replaceAll;
import static java.util.Collections.indexOfSubList;
import static java.util.Collections.lastIndexOfSubList;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableSet;
import static java.util.Collections.unmodifiableSortedSet;
import static java.util.Collections.unmodifiableNavigableSet;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSortedMap;
import static java.util.Collections.synchronizedCollection;
import static java.util.Collections.synchronizedSet;
import static java.util.Collections.synchronizedSortedSet;
import static java.util.Collections.synchronizedNavigableSet;
import static java.util.Collections.synchronizedList;
import static java.util.Collections.synchronizedMap;
import static java.util.Collections.synchronizedSortedMap;
import static java.util.Collections.synchronizedNavigableMap;
import static java.util.Collections.checkedCollection;
import static java.util.Collections.checkedSet;
import static java.util.Collections.checkedSortedSet;
import static java.util.Collections.checkedList;
import static java.util.Collections.checkedMap;
import static java.util.Collections.checkedSortedMap;
import static java.util.Collections.emptyIterator;
import static java.util.Collections.emptyListIterator;
import static java.util.Collections.emptyEnumeration;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static java.util.Collections.nCopies;
import static java.util.Collections.frequency;
import static java.util.Collections.disjoint;
import static java.util.Collections.addAll;
import static java.util.Collections.newSetFromMap;
import static java.util.Collections.asLifoQueue;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class CollectionsTest {

    public static void main(String[] args) {
        testSort();
        testBinarySearch();
        testBinarySearchInsertionPoint();
        testReverse();
        testShuffle();
        testSwap();
        testSwapIndexOutOfBoundsException();
        testFill();
        testCopy();
        testCopyIndexOutOfBoundsException();
        testMin();
        testMax();
        testRotate();
        testReplaceAll();
        testIndexOfSubList();
        testIndexOfSubListAbsence();
        testLastIndexOfSubList();
        testLastIndexOfSubListAbsence();
        testUnmodifiableCollection();
        testUnmodifiableSet();
        testUnmodifiableSortedSet();
        testUnmodifiableNavigableSet();
        testUnmodifiableList();
        testUnmodifiableMap();
        testUnmodifiableSortedMap();
        testSynchronizedCollection();
        testSynchronizedSet();
        testSynchronizedSortedSet();
        testSynchronizedNavigableSet();
        testSynchronizedList();
        testSynchronizedMap();
        testSynchronizedSortedMap();
        testSynchronizedNavigableMap();
        testCheckedCollection();
        testCheckedSet();
        testCheckedSortedSet();
        testCheckedList();
        testCheckedMap();
        testCheckedSortedMap();
        testEmptyIterator();
        testEmptyIteratorNoSuchElementException();
        testEmptyIteratorIllegalStateException();
        testEmptyListIterator();
        testEmptyListIteratorUnsupportedOperationException();
        testEmptyListIteratorNextIndex();
        testEmptyListIteratorPreviousIndex();
        testEmptyEnumeration();
        testEmptyEnumerationNoSuchElementException();
        testEmptyList();
        testEmptyMap();
        testSingleton();
        testSingletonList();
        testSingletonMap();
        testNCopies();
        testNCopiesIllegalArgumentException();
        testFrequency();
        testDisjointTrue();
        testDisjointFalse();
        testAddAll();
        testAddAllUnsupportedOperationException();
        testNewSetFromMap();
        testNewSetFromMapIllegalArgumentException();
        testAsLifoQueue();
    }

    public static void testSort() {
        List<String> source = getUnsortedCollection();
        List<String> expected = getSortedCollection();
        sort(source);
        assertEquals("CollectionsTest.testSort", expected, source);
    }

    public static void testBinarySearch() {
        List<String> source = getSortedCollection();
        assertEquals("CollectionsTest.testBinarySearch", 1, binarySearch(source, "Bravo"));
    }

    public static void testBinarySearchInsertionPoint() {
        List<String> source = getSortedCollection();
        assertEquals("CollectionsTest.testBinarySearchInsertionPoint", -2, binarySearch(source, "Baravo"));
    }

    public static void testReverse() {
        List<String> source = getSortedCollection();
        reverse(source);
        List<String> expected = asList("Charlie", "Bravo", "Alpha");
        assertEquals("CollectionsTest.testReverse", expected, source);
    }

    public static void testShuffle() {
        List<String> list1 = getSortedCollection();
        List<String> list2 = getSortedCollection();
        shuffle(list1);
        boolean actual = list1.equals(list2);
        assertEquals("CollectionsTest.testShuffle", false, actual);
    }

    public static void testSwap() {
        List<String> actual = getSortedCollection();
        swap(actual, 0, 1);
        List<String> expected = asList("Bravo", "Alpha", "Charlie");
        assertEquals("CollectionsTest.testSwap", expected, actual);
    }

    public static void testSwapIndexOutOfBoundsException() {
        List<String> actual = getSortedCollection();
        try {
            swap(actual, 0, 100);
        } catch (IndexOutOfBoundsException e) {
            assertEquals("CollectionsTest.testSwapIndexOutOfBoundsException", "Index: 100, Size: 3", e.getMessage());
            return;
        }
        fail("CollectionsTest.testSwapIndexOutOfBoundsException");
    }

    public static void testFill() {
        List<String> actual = getSortedCollection();
        fill(actual, "test");
        List<String> expected = asList("test", "test", "test");
        assertEquals("CollectionsTest.testFill", expected, actual);
    }

    public static void testCopy() {
        List<String> source = getSortedCollection();
        List<String> actual = getUnsortedCollection();
        copy(actual, source);
        assertEquals("CollectionsTest.testCopy", true, actual.equals(source));
    }

    public static void testCopyIndexOutOfBoundsException() {
        List<String> source = getSortedCollection();
        List<String> actual = new ArrayList<>();
        try {
            copy(actual, source);
        } catch (IndexOutOfBoundsException e) {
            assertEquals("CollectionsTest.testCopyIndexOutOfBoundsException", "Source does not fit in dest",
                    e.getMessage());
            return;
        }
        fail("CollectionsTest.testCopyIndexOutOfBoundsException");
    }

    public static void testMin() {
        List<String> list = getSortedCollection();
        String expected = "Alpha";
        assertEquals("CollectionsTest.testMin", expected, min(list));
    }

    public static void testMax() {
        List<String> list = getSortedCollection();
        String expected = "Charlie";
        assertEquals("CollectionsTest.testMin", expected, max(list));
    }

    public static void testRotate() {
        List<String> list = getSortedCollection();
        rotate(list, 1);
        List<String> expected = asList("Charlie", "Alpha", "Bravo");
        assertEquals("CollectionsTest.testRotate", expected, list);
    }

    public static void testReplaceAll() {
        List<String> actual = asList("a", "a", "a", "b", "b", "b");
        replaceAll(actual, "a", "c");
        List<String> expected = asList("c", "c", "c", "b", "b", "b");
        assertEquals("CollectionsTest.testReplaceAll", expected, actual);
    }

    public static void testIndexOfSubList() {
        List<String> list = getSortedCollection();
        List<String> toSearch = asList("Bravo");
        assertEquals("CollectionsTest.testIndexOfSubList", 1, indexOfSubList(list, toSearch));
    }

    public static void testIndexOfSubListAbsence() {
        List<String> list = getSortedCollection();
        List<String> toSearch = asList("Whiskey");
        assertEquals("CollectionsTest.testIndexOfSubListAbsence", -1, indexOfSubList(list, toSearch));
    }

    public static void testLastIndexOfSubList() {
        List<String> list = asList("a", "a", "a", "b", "b");
        List<String> toSearch = asList("a");
        assertEquals("CollectionsTest.testLastIndexOfSubList", 2, lastIndexOfSubList(list, toSearch));
    }

    public static void testLastIndexOfSubListAbsence() {
        List<String> list = asList("a", "a", "a", "b", "b");
        List<String> toSearch = asList("z");
        assertEquals("CollectionsTest.testLastIndexOfSubListAbsence", -1, lastIndexOfSubList(list, toSearch));
    }

    public static void testUnmodifiableCollection() {
        Collection<String> list = getSortedCollection();
        Collection<String> expected = getSortedCollection();
        assertEquals("CollectionsTest.testUnmodifiableCollection", expected, unmodifiableCollection(list));
    }

    public static void testUnmodifiableSet() {
        Set<String> actual = new HashSet<>();
        actual.add("Alpha");
        actual.add("Bravo");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testUnmodifiableSet", expected, unmodifiableSet(actual));
    }

    public static void testUnmodifiableSortedSet() {
        SortedSet<String> actual = new TreeSet<>();
        actual.add("Bravo");
        actual.add("Alpha");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testUnmodifiableSortedSet", expected, unmodifiableSortedSet(actual));
    }

    public static void testUnmodifiableNavigableSet() {
        NavigableSet<String> actual = new TreeSet<>();
        actual.add("Bravo");
        actual.add("Alpha");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testUnmodifiableNavigableSet", expected, unmodifiableNavigableSet(actual));
    }

    public static void testUnmodifiableList() {
        List<String> actual = getSortedCollection();
        List<String> expected = getSortedCollection();
        assertEquals("CollectionsTest.testUnmodifiableList", expected, unmodifiableList(actual));
    }

    public static void testUnmodifiableMap() {
        Map<Integer, String> actual = new HashMap<>();
        actual.put(1, "one");
        actual.put(2, "two");
        Map<Integer, String> expected = new HashMap<>();
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("CollectionsTest.testUnmodifiableMap", expected, unmodifiableMap(actual));
    }

    public static void testUnmodifiableSortedMap() {
        SortedMap<Integer, String> actual = new TreeMap<>();
        actual.put(1, "one");
        actual.put(2, "two");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("CollectionsTest.testUnmodifiableSortedMap", expected, unmodifiableSortedMap(actual));
    }

    public static void testSynchronizedCollection() {
        Collection<String> actual = getSortedCollection();
        Collection<String> expected = getSortedCollection();
        assertEquals("CollectionsTest.testSynchronizedCollection", expected, synchronizedCollection(actual));
    }

    public static void testSynchronizedSet() {
        Set<String> actual = new HashSet<>();
        actual.add("Alpha");
        actual.add("Bravo");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testSynchronizedSet", expected, synchronizedSet(actual));
    }

    public static void testSynchronizedSortedSet() {
        SortedSet<String> actual = new TreeSet<>();
        actual.add("Bravo");
        actual.add("Alpha");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testSynchronizedSortedSet", expected, synchronizedSortedSet(actual));
    }

    public static void testSynchronizedNavigableSet() {
        NavigableSet<String> actual = new TreeSet<>();
        actual.add("Bravo");
        actual.add("Alpha");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testSynchronizedNavigableSet", expected, synchronizedNavigableSet(actual));
    }

    public static void testSynchronizedList() {
        List<String> actual = getSortedCollection();
        List<String> expected = getSortedCollection();
        assertEquals("CollectionsTest.testSynchronizedList", expected, synchronizedList(actual));
    }

    public static void testSynchronizedMap() {
        Map<Integer, String> actual = new HashMap<>();
        actual.put(1, "one");
        actual.put(2, "two");
        Map<Integer, String> expected = new HashMap<>();
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("CollectionsTest.testSynchronizedMap", expected, synchronizedMap(actual));
    }

    public static void testSynchronizedSortedMap() {
        SortedMap<Integer, String> actual = new TreeMap<>();
        actual.put(1, "one");
        actual.put(2, "two");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("CollectionsTest.testSynchronizedSortedMap", expected, synchronizedSortedMap(actual));
    }

    public static void testSynchronizedNavigableMap() {
        NavigableMap<Integer, String> actual = new TreeMap<>();
        actual.put(1, "one");
        actual.put(2, "two");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("CollectionsTest.testSynchronizedNavigableMap", expected, synchronizedNavigableMap(actual));
    }

    public static void testCheckedCollection() {
        Collection<String> actual = checkedCollection(getSortedCollection(), String.class);
        Collection<String> expected = getSortedCollection();
        assertEquals("CollectionsTest.testCheckedCollection", expected, actual);
    }

    public static void testCheckedSet() {
        Set<String> actual = new HashSet<>();
        actual.add("Alpha");
        actual.add("Bravo");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testCheckedSet", expected, checkedSet(actual, String.class));
    }

    public static void testCheckedSortedSet() {
        SortedSet<String> actual = new TreeSet<>();
        actual.add("Bravo");
        actual.add("Alpha");
        actual.add("Charlie");
        Set<String> expected = new HashSet<>();
        expected.add("Alpha");
        expected.add("Bravo");
        expected.add("Charlie");
        assertEquals("CollectionsTest.testCheckedSortedSet", expected, checkedSortedSet(actual, String.class));
    }

    public static void testCheckedList() {
        List<String> actual = getSortedCollection();
        List<String> expected = getSortedCollection();
        assertEquals("CollectionsTest.testCheckedList", expected, checkedList(actual, String.class));
    }

    public static void testCheckedMap() {
        Map<Integer, String> actual = new HashMap<>();
        actual.put(1, "one");
        actual.put(2, "two");
        Map<Integer, String> expected = new HashMap<>();
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("CollectionsTest.testCheckedMap", expected, checkedMap(actual, Integer.class, String.class));
    }

    public static void testCheckedSortedMap() {
        SortedMap<Integer, String> actual = new TreeMap<>();
        actual.put(1, "one");
        actual.put(2, "two");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("CollectionsTest.testCheckedSortedMap", expected, checkedSortedMap(actual, Integer.class,
                String.class));
    }

    public static void testEmptyIterator() {
        Iterator<String> iterator = emptyIterator();
        assertEquals("CollectionsTest.testEmptyIterator", false, iterator.hasNext());
    }

    public static void testEmptyIteratorNoSuchElementException() {
        Iterator<String> iterator = emptyIterator();
        try {
            iterator.next();
        } catch (NoSuchElementException e) {
            assertEquals("CollectionsTest.testEmptyIteratorNoSuchElementException", null, e.getMessage());
            return;
        }
        fail("CollectionsTest.testEmptyIteratorNoSuchElementException");
    }

    public static void testEmptyIteratorIllegalStateException() {
        Iterator<String> iterator = emptyIterator();
        try {
            iterator.remove();
        } catch (IllegalStateException e) {
            assertEquals("CollectionsTest.testEmptyIteratorIllegalStateException", null, e.getMessage());
            return;
        }
        fail("CollectionsTest.testEmptyIteratorIllegalStateException");
    }

    public static void testEmptyListIterator() {
        ListIterator<String> iterator = emptyListIterator();
        assertEquals("CollectionsTest.testEmptyListIterator", false, iterator.hasNext());
    }

    public static void testEmptyListIteratorUnsupportedOperationException() {
        ListIterator<String> iterator = emptyListIterator();
        try {
            iterator.add("1");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionsTest.testEmptyListIteratorUnsupportedOperationException", null, e.getMessage());
            return;
        }
        fail("CollectionsTest.testEmptyListIteratorUnsupportedOperationException");
    }

    public static void testEmptyListIteratorNextIndex() {
        ListIterator<String> iterator = emptyListIterator();
        assertEquals("CollectionsTest.testEmptyListIteratorNextIndex", 0, iterator.nextIndex());
    }

    public static void testEmptyListIteratorPreviousIndex() {
        ListIterator<String> iterator = emptyListIterator();
        assertEquals("CollectionsTest.testEmptyListIteratorPreviousIndex", -1, iterator.previousIndex());
    }

    public static void testEmptyEnumeration() {
        Enumeration<String> enumeration = emptyEnumeration();
        assertEquals("CollectionsTest.testEmptyEnumeration", false, enumeration.hasMoreElements());
    }

    public static void testEmptyEnumerationNoSuchElementException() {
        Enumeration<String> enumeration = emptyEnumeration();
        try {
            enumeration.nextElement();
        } catch (NoSuchElementException e) {
            assertEquals("CollectionsTest.testEmptyEnumerationNoSuchElementException", null, e.getMessage());
            return;
        }
        fail("CollectionsTest.testEmptyEnumerationNoSuchElementException");
    }

    public static void testEmptyList() {
        List<String> actual = emptyList();
        List<String> expected = new ArrayList<>();
        assertEquals("CollectionsTest.testEmptyList", expected, actual);
    }

    public static void testEmptyMap() {
        Map<Integer, String> actual = emptyMap();
        Map<Integer, String> expected = new HashMap<>();
        assertEquals("CollectionsTest.testEmptyMap", expected, actual);
    }

    public static void testSingleton() {
        Set<String> expected = new HashSet<>();
        expected.add("a");
        Set<String> actual = singleton("a");
        assertEquals("CollectionsTest.testSingleton", expected, actual);
    }

    public static void testSingletonList() {
        List<String> actual = singletonList("abc");
        List<String> expected = asList("abc");
        assertEquals("CollectionsTest.testSingletonList", expected, actual);
    }

    public static void testSingletonMap() {
        Map<Integer, String> actual = singletonMap(1, "one");
        Map<Integer, String> expected = new HashMap<>();
        expected.put(1, "one");
        assertEquals("CollectionsTest.testSingletonMap", expected, actual);
    }

    public static void testNCopies() {
        List<String> actual = nCopies(3, "a");
        List<String> expected = asList("a", "a", "a");
        assertEquals("CollectionsTest.testNCopies", expected, actual);
    }

    public static void testNCopiesIllegalArgumentException() {
        try {
            List<String> actual = nCopies(-3, "a");
        } catch (IllegalArgumentException e) {
            assertEquals("CollectionsTest.testNCopiesIllegalArgumentException", "List length = -3", e.getMessage());
            return;
        }
        fail("CollectionsTest.testNCopiesIllegalArgumentException");
    }

    public static void testFrequency() {
        Collection<String> source = asList("a", "a", "a", "b", "c");
        assertEquals("CollectionsTest.testFrequency", 3, frequency(source, "a"));
    }

    public static void testDisjointTrue() {
        Collection<String> list1 = asList("a", "b");
        Collection<String> list2 = asList("c", "d");
        assertEquals("CollectionsTest.testDisjointTrue", true, disjoint(list1, list2));
    }

    public static void testDisjointFalse() {
        Collection<String> list1 = asList("a", "c");
        Collection<String> list2 = asList("c", "d");
        assertEquals("CollectionsTest.testDisjointFalse", false, disjoint(list1, list2));
    }

    public static void testAddAll() {
        ArrayList<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        addAll(actual, "c", "d");
        List<String> expected = asList("a", "b", "c", "d");
        assertEquals("CollectionsTest.testAddAll", expected, actual);
    }

    public static void testAddAllUnsupportedOperationException() {
        Collection<String> list = asList("a", "b");
        try {
            addAll(list, "d", "z");
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionsTest.testAddAllUnsupportedOperationException", null, e.getMessage());
            return;
        }
        fail("CollectionsTest.testAddAllUnsupportedOperationException");
    }

    public static void testNewSetFromMap() {
        Map<Integer, Boolean> map = new HashMap<>();
        Set<Integer> expected = new HashSet<>();
        assertEquals("CollectionsTest.testNewSetFromMap", expected, newSetFromMap(map));
    }

    public static void testNewSetFromMapIllegalArgumentException() {
        Map<Integer, Boolean> map = new HashMap<>();
        map.put(1, true);
        try {
            newSetFromMap(map);
        } catch (IllegalArgumentException e) {
            assertEquals("CollectionsTest.testNewSetFromMapIllegalArgumentException", "Map is non-empty", e.getMessage());
            return;
        }
        fail("CollectionsTest.testNewSetFromMapIllegalArgumentException");
    }

    public static void testAsLifoQueue() {
        Deque<Integer> queue = new ArrayDeque<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        Queue<Integer> expected = new ArrayDeque<>();
        expected.add(3);
        expected.add(2);
        expected.add(1);
        assertEquals("CollectionsTest.testAsLifoQueue", expected, asLifoQueue(queue));
    }

    private static List<String> getUnsortedCollection() {
        List<String> result = new ArrayList<>();
        result.add("Charlie");
        result.add("Alpha");
        result.add("Bravo");
        return result;
    }

    private static List<String> getSortedCollection() {
        List<String> result = new ArrayList<>();
        result.add("Alpha");
        result.add("Bravo");
        result.add("Charlie");
        return result;
    }
}