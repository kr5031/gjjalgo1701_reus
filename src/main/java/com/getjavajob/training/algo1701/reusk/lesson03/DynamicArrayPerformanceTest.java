package com.getjavajob.training.algo1701.reusk.lesson03;

import static com.getjavajob.training.algo1701.reusk.util.StopWatch.start;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.finish;
import static com.getjavajob.training.algo1701.reusk.util.StopWatch.getElapsedTimeMs;

import java.util.ArrayList;
import java.util.List;

public class DynamicArrayPerformanceTest {
    private static final int NUMBER_OF_ELEMENTS_ADD_BEGINNING = 200_000;
    private static final int NUMBER_OF_ELEMENTS_ADD_MIDDLE = 200_000;
    private static final int NUMBER_OF_ELEMENTS_ADD_END = 30_000_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_BEGINNING = 200_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_MIDDLE = 500_000;
    private static final int NUMBER_OF_ELEMENTS_REMOVE_END = 30_000_000;

    private static void testDynamicArrayAddToBeginning() {
        DynamicArray dynamicArray = new DynamicArray();
        System.out.println("-------- Addition to the beginning --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_BEGINNING; i++) {
            dynamicArray.add(0, new Object());
        }
        finish();
        printElapsedTime("DynamicArray.add");
    }

    private static void testDynamicArrayAddToMiddle() {
        DynamicArray dynamicArray = new DynamicArray();
        System.out.println("-------- Addition to the middle --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_MIDDLE; i++) {
            dynamicArray.add(dynamicArray.size() / 2, new Object());
        }
        finish();
        printElapsedTime("DynamicArray.add");
        return;
    }

    private static void testDynamicArrayAddToEnd() {
        DynamicArray dynamicArray = new DynamicArray();
        System.out.println("-------- Addition to the end --------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_END; i++) {
            dynamicArray.add(new Object());
        }
        finish();
        printElapsedTime("DynamicArray.add");
    }

    private static void testDynamicArrayRemoveBeginning() {
        DynamicArray dynamicArray = new DynamicArray();
        System.out.println("-------- Remove from the beginning --------");
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            dynamicArray.add(new Object());
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            dynamicArray.remove(0);
        }
        finish();
        printElapsedTime("DynamicArray.remove");
    }

    private static void testDynamicArrayRemoveMiddle() {
        DynamicArray dynamicArray = new DynamicArray();
        System.out.println("-------- Remove from the middle --------");
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            dynamicArray.add(new Object());
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            dynamicArray.remove(dynamicArray.size() / 2);
        }
        finish();
        printElapsedTime("DynamicArray.remove");
    }

    private static void testDynamicArrayRemoveEnd() {
        DynamicArray dynamicArray = new DynamicArray();
        System.out.println("-------- Remove from the end --------");
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            dynamicArray.add(new Object());
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            dynamicArray.remove(dynamicArray.size() - 1);
        }
        finish();
        printElapsedTime("DynamicArray.remove");
    }

    private static void testArrayListAddToBeginning() {
        List<Object> arrayList = new ArrayList<>();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_BEGINNING; i++) {
            arrayList.add(0, new Object());
        }
        finish();
        printElapsedTime("ArrayList.add");
    }

    private static void testArrayListAddToMiddle() {
        List<Object> arrayList = new ArrayList<>();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_MIDDLE; i++) {
            arrayList.add(arrayList.size() / 2, new Object());
        }
        finish();
        printElapsedTime("ArrayList.add");
    }

    private static void testArrayListAddToEnd() {
        List<Object> arrayList = new ArrayList<>();
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_ADD_END; i++) {
            arrayList.add(new Object());
        }
        finish();
        printElapsedTime("ArrayList.add");
    }

    private static void testArrayListRemoveBeginning() {
        List<Object> arrayList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            arrayList.add(new Object());
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_BEGINNING; i++) {
            arrayList.remove(0);
        }
        finish();
        printElapsedTime("ArrayList.remove");
    }

    private static void testArrayListRemoveMiddle() {
        List<Object> arrayList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            arrayList.add(new Object());
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_MIDDLE; i++) {
            arrayList.remove(arrayList.size() / 2);
        }
        finish();
        printElapsedTime("ArrayList.remove");
    }

    private static void testArrayListRemoveEnd() {
        List<Object> arrayList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            arrayList.add(new Object());
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_REMOVE_END; i++) {
            arrayList.remove(arrayList.size() - 1);
        }
        finish();
        printElapsedTime("ArrayList.remove");
    }

    private static void printElapsedTime(String testName) {
        System.out.printf(testName + ": %.0f ms\n", getElapsedTimeMs());
    }

}