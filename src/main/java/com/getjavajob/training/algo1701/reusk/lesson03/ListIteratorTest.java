package com.getjavajob.training.algo1701.reusk.lesson03;

import java.util.ConcurrentModificationException;

import com.getjavajob.training.algo1701.reusk.lesson03.DynamicArray.ListIterator;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class ListIteratorTest {
    public static final String CONCURRENT_MODIFY_MSG = "External modification detected";

    public static void main(String[] args) {
        testHasNextTrue();
        testHasNextFalse();
        testNext();
        testNextConcurrentModify();
        testHasPreviousTrue();
        testHasPreviousFalse();
        testPrevious();
        testPreviousConcurrentModify();
        testNextIndex();
        testPreviousIndex();
        testRemove();
        testRemoveConcurrentModify();
        testSet();
        testSetConcurrentModify();
        testAdd();
        testAddConcurrentModify();
    }

    public static DynamicArray getDynamicArray() {
        DynamicArray dynamicArray = new DynamicArray();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                dynamicArray.add(null);
            } else {
                dynamicArray.add(i);
            }
        }
        return dynamicArray;
    } // {0, 1, 2, 3, 4, null, 6, 7, 8, 9}

    public static void testHasNextTrue() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator();
        assertEquals("ListIteratorTest.testHasNextTrue", true, listIterator.hasNext());
    }

    public static void testHasNextFalse() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator(10);
        assertEquals("ListIteratorTest.testHasNextFalse", false, listIterator.hasNext());
    }

    public static void testNext() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator(0);
        Object[] actual = new Object[dynamicArray.size()];
        int counter = 0;
        while (listIterator.hasNext()) {
            actual[counter++] = listIterator.next();
        }
        Object[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("ListIteratorTest.testNext", expected, actual);
    }

    public static void testNextConcurrentModify() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator1 = dynamicArray.listIterator();
        ListIterator listIterator2 = dynamicArray.listIterator();
        if (listIterator2.hasNext()) {
            listIterator2.next();
            listIterator2.remove();
        }
        try {
            if (listIterator1.hasNext()) {
                listIterator1.next();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorTest.testNextConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorTest.testNextConcurrentModify");
    }

    public static void testHasPreviousTrue() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator(dynamicArray.size());
        assertEquals("ListIteratorTest.testHasPreviousTrue", true, listIterator.hasPrevious());
    }

    public static void testHasPreviousFalse() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator();
        assertEquals("ListIteratorTest.testHasPreviousFalse", false, listIterator.hasPrevious());
    }

    public static void testPrevious() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator(dynamicArray.size());
        Object[] actual = new Object[dynamicArray.size()];
        int counter = 0;
        while (listIterator.hasPrevious()) {
            actual[counter++] = listIterator.previous();
        }
        Object[] expected = {9, 8, 7, 6, null, 4, 3, 2, 1, 0};
        assertEquals("ListIteratorTest.testPrevious", expected, actual);
    }

    public static void testPreviousConcurrentModify() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator1 = dynamicArray.listIterator(dynamicArray.size());
        ListIterator listIterator2 = dynamicArray.listIterator(dynamicArray.size());
        if (listIterator2.hasPrevious()) {
            listIterator2.previous();
            listIterator2.remove();
        }
        try {
            if (listIterator1.hasPrevious()) {
                listIterator1.previous();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorTest.testPreviousConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorTest.testPreviousConcurrentModify");
    }

    public static void testNextIndex() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator();
        int[] actual = new int[dynamicArray.size()];
        int counter = 0;
        while (listIterator.hasNext()) {
            actual[counter++] = listIterator.nextIndex();
            listIterator.next();
        }
        int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertEquals("ListIteratorTest.testNextIndex", expected, actual);
    }

    public static void testPreviousIndex() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator(dynamicArray.size());
        int[] actual = new int[dynamicArray.size()];
        int counter = 0;
        while (listIterator.hasPrevious()) {
            actual[counter++] = listIterator.previousIndex();
            listIterator.previous();
        }
        int[] expected = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        assertEquals("ListIteratorTest.testPreviousIndex", expected, actual);
    }

    public static void testRemove() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator();
        if (listIterator.hasNext()) {
            listIterator.next();
            listIterator.remove();
        }
        Object[] actual = dynamicArray.toArray();
        Object[] expected = {1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("ListIteratorTest.testRemoveReturn", expected, actual);
    }

    public static void testRemoveConcurrentModify() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator1 = dynamicArray.listIterator();
        ListIterator listIterator2 = dynamicArray.listIterator();
        if (listIterator2.hasNext()) {
            listIterator2.next();
            listIterator2.remove();
        }
        try {
            if (listIterator1.hasNext()) {
                listIterator1.next();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorTest.testRemoveConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorTest.testRemoveConcurrentModify");
    }

    public static void testSet() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator();
        if (listIterator.hasNext()) {
            listIterator.next();
            listIterator.set("set");
        }
        Object[] expected = {"set", 1, 2, 3, 4, null, 6, 7, 8, 9};
        Object[] actual = dynamicArray.toArray();
        assertEquals("ListIteratorTest.testSet", expected, actual);
    }

    public static void testSetConcurrentModify() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator1 = dynamicArray.listIterator();
        ListIterator listIterator2 = dynamicArray.listIterator();
        if (listIterator2.hasNext()) {
            listIterator2.next();
            listIterator2.set("set");
        }
        try {
            if (listIterator1.hasNext()) {
                listIterator1.next();
            }
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorTest.testSetConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorTest.testSetConcurrentModify");
    }

    public static void testAdd() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator = dynamicArray.listIterator();
        listIterator.add("add");
        Object[] expected = {"add", 0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("ListIteratorTest.testAdd", expected, dynamicArray.toArray());
    }

    public static void testAddConcurrentModify() {
        DynamicArray dynamicArray = getDynamicArray();
        ListIterator listIterator1 = dynamicArray.listIterator();
        ListIterator listIterator2 = dynamicArray.listIterator();
        listIterator2.add("add listIterator2");
        try {
            listIterator1.add("add listIterator1");
        } catch (ConcurrentModificationException e) {
            assertEquals("ListIteratorTest.testAddConcurrentModify", CONCURRENT_MODIFY_MSG, e.getMessage());
            return;
        }
        fail("ListIteratorTest.testAddConcurrentModify");
    }

}
