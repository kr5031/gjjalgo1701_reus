package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class LinkedListQueue<V> extends AbstractLinkedListQueue {
    private SinglyLinkedList<V> singlyLinkedList;
    private int dequeueNumber;

    public LinkedListQueue(SinglyLinkedList<V> singlyLinkedList) {
        this.singlyLinkedList = singlyLinkedList;
    }

    public LinkedListQueue() {
        this(new SinglyLinkedList());
    }

    @Override
    public boolean add(Object o) {
        singlyLinkedList.add((V) o);
        return true;
    }

    @Override
    public V remove() {
        return singlyLinkedList.get(dequeueNumber++);
    }
}

abstract class AbstractLinkedListQueue implements Queue {
    @Override
    public boolean add(Object o) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public boolean offer(Object o) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public Object poll() {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public Object element() {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public Object peek() {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public Iterator iterator() {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException("The operation is not supported");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("The operation is not supported");
    }
}
