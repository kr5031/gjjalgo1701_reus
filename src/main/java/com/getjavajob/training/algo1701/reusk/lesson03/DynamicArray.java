package com.getjavajob.training.algo1701.reusk.lesson03;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

import static java.util.Arrays.copyOf;
import static java.lang.System.arraycopy;

public class DynamicArray {
    private static final String CONCURRENT_MODIFY_MSG = "External modification detected";
    private static final String WRONG_INDEX_MSG = "Wrong index #";
    private Object[] elements;
    private int nextIndexToWrite;
    private int modVersion;

    public DynamicArray(int size) {
        this.elements = new Object[size];
    }

    public DynamicArray() {
        this(10);
    }

    public boolean add(Object e) {
        checkArraySize();
        modVersion++;
        elements[nextIndexToWrite++] = e;
        return true;
    }

    public void add(int i, Object e) {
        checkIndex(i);
        checkArraySize();
        modVersion++;
        arraycopy(elements, i, elements, i + 1, nextIndexToWrite - i);
        elements[i] = e;
        nextIndexToWrite++;
    }

    public Object set(int i, Object e) {
        checkIndex(i);
        Object toReturn = elements[i];
        modVersion++;
        elements[i] = e;
        return toReturn;
    }

    public Object get(int i) {
        checkIndex(i);
        return elements[i];
    }

    public Object remove(int i) {
        checkIndex(i);
        Object toReturn = elements[i];
        int numToMove = size() - i - 1;
        if (numToMove > 0) {
            arraycopy(elements, i + 1, elements, i, nextIndexToWrite - i - 1);
        }
        modVersion++;
        elements[--nextIndexToWrite] = null;
        return toReturn;
    }

    public boolean remove(Object e) {
        int indexToRemove = findIndexOfObject(e);
        if (indexToRemove != -1) {
            modVersion++;
            remove(indexToRemove);
            return true;
        }
        return false;
    }

    public int size() {
        return nextIndexToWrite;
    }

    public int indexOf(Object e) {
        return findIndexOfObject(e);
    }

    public boolean contains(Object e) {
        return findIndexOfObject(e) > -1;
    }

    public Object[] toArray() {
        return copyOf(elements, size());
    }

    public ListIterator listIterator(int initialCursorPosition) {
        return new ListIterator(initialCursorPosition);
    }

    public ListIterator listIterator() {
        return listIterator(0);
    }

    private int findIndexOfObject(Object e) {
        for (int i = 0; i < elements.length; i++) {
            if (e == null) {
                if (get(i) == null) {
                    return i;
                } else {
                    continue;
                }
            }
            if (e.equals(get(i))) {
                return i;
            }
        }
        return -1;
    }

    private void checkIndex(int index) {
        if (index < 0 || index > nextIndexToWrite) {
            throw new ArrayIndexOutOfBoundsException(WRONG_INDEX_MSG + index);
        }
    }

    private void checkArraySize() {
        if (nextIndexToWrite == elements.length) {
            growArray();
        }
    }

    private void growArray() {
        int newSize = (int) (elements.length * 1.5);
        elements = copyOf(elements, newSize);
    }

    public class ListIterator {
        private int cursor;
        private int lastReturnedIndex = -1;
        private int expectedModVersion;

        public ListIterator(int cursor) {
            expectedModVersion = modVersion;
            this.cursor = cursor;
        }

        public ListIterator() {
            this(0);
        }

        public void checkModification() {
            if (expectedModVersion != modVersion) {
                throw new ConcurrentModificationException(CONCURRENT_MODIFY_MSG);
            }
        }

        public boolean hasNext() {
            return cursor < size();
        }

        public Object next() {
            checkModification();
            if (hasNext()) {
                lastReturnedIndex = cursor;
                return get(cursor++);
            } else {
                throw new NoSuchElementException(WRONG_INDEX_MSG + cursor);
            }
        }

        public boolean hasPrevious() {
            return cursor > 0;
        }

        public Object previous() {
            checkModification();
            if (hasPrevious()) {
                lastReturnedIndex = --cursor;
                return get(cursor);
            } else {
                throw new NoSuchElementException(WRONG_INDEX_MSG + cursor);
            }
        }

        //Returns the index of the element that would be returned by a subsequent call to next().
        // (Returns list size if the list iterator is at the end of the list.)
        public int nextIndex() {
            return cursor;
        }

        //Returns the index of the element that would be returned by a subsequent call to previous().
        // (Returns -1 if the list iterator is at the beginning of the list.)
        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            checkModification();
            DynamicArray.this.remove(lastReturnedIndex);
            expectedModVersion++;
            cursor = lastReturnedIndex;
            lastReturnedIndex = -1;
        }

        public void set(Object e) {
            checkModification();
            DynamicArray.this.set(lastReturnedIndex, e);
            expectedModVersion++;
        }

        public void add(Object e) {
            checkModification();
            DynamicArray.this.add(cursor, e);
            expectedModVersion++;
        }
    }

}
