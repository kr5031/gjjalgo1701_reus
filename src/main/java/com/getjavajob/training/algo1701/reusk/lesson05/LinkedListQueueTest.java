package com.getjavajob.training.algo1701.reusk.lesson05;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class LinkedListQueueTest {

    public static void main(String[] args) {
        testLinkedListQueue();
    }

    public static void testLinkedListQueue() {
        Queue<Integer> linkedListQueue = getLinkedListQueue();
        List<Integer> actual = new ArrayList();
        for (int i = 0; i < 10; i++) {
            actual.add(linkedListQueue.remove());
        }
        List expected = getList();
        assertEquals("LinkedListQueueTest.testLinkedListQueue", expected, actual);
    }

    private static Queue<Integer> getLinkedListQueue() {
        Queue<Integer> result = new LinkedListQueue<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                result.add(null);
            } else {
                result.add(i);
            }
        }
        return result;
    }

    private static List<Integer> getList() {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                result.add(null);
            } else {
                result.add(i);
            }
        }
        return result;
    }
}
