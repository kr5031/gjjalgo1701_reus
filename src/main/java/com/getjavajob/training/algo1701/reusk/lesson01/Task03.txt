convert -1, -115 in bin of size 1,2,4,8 byte

-1 (decimal) = 00000001 (binary, 1 byte)
Starting from the right, find the first "1", invert all of the bits to the left of that one
-1 (dec) = 11111111 (binary, 1 byte)
-1 (dec) = 11111111 11111111 (binary, 2 bytes)
-1 (dec) = 11111111 11111111 11111111 11111111 (binary, 4 bytes)
-1 (dec) = 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111 (binary, 8 bytes)

-115 (dec) = 10001101 (binary, 1 byte)
-115 (dec) = 11111111 10001101 (binary, 2 bytes)
-115 (dec) = 11111111 11111111 11111111 10001101 (binary, 4 bytes)
-115 (dec) = 11111111 11111111 11111111 11111111 11111111 11111111 11111111 10001101 (binary, 8 bytes)
