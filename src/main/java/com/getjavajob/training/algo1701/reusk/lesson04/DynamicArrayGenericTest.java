package com.getjavajob.training.algo1701.reusk.lesson04;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.lesson04.DynamicArrayGeneric.WRONG_INDEX_MSG;

public class DynamicArrayGenericTest {

    public static void main(String[] args) {
        testBooleanAdd();
        testVoidAddBeggining();
        testVoidAddMiddle();
        testVoidAddEnd();
        testCheckIndex();
        testGet();
        testRemoveBeginning();
        testRemoveMiddle();
        testRemoveEnd();
        testSize();
        testIndexOfExist();
        testIndexOfDontExist();
        testContainsTrue();
        testContainsFalse();
        testToArray();
    }

    public static void testBooleanAdd() {
        DynamicArrayGeneric<Integer> dynamicArrayGeneric = new DynamicArrayGeneric();
        assertEquals("DynamicArrayGenericTest.testBooleanAdd", true, dynamicArrayGeneric.add(777));
    }

    public static void testVoidAddBeggining() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        actual.add(0, 10);
        Integer[] expected = {10, 0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("DynamicArrayGenericTest.testVoidAddBeggining", expected, actual.toArray());
    }

    public static void testVoidAddMiddle() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        actual.add(5, 10);
        Integer[] expected = {0, 1, 2, 3, 4, 10, null, 6, 7, 8, 9};
        assertEquals("DynamicArrayGenericTest.testVoidAddMiddle", expected, actual.toArray());
    }

    public static void testVoidAddEnd() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        actual.add(10, 10);
        Integer[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9, 10};
        assertEquals("DynamicArrayGenericTest.testVoidAddEnd", expected, actual.toArray());
    }

    public static void testCheckIndex() {
        DynamicArrayGeneric<Integer> dynamicArrayGeneric = getDynamicArrayGeneric();
        try {
            dynamicArrayGeneric.add(100, 777);
        } catch (ArrayIndexOutOfBoundsException exception) {
            assertEquals("DynamicArrayGenericTest.testCheckIndex", WRONG_INDEX_MSG + "100", exception.getMessage());
        }
    }

    public static void testGet() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        assertEquals("DynamicArrayGenericTest.testGet", null, actual.get(5));
    }

    public static void testRemoveBeginning() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        actual.remove(0);
        Integer[] expected = {1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("DynamicArrayGenericTest.testRemoveBeginning", expected, actual.toArray());
    }

    public static void testRemoveMiddle() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        actual.remove(5);
        Integer[] expected = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        assertEquals("DynamicTestArray.testRemoveMiddle", expected, actual.toArray());
    }

    public static void testRemoveEnd() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        actual.remove(9);
        Integer[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8};
        assertEquals("DynamicArrayGenericTest.testRemoveEnd", expected, actual.toArray());
    }

    public static void testSize() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        assertEquals("DynamicArrayGenericTest.testSize", 10, actual.size());
    }

    public static void testIndexOfExist() {
        DynamicArrayGeneric actual = getDynamicArrayGeneric();
        assertEquals("DynamicArrayGenericTest.testIndexOfExist", 5, actual.indexOf(null));
    }

    public static void testIndexOfDontExist() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        assertEquals("DynamicArrayGenericTest.testIndexOfDontExist", -1, actual.indexOf("abc"));
    }

    public static void testContainsTrue() {
        DynamicArrayGeneric<Integer> actual = getDynamicArrayGeneric();
        assertEquals("DynamicArrayGenericTest.testContainsTrue", true, actual.contains(null));
    }

    public static void testContainsFalse() {
        DynamicArrayGeneric actual = getDynamicArrayGeneric();
        assertEquals("DynamicArrayGenericTest.testContainsFalse", false, actual.contains("abc"));
    }

    public static void testToArray() {
        Object[] actual = getDynamicArrayGeneric().toArray();
        Integer[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("DynamicArrayGenericTest.testToArray", expected, actual);
    }

    private static DynamicArrayGeneric<Integer> getDynamicArrayGeneric() { // {0, 1, 2, 3, 4, null, 6, 7, 8, 9}
        DynamicArrayGeneric<Integer> result = new DynamicArrayGeneric<>();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                result.add(null);
                continue;
            }
            result.add(i);
        }
        return result;
    }

}
