package com.getjavajob.training.algo1701.reusk.lesson05;

public class LinkedListStack<E> implements Stack<E> {
    private SinglyLinkedList<E> singlyLinkedList;
    private int stackTop;

    public LinkedListStack(SinglyLinkedList singlyLinkedList) {
        this.singlyLinkedList = singlyLinkedList;
        stackTop = singlyLinkedList.size() - 1;
    }

    public LinkedListStack() {
        this(new SinglyLinkedList());
    }

    @Override
    public void push(E e) {
        singlyLinkedList.add(e);
        stackTop++;
    }

    @Override
    public E pop() {
        return singlyLinkedList.get(stackTop--);
    }
}
