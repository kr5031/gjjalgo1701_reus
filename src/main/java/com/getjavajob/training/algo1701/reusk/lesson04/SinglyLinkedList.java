package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<V> {
    private static final String WRONG_ARGUMENT_EXCEPTION = "Wrong argument #";
    private int size;
    private Element<V> first;
    private Element<V> nextElement;
    private Element<V> prevElement;
    private Element<V> currentElement;

    public int size() {
        return size;
    }

    public boolean add(V val) {
        if (size == 0) {
            first = new Element<>(null, val);
            size++;
        } else {
            Element<V> toAdd = new Element<>(null, val);
            Element<V> currentLast = findLast();
            currentLast.next = toAdd;
            size++;
        }
        return true;
    }

    public V get(int index) {
        checkIndex(index);
        Element<V> current = first;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        return current.val;
    }

    public void reverse() {
        relink(first, first.next, null);
    }

    private void relink(Element<V> ptrOne, Element<V> ptrTwo, Element<V> prev) {
        if (ptrTwo != null) {
            if (ptrTwo.next != null) {
                Element t1 = ptrTwo;
                Element t2 = ptrTwo.next;
                ptrOne.next = prev;
                prev = ptrOne;
                relink(t1, t2, prev);
            } else {
                first = ptrTwo;
                ptrTwo.next = ptrOne;
                ptrOne.next = prev;
            }
        }
    }

    public List<V> asList() {
        List<V> result = new ArrayList<V>();
        Element<V> current = first;
        while (current != null) {
            result.add(current.val);
            current = current.next;
        }
        return result;
    }

    private void checkIndex(int index) {
        if (index < 0 || index > size() - 1) {
            throw new IllegalArgumentException(WRONG_ARGUMENT_EXCEPTION + index);
        }
    }

    private Element<V> findLast() {
        Element<V> current = first;
        while (current.next != null) {
            current = current.next;
        }
        return current;
    }

    private static class Element<V> {
        private Element<V> next;
        private V val;

        public Element(Element<V> next, V val) {
            this.next = next;
            this.val = val;
        }
    }

}