package com.getjavajob.training.algo1701.reusk.lesson08;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;

public abstract class BalanceableTree<E extends Comparable> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param newParent     new parent
     * @param newChild      new child
     * @param makeLeftChild whether new child must be left or right
     */
    protected void relink(NodeImpl<E> newParent, NodeImpl<E> newChild, boolean makeLeftChild) {
        NodeImpl<E> grandParent = validate(newChild.getParent());
        if (grandParent != null && isLeftChild(grandParent, newChild)) {
            grandParent.setLeftChild(newParent);
        } else if (grandParent != null && isRightChild(grandParent, newChild)) {
            grandParent.setRightChild(newParent);
        }
        newParent.setParent(grandParent);
        if (grandParent == null) {
            root = newParent;
        }
        boolean isRightMove = !makeLeftChild; //redundant variables were added for better readability
        boolean isLeftMove = makeLeftChild;
        if (isRightMove) {
            NodeImpl<E> rightChildOfNewParent = validate(right(newParent));
            newChild.setLeftChild(rightChildOfNewParent);
            newParent.setRightChild(newChild);
        } else if (isLeftMove) { //for better readability
            NodeImpl<E> leftChildOfNewParent = validate(left(newParent));
            newChild.setRightChild(leftChildOfNewParent);
            newParent.setLeftChild(newChild);
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> child = validate(n);
        NodeImpl<E> parent = validate(child.getParent());
        boolean isLeftMove = isRightChild(parent, child);
        relink(child, parent, isLeftMove);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.init.severynv.algo.tree.Node)} to reduce the height of subtree rooted
     * at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> grandChild = validate(n);
        NodeImpl<E> parent = validate(grandChild.getParent());
        NodeImpl<E> grandParent = validate(parent.getParent());
        NodeImpl<E> result = parent;
        boolean isLine = isLeftChild(grandParent, parent) && isLeftChild(parent, grandChild) ||
                isRightChild(grandParent, parent) && isRightChild(parent, grandChild);
        if (isLine) {
            rotate(parent);
        } else {
            result = grandChild;
            rotate(grandChild);
            rotate(grandChild);
        }
        return result;
    }
}
