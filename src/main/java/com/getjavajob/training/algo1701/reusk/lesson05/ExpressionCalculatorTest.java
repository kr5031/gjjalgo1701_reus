package com.getjavajob.training.algo1701.reusk.lesson05;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.lesson05.ExpressionCalculator.convertToPostfix;
import static com.getjavajob.training.algo1701.reusk.lesson05.ExpressionCalculator.calculatePostfix;

public class ExpressionCalculatorTest {

    public static void main(String[] args) {
        testConvertPostfixSimple1();
        testConvertPostfixSimple2();
        testConvertPostfixSimpleWithSpaces1();
        testConvertPostfixSimpleWithSpaces2();
        testConvertPostfixParentheses();
        testCalculatePostfixSimple();
        testCalculatePostfixWithParentheses();
        testCalculatePostfixWithMultiDigitalNumbers();
    }

    public static void testConvertPostfixSimple1() {
        assertEquals("ExpressionCalculatorTest.testConvertPostfixSimple1", "12 13 14*+", convertToPostfix("12+13*14"));
    }

    public static void testConvertPostfixSimple2() {
        assertEquals("ExpressionCalculatorTest.testConvertPostfixSimple2", "111 222 333*+444 555/-",
                convertToPostfix("111+222*333-444/555"));
    }

    public static void testConvertPostfixParentheses() {
        assertEquals("ExpressionCalculatorTest.testConvertPostfixParentheses", "2 3 16 4/2-*+",
                convertToPostfix("2+3*((16/4)-2)"));
    }

    public static void testConvertPostfixSimpleWithSpaces1() {
        assertEquals("ExpressionCalculatorTest.testConvertPostfixSimpleWithSpaces1", "11 22 33*+",
                convertToPostfix("11 + 22 * 33"));
    }

    public static void testConvertPostfixSimpleWithSpaces2() {
        assertEquals("ExpressionCalculatorTest.testConvertPostfixSimpleWithSpaces2", "11 22 44 33-55**+",
                convertToPostfix("11 + 22 * ((44 - 33) * 55)"));
    }

    public static void testCalculatePostfixSimple() {
        assertEquals("ExpressionCalculatorTest.testCalculatePostfixSimple", 6, calculatePostfix("2 2 2*+"));
    }

    public static void testCalculatePostfixWithParentheses() {
        assertEquals("ExpressionCalculatorTest.testCalculatePostfixWithParentheses", 8, calculatePostfix("2 2+2*"));
    }

    public static void testCalculatePostfixWithMultiDigitalNumbers() {
        assertEquals("ExpressionCalculatorTest.testCalculatePostfixWithMultiDigitalNumbers", 8,
                calculatePostfix("2 3 16 4/2-*+"));
    }

}
