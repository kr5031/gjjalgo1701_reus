package com.getjavajob.training.algo1701.reusk.lesson09;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class SortedMapTest {

    public static void main(String[] args) {
        testSubmap();
        testSubmapEmpty();
        testSubmapNullPointerException();
        testSubmapIllegalArgumentException();
        testHeadmap();
        testHeadmapNullPointerException();
        testHeadmapIllegalArgumentException();
        testTailmap();
        testTailmapNullPointerException();
        testTailmapIllegalArgumentException();
        testFirstKey();
        testFirstKeyNoSuchElementException();
        testLastKey();
        testLastKeyNoSuchElementException();
        testKeyset();
        testValues();
    }

    public static void testSubmap() {
        SortedMap<Integer, String> map = getSortedMap();
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(0, "zero");
        expected.put(1, "one");
        assertEquals("SortedMapTest.testSubmap", expected, map.subMap(0, 2));
    }

    public static void testSubmapEmpty() {
        SortedMap<Integer, String> map = getSortedMap();
        SortedMap<Integer, String> expected = new TreeMap<>();
        assertEquals("SortedMapTest.testSubmapEmpty", expected, map.subMap(0, 0));
    }

    public static void testSubmapNullPointerException() {
        SortedMap<Integer, String> map = getSortedMap();
        try {
            map.subMap(null, null);
        } catch (NullPointerException e) {
            assertEquals("SortedMapTest.testSubmapNullPointerException", null, e.getMessage());
            return;
        }
        fail("SortedMapTest.testSubmapNullPointerException");
    }

    public static void testSubmapIllegalArgumentException() {
        SortedMap<Integer, String> map = getSortedMap();
        try {
            map.subMap(2, 0);
        } catch (IllegalArgumentException e) {
            assertEquals("SortedMapTest.testSubmapIllegalArgumentException", "fromKey > toKey", e.getMessage());
            return;
        }
        fail("SortedMapTest.testSubmapIllegalArgumentException");
    }

    public static void testHeadmap() {
        SortedMap<Integer, String> map = getSortedMap();
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(0, "zero");
        expected.put(1, "one");
        expected.put(2, "two");
        assertEquals("SortedMapTest.testHeadmap", expected, map.headMap(3));
    }

    public static void testHeadmapNullPointerException() {
        SortedMap<Integer, String> map = getSortedMap();
        try {
            map.headMap(null);
        } catch (NullPointerException e) {
            assertEquals("SortedMapTest.testHeadmapNullPointerException", null, e.getMessage());
            return;
        }
        fail("SortedMapTest.testHeadmapNullPointerException");
    }

    public static void testHeadmapIllegalArgumentException() {
        SortedMap<Integer, String> map = getSortedMap();
        SortedMap<Integer, String> subMap = map.subMap(0, 2);
        try {
            subMap.headMap(9);
        } catch (IllegalArgumentException e) {
            assertEquals("SortedMapTest.testHeadmapIllegalArgumentException", "toKey out of range", e.getMessage());
            return;
        }
        fail("SortedMapTest.testHeadmapIllegalArgumentException");
    }

    public static void testTailmap() {
        SortedMap<Integer, String> map = getSortedMap();
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(3, "three");
        expected.put(4, "four");
        assertEquals("SortedMapTest.testTailmap", expected, map.tailMap(3));
    }

    public static void testTailmapNullPointerException() {
        SortedMap<Integer, String> map = getSortedMap();
        try {
            map.tailMap(null);
        } catch (NullPointerException e) {
            assertEquals("SortedMapTest.testTailmapNullPointerException", null, e.getMessage());
            return;
        }
        fail("SortedMapTest.testTailmapNullPointerException");
    }

    public static void testTailmapIllegalArgumentException() {
        SortedMap<Integer, String> map = getSortedMap();
        SortedMap<Integer, String> subMap = map.subMap(0, 2);
        try {
            subMap.tailMap(9);
        } catch (IllegalArgumentException e) {
            assertEquals("SortedMapTest.testTailmapIllegalArgumentException", "fromKey out of range", e.getMessage());
            return;
        }
        fail("SortedMapTest.testTailmapIllegalArgumentException");
    }

    public static void testFirstKey() {
        SortedMap<Integer, String> map = getSortedMap();
        assertEquals("SortedMapTest.testFirstKey", 0, (int) map.firstKey());
    }

    public static void testFirstKeyNoSuchElementException() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.firstKey();
        } catch (NoSuchElementException e) {
            assertEquals("SortedMapTest.testFirstKeyNoSuchElementException", null, e.getMessage());
            return;
        }
        fail("SortedMapTest.testFirstKeyNoSuchElementException");
    }

    public static void testLastKey() {
        SortedMap<Integer, String> map = getSortedMap();
        assertEquals("SortedMapTest.testLastKey", 4, (int) map.lastKey());
    }

    public static void testLastKeyNoSuchElementException() {
        SortedMap<Integer, String> map = new TreeMap<>();
        try {
            map.firstKey();
        } catch (NoSuchElementException e) {
            assertEquals("SortedMapTest.testLastKeyNoSuchElementException", null, e.getMessage());
            return;
        }
        fail("SortedMapTest.testLastKeyNoSuchElementException");
    }

    public static void testKeyset() {
        SortedMap<Integer, String> map = getSortedMap();
        SortedSet<Integer> expected = new TreeSet<>();
        for (int i = 0; i < 5; i++) {
            expected.add(i);
        }
        assertEquals("SortedMapTest.testKeyset", expected, map.keySet());
    }

    public static void testValues() {
        SortedMap<Integer, String> map = getSortedMap();
        Collection<String> expected = new ArrayList<>();
        expected.add("zero");
        expected.add("one");
        expected.add("two");
        expected.add("three");
        expected.add("four");
        assertEquals("SortedMapTest.testValues", expected, map.values());
    }

    public static SortedMap<Integer, String> getSortedMap() {
        SortedMap<Integer, String> map = new TreeMap<>();
        map.put(0, "zero");
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        return map;
    }
}
