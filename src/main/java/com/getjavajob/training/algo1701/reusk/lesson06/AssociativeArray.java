package com.getjavajob.training.algo1701.reusk.lesson06;

import static java.lang.Math.abs;

public class AssociativeArray<K, V> {
    private static final double LOADING = 0.75;
    private Entry<K, V>[] elements;
    private int numberOfAddedElements;

    public AssociativeArray(int initialSize) {
        this.elements = new Entry[initialSize];
    }

    public AssociativeArray() {
        this(16);
    }

    public V add(K key, V val) {
        checkSize();
        Entry<K, V> toAdd = new Entry(key, val, null);
        int i = calculateIndexFor(key, elements.length);
        if (elements[i] == null) {
            elements[i] = toAdd;
            numberOfAddedElements++;
        } else {
            for (Entry<K, V> e = elements[i]; e != null; e = e.next) {
                if (e.getKey() == null && key == null || hash(e.getKey()) == hash(key)
                        && key.equals(e.getKey())) {
                    V toReturn = e.getValue();
                    e.setValue(val);
                    return toReturn;
                } else if (e.next == null) {
                    e.next = toAdd;
                    return null;
                }
            }
        }
        return null;
    }

    public V get(K key) {
        int i = calculateIndexFor(key, elements.length);
        for (Entry<K, V> e = elements[i]; e != null; e = e.next) {
            if (e.getKey() == key) {
                return e.getValue();
            }
        }
        return null;
    }

    public V remove(K key) {
        int i = calculateIndexFor(key, elements.length);
        if (elements[i] == null) {
            return null;
        } else {
            Entry<K, V> firstEntry = elements[i];
            if (key == null && firstEntry.getKey() == null || key.equals(firstEntry.getKey())) {
                V toReturn = firstEntry.getValue();
                elements[i] = firstEntry.next;
                return toReturn;
            } else {
                for (Entry<K, V> e = firstEntry; e.next != null; e = e.next) {
                    if (key == null && e.next.getKey() == null || key.equals(e.next.getKey())) {
                        V toReturn = e.next.getValue();
                        e.next = e.next.next;
                        return toReturn;
                    }
                }
            }
        }
        return null;
    }

    private void checkSize() {
        if (numberOfAddedElements >= LOADING * elements.length) {
            rehash();
        }
    }

    private void rehash() {
        Entry<K, V>[] newElements = new Entry[elements.length * 2];
        for (int i = 0; i < elements.length; i++) {
            Entry<K, V> e = elements[i];
            if (e != null) {
                int newIndex = calculateIndexFor(e.getKey(), newElements.length);
                newElements[newIndex] = e;
            }
        }
        elements = newElements;
    }

    private int hash(K key) {
        int h;
        return key == null ? 0 : (h = key.hashCode()) ^ h >>> 16;
    }

    private int calculateIndexFor(K key, int arrayLength) {
        return key == null ? 0 : abs(hash(key) % arrayLength);
    }

    private static class Entry<K, V> {
        private final K key;
        private V value;
        private Entry<K, V> next;

        private Entry(K key, V value, Entry<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        private K getKey() {
            return key;
        }

        private V getValue() {
            return value;
        }

        private void setValue(V value) {
            this.value = value;
        }
    }
}