package com.getjavajob.training.algo1701.reusk.lesson10;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static java.lang.Math.random;
import static java.lang.Math.round;

public class SortingsTest {

    public static void main(String[] args) {
        testBubbleSort();
        testInsertSort();
        testQuickSort();
        testMergeSort();
        testQuickSortOverflow();
        testMergeSortOverflow();
    }

    public static void testBubbleSort() {
        Sortings sort = new Sortings();
        assertEquals("SortingsTest.testBubbleSort", getSortedArray(), sort.bubbleSort(getUnsortedArray()));
    }

    public static void testInsertSort() {
        Sortings sort = new Sortings();
        assertEquals("SortingsTest.testInsertSort", getSortedArray(), sort.insertSort(getUnsortedArray()));
    }

    public static void testQuickSort() {
        Sortings sort = new Sortings();
        assertEquals("SortingsTest.testQuickSort", getSortedArray(), sort.quickSort(getUnsortedArray()));
    }

    public static void testMergeSort() {
        Sortings sort = new Sortings();
        assertEquals("SortingsTest.testMergeSort", getSortedArray(), sort.mergeSort(getUnsortedArray()));
    }

    /*
        the following 2 tests always fail since we don't know what expected resulting arrays are
        though, it proves such arrays can be sorted
     */
    public static void testQuickSortOverflow() {
        Sortings sort = new Sortings();
        assertEquals("SortingsTest.testQuickSortOverflow", getSortedArray(), sort.quickSort(getByteMaxArray()));
    }

    public static void testMergeSortOverflow() {
        Sortings sort = new Sortings();
        assertEquals("SortingsTest.testMergeSortOverflow", getSortedArray(), sort.mergeSort(getByteMaxArray()));
    }

    public static Integer[] getUnsortedArray() {
        return new Integer[]{1, 7, 0, 6, 2, 4, 5, 3, 9, 8};
    }

    public static Integer[] getSortedArray() {
        return new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    }

    public static Integer[] getByteMaxArray() {
        Integer[] bigArray = new Integer[Integer.MAX_VALUE / 2];
        for (int i = 0; i < Integer.MAX_VALUE / 2; i++) {
            bigArray[i] = (int) round(random() * 10);
        }
        return bigArray;
    }

}