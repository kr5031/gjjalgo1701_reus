package com.getjavajob.training.algo1701.reusk.lesson04;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class ListTest {

    public static void main(String[] args) {
        testEqualsTrue();
        testEqualsFalse();
        testGet();
        testSet();
        testAdd();
        testRemove();
        testIndexOfExisting();
        testIndexOfNonExisting();
        testLastIndexOfExisting();
        testLastIndexOfNonExisting();
        testSubList();
    }

    public static void testEqualsTrue() {
        List<Integer> arrayList1 = getArrayList();
        List<Integer> arrayList2 = getArrayList();
        assertEquals("ListTest.testEqualsTrue", true, arrayList1.equals(arrayList2));
    }

    public static void testEqualsFalse() {
        List<Integer> arrayList1 = getArrayList();
        List<Integer> arrayList2 = getNumbersOutOfRange();
        assertEquals("ListTest.testEqualsFalse", false, arrayList1.equals(arrayList2));
    }

    public static void testGet() {
        List<Integer> arrayList = getArrayList();
        assertEquals("ListTest.testGet", 5, (int) arrayList.get(5));
    }

    public static void testSet() {
        List<Integer> arrayList = getArrayList();
        arrayList.set(5, null);
        Integer[] expected = {0, 1, 2, 3, 4, null, 6, 7, 8, 9};
        assertEquals("ListTest.testSet", expected, arrayList.toArray());
    }

    public static void testAdd() {
        List<Integer> arrayList = getArrayList();
        arrayList.add(5, 777);
        Integer[] expected = {0, 1, 2, 3, 4, 777, 5, 6, 7, 8, 9};
        assertEquals("ListTest.testAdd", expected, arrayList.toArray());
    }

    public static void testRemove() {
        List<Integer> arrayList = getArrayList();
        assertEquals("ListTest.testRemoveReturn", 5, (int) arrayList.remove(5));
    }

    public static void testIndexOfExisting() {
        List<Integer> arrayList = getArrayList();
        assertEquals("ListTest.testIndexOfExisting", 6, arrayList.indexOf(6));
    }

    public static void testIndexOfNonExisting() {
        List<Integer> arrayList = getArrayList();
        assertEquals("ListTest.testIndexOfNonExisting", -1, arrayList.indexOf(777));
    }

    public static void testLastIndexOfExisting() {
        List<Integer> arrayList = getArrayList();
        arrayList.add(1);
        assertEquals("ListTest.testLastIndexOfExisting", 10, arrayList.lastIndexOf(1));
    }

    public static void testLastIndexOfNonExisting() {
        List<Integer> arrayList = getArrayList();
        assertEquals("ListTest.testLastIndexOfNonExisting", -1, arrayList.lastIndexOf(777));
    }

    public static void testSubList() {
        List<Integer> arrayList = getArrayList();
        Object[] expected = {3, 4, 5};
        assertEquals("ListTest.testSubList", expected, arrayList.subList(3, 6).toArray());
    }

    private static List<Integer> getArrayList() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

    private static List<Integer> getNumbersOutOfRange() {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 100; i < 110; i++) {
            arrayList.add(i);
        }
        return arrayList;
    }

}