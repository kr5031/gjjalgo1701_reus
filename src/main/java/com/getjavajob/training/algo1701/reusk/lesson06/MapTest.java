package com.getjavajob.training.algo1701.reusk.lesson06;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class MapTest {

    public static void main(String[] args) {
        testSize();
        testIsEmptyTrue();
        testIsEmptyFalse();
        testContainsKeyTrue();
        testContainsKeyFalse();
        testContainsValueTrue();
        testContainsValueFalse();
        testGet();
        testPutNew();
        testPutChange();
        testRemoveExisting();
        testRemoveNonExisting();
        testPutAll();
        testClear();
        testKeySet();
        testValues();
        testEntrySet();
        testEqualsTrue();
        testEqualsFalse();
        testGetOrDefaultExisting();
        testGetOrDefaultNonExisting();
        testPutIfAbsentExisting();
        testPutIfAbsentNonExisting();
        testReplaceTrue();
        testReplaceFalse();
        testHashCodeTrue();
        testHashCodeFalse();
    }

    public static void testSize() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testSize", 6, map.size());
    }

    public static void testIsEmptyTrue() {
        Map<Integer, String> map = new HashMap<>();
        assertEquals("MapTest.testIsEmptyTrue", true, map.isEmpty());
    }

    public static void testIsEmptyFalse() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testIsEmptyFalse", false, map.isEmpty());
    }

    public static void testContainsKeyTrue() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testContainsKeyTrue", true, map.containsKey(null));
    }

    public static void testContainsKeyFalse() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testContainsKeyFalse", false, map.containsKey(777));
    }

    public static void testContainsValueTrue() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testContainsValueTrue", true, map.containsValue("ZERO"));
    }

    public static void testContainsValueFalse() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testContainsValueFalse", false, map.containsValue("HUNDRED"));
    }

    public static void testGet() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testGet", "ZERO", map.get(null));
    }

    public static void testPutNew() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testPutNew", null, map.put(100, "HUNDRED"));
    }

    public static void testPutChange() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testPutChange", "ZERO", map.put(null, "NOT NULL"));
    }

    public static void testRemoveExisting() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testRemoveExisting", "ZERO", map.remove(null));
    }

    public static void testRemoveNonExisting() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testRemoveNonExisting", null, map.remove(777));
    }

    public static void testPutAll() {
        Map<Integer, String> map1 = getHashMap();
        Map<Integer, String> map2 = new HashMap<>();
        map2.put(101, "ONE HUNDRED ONE");
        map2.put(102, "ONE HUNDRED TWO");
        map1.putAll(map2);
        assertEquals("MapTest.testPutAll", "ONE HUNDRED TWO", map1.get(102));
    }

    public static void testClear() {
        Map<Integer, String> map = getHashMap();
        map.clear();
        assertEquals("MapTest.testClear", 0, map.size());
    }

    public static void testKeySet() {
        Map<Integer, String> map = getHashMap();
        Set<Integer> expected = getSet();
        assertEquals("MapTest.testKeySet", expected, map.keySet());
    }

    public static void testValues() {
        Map<Integer, String> map = getHashMap();
        Collection<String> expected = getValues();
        assertEquals("MapTest.testValues", expected, map.values());
    }

    public static void testEntrySet() {
        Map<Integer, String> map = getHashMap();
        Set<Map.Entry<Integer, String>> expected = getEntrySet();
        assertEquals("MapTest.testEntrySet", expected, map.entrySet());
    }

    public static void testEqualsTrue() {
        Map<Integer, String> map1 = getHashMap();
        Map<Integer, String> map2 = getHashMap();
        assertEquals("MapTest.testEqualsTrue", true, map1.equals(map2));
    }

    public static void testEqualsFalse() {
        Map<Integer, String> map1 = getHashMap();
        Map<Integer, String> map2 = getHashMap();
        map2.put(700, "SEVEN HUNDREDS");
        assertEquals("MapTest.testEqualsFalse", false, map1.equals(map2));
    }

    public static void testGetOrDefaultExisting() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testGetOrDefaultExisting", "ZERO", map.getOrDefault(null, "DEFAULT"));
    }

    public static void testGetOrDefaultNonExisting() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testGetOrDefaultNonExisting", "DEFAULT", map.getOrDefault(777, "DEFAULT"));
    }

    public static void testPutIfAbsentExisting() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testPutIfAbsentExisting", "ZERO", map.putIfAbsent(null, "NOT ZERO"));
    }

    public static void testPutIfAbsentNonExisting() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testPutIfAbsentExisting", null, map.putIfAbsent(100, "HUNDRED"));
    }

    public static void testReplaceTrue() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testReplaceTrue", true, map.replace(null, "ZERO", "NOT ZERO"));
    }

    public static void testReplaceFalse() {
        Map<Integer, String> map = getHashMap();
        assertEquals("MapTest.testReplaceFalse", false, map.replace(777, "OLD", "NEW"));
    }

    public static void testHashCodeTrue() {
        Map<Integer, String> map1 = getHashMap();
        Map<Integer, String> map2 = getHashMap();
        assertEquals("MapTest.testHashCodeTrue", true, map1.hashCode() == map2.hashCode());
    }

    public static void testHashCodeFalse() {
        Map<Integer, String> map1 = getHashMap();
        Map<Integer, String> map2 = new HashMap<>();
        assertEquals("MapTest.testHashCodeFalse", false, map1.hashCode() == map2.hashCode());
    }

    private static Map<Integer, String> getHashMap() {
        Map<Integer, String> map = new HashMap<>();
        map.put(null, "ZERO");
        map.put(1, "ONE");
        map.put(2, "TWO");
        map.put(3, "THREE");
        map.put(4, "FOUR");
        map.put(5, "FIVE");
        return map;
    }

    private static Set<Integer> getSet() {
        Set<Integer> set = new HashSet<>();
        set.add(null);
        for (int i = 1; i < 6; i++) {
            set.add(i);
        }
        return set;
    }

    private static Collection<String> getValues() {
        Collection<String> collection = new ArrayList<>();
        collection.add("ZERO");
        collection.add("ONE");
        collection.add("TWO");
        collection.add("THREE");
        collection.add("FOUR");
        collection.add("FIVE");
        return collection;
    }

    private static Set<Map.Entry<Integer, String>> getEntrySet() {
        Set<Map.Entry<Integer, String>> entrySet = new HashSet<>();
        entrySet.add(new AbstractMap.SimpleEntry<Integer, String>(null, "ZERO"));
        entrySet.add(new AbstractMap.SimpleEntry<Integer, String>(1, "ONE"));
        entrySet.add(new AbstractMap.SimpleEntry<Integer, String>(2, "TWO"));
        entrySet.add(new AbstractMap.SimpleEntry<Integer, String>(3, "THREE"));
        entrySet.add(new AbstractMap.SimpleEntry<Integer, String>(4, "FOUR"));
        entrySet.add(new AbstractMap.SimpleEntry<Integer, String>(5, "FIVE"));
        return entrySet;
    }

}
