package com.getjavajob.training.algo1701.reusk.lesson07.binary;

import com.getjavajob.training.algo1701.reusk.lesson07.Node;
import com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree.NodeImpl;

import java.util.ArrayList;
import java.util.Collection;

import static com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree.NODE_NULL_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree.WRONG_NODE_TYPE_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree.ROOT_NOT_NULL_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree.CHILDREN_NOT_NULL_MSG;
import static com.getjavajob.training.algo1701.reusk.lesson07.binary.LinkedBinaryTree.CAN_NOT_DELETE_MSG;
import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;
import static com.getjavajob.training.algo1701.reusk.util.Assert.fail;

public class LinkedBinaryTreeTest {

    public static void main(String[] args) {
        testValidate();
        testValidateException();
        testAddRoot();
        testAddRootException();
        testAdd();
        testAddException();
        testAddLeft();
        testAddLeftException();
        testAddRight();
        testAddRightException();
        testSet();
        testSetException();
        testRemove();
        testRemoveException();
        testLeft();
        testRight();
        testRoot();
        testParent();
        testSize();
        testNodes();
        testIsInternalTrue();
        testIsInternalFalse();
        testIsExternalTrue();
        testIsExternalFalse();
        testIsRootTrue();
        testIsRootFalse();
        testIsEmptyTrue();
        testIsEmptyFalse();
        testPreorder();
        testInorder();
        testPostorder();
        testBreadthFirst();
        testSibling();
        testChildren();
        testChildrenNumber();
    }

    /*  linked binary tree for tests
                            7
                          /    \
                         3      6
                        / \    /
                       5   9  8
                          /
                         12
       */
    private static LinkedBinaryTree<Integer> getLinkedBinaryTree() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(7);
        NodeImpl<Integer> root = linkedBinaryTree.validate(linkedBinaryTree.root());
        linkedBinaryTree.add(root, 3);
        linkedBinaryTree.add(root, 6);
        linkedBinaryTree.add(root.getLeftChild(), 5);
        linkedBinaryTree.add(linkedBinaryTree.add(root.getLeftChild(), 9), 12);
        linkedBinaryTree.add(root.getRightChild(), 8);
        return linkedBinaryTree;
    }

    public static void testValidate() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        NodeImpl<Integer> expected = new NodeImpl<>(null, 7);
        assertEquals("LinkedBinaryTreeTest.testValidate", expected, linkedBinaryTree.validate(linkedBinaryTree.root()));
    }

    public static void testValidateException() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        NodeImpl<Integer> expected = new NodeImpl<>(null, 7);
        try {
            linkedBinaryTree.validate(() -> null);
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testValidateException", WRONG_NODE_TYPE_MSG, e.getMessage());
            return;
        }
        fail("LinkedBinaryTreeTest.testValidateException");
    }

    public static void testAddRoot() {
        LinkedBinaryTree<Integer> emptyLinkedBinaryTree = new LinkedBinaryTree<>();
        LinkedBinaryTree<Integer> filledLinkedBinaryTree = getLinkedBinaryTree();
        assertEquals("LinkedBinaryTreeTest.testAddRoot", filledLinkedBinaryTree.root(), emptyLinkedBinaryTree.addRoot(7));
    }

    public static void testAddRootException() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        try {
            linkedBinaryTree.addRoot(1);
        } catch (IllegalStateException e) {
            assertEquals("LinkedBinaryTreeTest.testAddRootException", ROOT_NOT_NULL_MSG, e.getMessage());
            return;
        }
        fail("LinkedBinaryTreeTest.testAddRootException");
    }

    public static void testAdd() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        Node<Integer> root = linkedBinaryTree.addRoot(777);
        Node<Integer> expected = new NodeImpl<>(root, 100);
        assertEquals("LinkedBinaryTreeTest.testAdd", expected, linkedBinaryTree.add(root, 100));
    }

    public static void testAddException() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        try {
            linkedBinaryTree.add(linkedBinaryTree.root(), 3);
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testAddException", CHILDREN_NOT_NULL_MSG, e.getMessage());
            return;
        }
        fail("LinkedBinaryTreeTest.testAddException");
    }

    public static void testAddLeft() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(7);
        Node<Integer> expected = new NodeImpl<>(linkedBinaryTree.root(), 5);
        assertEquals("LinkedBinaryTreeTest.testAddLeft", expected,
                linkedBinaryTree.addLeft(linkedBinaryTree.root(), 5));
    }

    public static void testAddRight() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(7);
        Node<Integer> expected = new NodeImpl<>(linkedBinaryTree.root(), 5);
        assertEquals("LinkedBinaryTreeTest.testAddRight", expected,
                linkedBinaryTree.addRight(linkedBinaryTree.root(), 5));
    }

    public static void testAddLeftException() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(7);
        linkedBinaryTree.addLeft(linkedBinaryTree.root(), 1);
        try {
            linkedBinaryTree.addLeft(linkedBinaryTree.root(), 2);
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testAddLeftException", CHILDREN_NOT_NULL_MSG, e.getMessage());
            return;
        }
        fail("LinkedBinaryTreeTest.testAddLeftException");
    }

    public static void testAddRightException() {
        LinkedBinaryTree<Integer> linkedBinaryTree = new LinkedBinaryTree<>();
        linkedBinaryTree.addRoot(7);
        linkedBinaryTree.addRight(linkedBinaryTree.root(), 1);
        try {
            linkedBinaryTree.addRight(linkedBinaryTree.root(), 2);
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testAddRightException", CHILDREN_NOT_NULL_MSG, e.getMessage());
            return;
        }
        fail("LinkedBinaryTreeTest.testAddRightException");
    }

    public static void testSet() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        linkedBinaryTree.set(linkedBinaryTree.root(), 777);
        assertEquals("LinkedBinaryTreeTest.testSet", 777, (int) linkedBinaryTree.root().getElement());
    }

    public static void testSetException() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        try {
            linkedBinaryTree.set(null, 777);
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testSetException", NODE_NULL_MSG, e.getMessage());
            return;
        }
        fail("LinkedBinaryTreeTest.testSetException");
    }

    public static void testRemove() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        int expected = 6;
        Node<Integer> toRemove = linkedBinaryTree.validate(linkedBinaryTree.root()).getRightChild();
        assertEquals("LinkedBinaryTreeTest.testRemove", expected, (int) linkedBinaryTree.remove(toRemove));
    }

    public static void testRemoveException() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        try {
            linkedBinaryTree.remove(linkedBinaryTree.root());
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTreeTest.testRemoveException", CAN_NOT_DELETE_MSG, e.getMessage());
            return;
        }
        fail("LinkedBinaryTreeTest.testRemoveException");
    }

    public static void testLeft() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        NodeImpl<Integer> expected = new NodeImpl<>(null, 3);
        assertEquals("LinkedBinaryTreeTest.testLeft", expected, linkedBinaryTree.left(linkedBinaryTree.root()));
    }

    public static void testRight() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        NodeImpl<Integer> expected = new NodeImpl<>(6);
        assertEquals("LinkedBinaryTreeTest.testRight", expected, linkedBinaryTree.right(linkedBinaryTree.root()));
    }

    public static void testRoot() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        NodeImpl<Integer> expected = new NodeImpl<>(7);
        assertEquals("LinkedBinaryTreeTest.testRoot", expected, linkedBinaryTree.root());
    }

    public static void testParent() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        Node<Integer> expected = linkedBinaryTree.root();
        assertEquals("LinkedBinaryTreeTest.testParent", expected,
                linkedBinaryTree.parent(linkedBinaryTree.validate(linkedBinaryTree.root()).getLeftChild()));
    }

    public static void testSize() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        assertEquals("LinkedBinaryTreeTest.testSize", 7, linkedBinaryTree.size());
    }

    public static void testNodes() {
        Collection<NodeImpl<Integer>> expected = new ArrayList<>();
        expected.add(new NodeImpl<>(7));
        expected.add(new NodeImpl<>(6));
        expected.add(new NodeImpl<>(8));
        expected.add(new NodeImpl<>(3));
        expected.add(new NodeImpl<>(9));
        expected.add(new NodeImpl<>(12));
        expected.add(new NodeImpl<>(5));
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        assertEquals("LinkedBinaryTreeTest.testNodes", expected, linkedBinaryTree.nodes());
    }

    public static void testIsInternalTrue() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node3 = tree.validate(root.getLeftChild());
        assertEquals("LinkedBinaryTreeTest.testIsInternalTrue", true, tree.isInternal(node3));
    }

    public static void testIsInternalFalse() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node3 = tree.validate(root.getLeftChild());
        NodeImpl<Integer> node5 = tree.validate(node3.getLeftChild());
        assertEquals("LinkedBinaryTreeTest.testIsInternalFalse", false, tree.isInternal(node5));
    }

    public static void testIsExternalTrue() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node3 = tree.validate(root.getLeftChild());
        NodeImpl<Integer> node5 = tree.validate(node3.getLeftChild());
        assertEquals("LinkedBinaryTreeTest.testIsExternalTrue", true, tree.isExternal(node5));
    }

    public static void testIsExternalFalse() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node3 = tree.validate(root.getLeftChild());
        assertEquals("LinkedBinaryTreeTest.testIsExternalFalse", false, tree.isExternal(node3));
    }

    public static void testIsRootTrue() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        assertEquals("LinkedBinaryTreeTest.testIsRootTrue", true, tree.isRoot(root));
    }

    public static void testIsRootFalse() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        NodeImpl<Integer> root = tree.validate(tree.root());
        NodeImpl<Integer> node3 = tree.validate(root.getLeftChild());
        assertEquals("LinkedBinaryTreeTest.testIsRootFalse", false, tree.isRoot(node3));
    }

    public static void testIsEmptyTrue() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        assertEquals("LinkedBinaryTreeTest.testIsEmptyTrue", true, tree.isEmpty());
    }

    public static void testIsEmptyFalse() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        assertEquals("LinkedBinaryTreeTest.testIsEmptyFalse", false, tree.isEmpty());
    }

    public static void testPreorder() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        int[] actual = new int[linkedBinaryTree.size()];
        int counter = 0;
        for (Node<Integer> n : linkedBinaryTree.preOrder()) {
            actual[counter++] = n.getElement();
        }
        int[] expected = {7, 6, 8, 3, 9, 12, 5};
        assertEquals("LinkedBinaryTreeTest.testPreorder", expected, actual);
    }

    public static void testInorder() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        int[] actual = new int[linkedBinaryTree.size()];
        int counter = 0;
        for (Node<Integer> n : linkedBinaryTree.inOrder()) {
            actual[counter++] = n.getElement();
        }
        int[] expected = {5, 3, 12, 9, 7, 8, 6};
        assertEquals("LinkedBinaryTreeTest.testInorder", expected, actual);
    }

    public static void testPostorder() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        int[] actual = new int[linkedBinaryTree.size()];
        int counter = 0;
        for (Node<Integer> n : linkedBinaryTree.postOrder()) {
            actual[counter++] = n.getElement();
        }
        int[] expected = {5, 12, 9, 3, 8, 6, 7};
        assertEquals("LinkedBinaryTreeTest.testPostorder", expected, actual);
    }

    public static void testBreadthFirst() {
        LinkedBinaryTree<Integer> linkedBinaryTree = getLinkedBinaryTree();
        int[] actual = new int[linkedBinaryTree.size()];
        int counter = 0;
        for (Node<Integer> n : linkedBinaryTree.breadthFirst()) {
            actual[counter++] = n.getElement();
        }
        int[] expected = {7, 3, 6, 5, 9, 8, 12};
        assertEquals("LinkedBinaryTreeTest.testBreadthFirst", expected, actual);
    }

    public static void testSibling() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        NodeImpl<Integer> node3 = new NodeImpl<>(tree.root, 3);
        NodeImpl<Integer> node6 = new NodeImpl<>(tree.root, 6);
        assertEquals("LinkedBinaryTreeTest.testSibling", node3, tree.sibling(node6));
    }

    public static void testChildren() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        Collection<Node<Integer>> expected = new ArrayList<>();
        expected.add(new NodeImpl<>(tree.root, 3));
        expected.add(new NodeImpl<>(tree.root, 6));
        assertEquals("LinkedBinaryTreeTest.testChildren", expected, tree.children(tree.root()));
    }

    public static void testChildrenNumber() {
        LinkedBinaryTree<Integer> tree = getLinkedBinaryTree();
        assertEquals("LinkedBinaryTreeTest.testChildrenNumber", 2, tree.childrenNumber(tree.root()));
    }

}