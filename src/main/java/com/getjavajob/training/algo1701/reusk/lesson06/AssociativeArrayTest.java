package com.getjavajob.training.algo1701.reusk.lesson06;

import static com.getjavajob.training.algo1701.reusk.util.Assert.assertEquals;

public class AssociativeArrayTest {

    public static void main(String[] args) {
        testAddToNew();
        testAddNull();
        testAddRewriteValue();
        testAddRewriteValueNull();
        testGet();
        testGetNull();
        testRemove();
        testRemoveNull();
    }

    public static void testAddToNew() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        assertEquals("AssociativeArrayTest.testAddToNew", null, aa.add("SEVEN", 7));
    }

    public static void testAddNull() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        assertEquals("AssociativeArrayTest.testAddNull", null, aa.add("NULL", null));
    }

    public static void testAddRewriteValue() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        assertEquals("AssociativeArrayTest.testAddRewriteValue", 1, (int) aa.add("ONE", 11));
    }

    public static void testAddRewriteValueNull() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        aa.add(null, 0);
        assertEquals("AssociativeArrayTest.testAddRewriteValueNull", 0, (int) aa.add(null, 999));
    }

    public static void testGet() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        assertEquals("AssociativeArrayTest.testGet", 3, (int) aa.get("THREE"));
    }

    public static void testGetNull() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        aa.add(null, 777);
        assertEquals("AssociativeArrayTest.testGetNull", 777, (int) aa.get(null));
    }

    public static void testRemove() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        assertEquals("AssociativeArrayTest.testRemove", 3, (int) aa.remove("THREE"));
    }

    public static void testRemoveNull() {
        AssociativeArray<String, Integer> aa = getAssociativeArray();
        aa.add(null, 777);
        assertEquals("AssociativeArrayTest.testRemoveNull", 777, (int) aa.remove(null));
    }

    private static AssociativeArray<String, Integer> getAssociativeArray() {
        AssociativeArray<String, Integer> aa = new AssociativeArray<>();
        aa.add("ONE", 1);
        aa.add("TWO", 2);
        aa.add("THREE", 3);
        return aa;
    }

}
